# Série 5, Exercice T4
# Clément Vogt

# Imports
from turtle import *

# Procédures et fonctions
def deplacer_ver_position_initiale():
    hideturtle() #pour éviter de voir la tortue bouger
    penup() #pour éviter un trait non demandé
    setpos(-400, 0) #pour démarrer plus à gauche
    pendown() #pour réactiver le dessin de la tortue
    showturtle() #pour afficher la tortue elle-même    

def un_trait():
    color('blue')
    dot(10)
    forward(50)
    dot(10)

def un_pic():
    left(45)
    un_trait()
    right(90)
    un_trait()
    left(45)

# Procédure main()
def main():
    nb_pics = int(input("Entrez un nombre de pics"))
    
    deplacer_ver_position_initiale()
    
    for i in range(0,nb_pics):
        un_pic()

# Appel de la procédure main()
main()