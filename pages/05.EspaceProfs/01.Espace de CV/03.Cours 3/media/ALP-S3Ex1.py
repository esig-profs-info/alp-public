# Série 3, Exercice 1
# Clément Vogt

# Imports
import math

# Procédures et fonctions
def donnees_entree():
    rayon = float(input("Entrez un rayon"))
    return rayon
    
def calcul_aire(r):
    return math.pi * r**2

def calcul_circonference(r):
    return math.pi * r * 2

def afficher_resultats(r,a,c):
    print("Un cercle de rayon ",r," a une aire de ",a," et une circonférence de ",c,sep="")

# Procédure main()
def main():
    rayon = donnees_entree()
    aire = calcul_aire(rayon)
    circonference = calcul_circonference(rayon)
    afficher_resultats(rayon,aire,circonference)

# Appel de la procédure main()
if __name__ == "__main__":
    main()