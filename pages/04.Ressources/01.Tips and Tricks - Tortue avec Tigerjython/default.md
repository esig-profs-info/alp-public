---
title: Tips and Tricks - Tigerjython turtle
published: true
---

# Tortue avec Tigerjython - _Tips and Tricks_

===

Pour accéler la vitesse de la tortue, ajouter un appel à la fonction `speed` au début de votre programme:

```python
makeTurtle()
speed(1) # très lent
speed(100) # rapide
speed(-1) # le plus rapide
```

Pour des calculs mathématiques, module `math` contient beaucoup de choses utiles:
```python
import math

print math.pi # affiche la valeur de Pi (3.14...)
print math.sqrt(3) # affiche la racine carrée de 3
```

