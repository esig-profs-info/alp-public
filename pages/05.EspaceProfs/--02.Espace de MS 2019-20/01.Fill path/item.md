Testez votre compréhension de la fonction fillPath()!

===

![code](code.PNG)

Dessin après exécution de la ligne 22 :

![dessin à la ligne 22](situation_avant_fillpath.PNG)

Que sera l’effet de l’instruction fillPath() ? Dessinez la forme finale :