---
template: item
published : true
title: Remédiation d'urgence après ER novembre
---

### Remédiation 1 :

[Lire jusqu'à "Passage par valeur et passage par référence (non inclus)"](https://openclassrooms.com/fr/courses/1894236-programmez-avec-le-langage-c/1895985-decoupez-votre-programme-en-fonctions)

### Remédiation 2 :

Pourquoi on utilise des paramètres pour communiquer: <br/>
* [Lire jusqu'à "Hors de portée" (non inclus)](http://python.lycee.free.fr/rurple/fr/inter/31-global.htm)<br/>
* [Mais aussi: (depuis "variables globales" jusqu'à la fin)](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/232520-decouvrez-la-portee-des-variables-et-les-references#/id/r-2232829)
)

### Remédiation 3:

Créer un programme de facturation qui:<br/>

1. Demande à l'utilisa-teur-trice son prénom
1. Le-La salue en utilisant son prénom
1. Demande à l'utilisateur-trice:
    * les montant HT des fournitures
    * Le nombre d'heures de main d'oeuvre concernées
1. Calcule le montant HT de la main d'oeuvre sachant que le tarif horaire est de 150 CHF / heure
1. Calcule le montant TTC de la facture sachant que la TVA est de 7.7% et des charges spéciales se montent à:
    * 5% pour un montant avec TVA jussqu'à 200 CHF (non inclus)
    * 3% pour un montant avec TVA entre 200 CHF et 500 CHF
    * 1% pour un montant au delà de 500 CHF

### Contraintes:

Créer et appeler les fonctions suivantes (à vous de décider si elles doivent prendes des paramètres et si elles doivent retourner une valeur):

```
demanderPrenom(?)
direBonjour(?)
demanderUneValeur(?)
calculerMainDoeuvre(?)
determinerPourcentageChargesSpeciales(?)
calculerMontantFacture(?)
afficherResultats(?)
```

### Analyse:

Argumentez (un petit texte en français tout simple):

* En quoi la création de ces fonctions représentent une bonne pratique.
* Est-ce que certaines fonctions demandées sont superflues (ou inutiles)? si oui pourquoi?
* Vos choix concernant l'utilisation du tarif horaires et celle du taux de TVA (variables ou non et si oui, déclarée où? pourquoi?)

### Remédiation 4 :
Comprenez puis corrigez le code suivant...
```


def nomduMois(no):
    mois = ["janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"]
    if no >= 1 and no <=12:
        return mois[no-1]
    else:
        print("{} est un numéro de mois impossible".format(no))
        return None

def yAtIlUnNegatif(liste):
    for no in liste:
        if liste[no] > 0:
            return False
        else:
            return True

def afficherSoldes(soldes):
    for i in range(12):
        print("{} : {}".format(nomduMois(i+1),soldes[i]))

#-- main --
compteNo = 5
soldesDuCompte = [1500,450,-50,3000,2542,-150,400,500,1000,-200,300,3327]
print("Union des banques ESIG:")
print("***********************")
print("le compte no {} présente, cette année, les soldes suivants:".format(compteNo))
afficherSoldes(soldesDuCompte)
if yAtIlUnNegatif(soldesDuCompte):
    print("soit au moins une fin de mois avec un solde négatif")
else:
    print("donc aucune fin de moins avec un solde négatif")

```
... pour qu'il affiche:
```
Union des banques ESIG:
***********************
le compte no 5 présente, cette année, les soldes suivants:
janvier : 1500
février : 450
mars : -50
avril : 3000
mai : 2542
juin : -150
juillet : 400
août : 500
septembre : 1000
octobre : -200
novembre : 300
décembre : 3327
soit au moins une fin de mois avec un solde négatif
```

### Analyse:
Relevez et argumentez chaque modification (dans un fichier texte à part.)
