import pandas as pd

# Le code utilisé pour la génération du tableau. Vous n'avez pas besoin de vos en servir, ni de le comprendre.
dicoNoms = {'ALP' : ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 'AREGAZRRO', 'HUJICY', 'YVEWOW', 'MIJGYH'],
'MIRESE' : ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH'],
'MODE' : ['LJUOSOP', 'HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'ZEEJ'],
'DROIT' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ'],
'DWEB' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'BUQOH-AKHSO', 'GUFUJU', 'SIO', 'ZEEJ'],
'FRTC' : ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 'AREGAZRRO', 'HUJICY', 'YVEWOW', 'MIJGYH'],
'TQGE' : ['LJUOSOP', 'HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ'],
'GESTION' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ'],
'TEPR' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'ZETHODE', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ']}


if __name__ == '__main__':
    listeEtu = sorted(set([etu for k in dicoNoms for etu in dicoNoms[k]]))
    print(listeEtu)
    print(list(sorted(dicoNoms.keys())))
    df_bool = pd.DataFrame(index=listeEtu, columns=sorted(dicoNoms.keys()))

    df_oui_non = df_bool.copy()

    for etu in df_bool.index:
        df_bool.loc[etu] = [etu in dicoNoms[c] for c in df_bool.columns]
        df_oui_non.loc[etu] = ['oui' if etu in dicoNoms[c] else 'non' for c in df_oui_non.columns]

    print(df_bool.to_markdown())
    print()
    print()
    print(df_oui_non.to_markdown())

    tab_bool = df_bool.to_numpy(dtype=bool)
    tab_oui_non = df_oui_non.to_numpy(dtype=str)

    print(tab_bool.tolist())

