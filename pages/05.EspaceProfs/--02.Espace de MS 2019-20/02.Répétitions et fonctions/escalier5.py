from gturtle import * 

makeTurtle()

def creerMarche(longueur):
    fd(longueur)
    rt(90)
    fd(longueur)
    lt(90)

def creerEscalier(nbMarches):
    repeat nbMarches: 
        creerMarche(5)

creerEscalier(8)

