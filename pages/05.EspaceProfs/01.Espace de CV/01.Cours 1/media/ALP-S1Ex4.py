# Série 1, Exercice 4
# Clément Vogt

nb_entier = int(input("Entrez un nombre entier:"))

if nb_entier < 0:
    print("Le nombre est négatif")
else:
    print("Le nombre est positif ou nul")