---
template: item
published: true
---

# Série 4
## Objectifs
Approfondir le concept d'**alternative**.

**Décomposer** la solution d'un problème en **tâches**.

Mettre en pratique les concepts de **procédures**, de **fonction** et de **paramètre**.


===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S4Ex1** à **ALP-S4Ex3** et se trouveront dans un dossier nommé **ALP-S4**.

## Énoncé

### Exercice 1
Une entreprise de déménagement ouvre un nouveau service spécial pour le transport de pianos. Elle vous demande de mettre à disposition un petit outil permettant de calculer et afficher le prix à payer pour quelqu'un souhaitant déménager son piano. Le prix est constitué des parties suivantes :
> * le prix de base dépend du nombre total d'étages à descendre et remonter ainsi que du type de piano (piano droit = type n°1 ou piano à queue = type n°2) :
>    * le prix par étage monté ou descendu sera de 60.- pour un piano droit et de 90.- pour un piano  à queue.
>    * dans tous les cas, au maximum 10 étages sont facturés.
> * le prix du transport est de 6.- par km, quel que soit le type de piano.
> * le prix total à payer sera au minimum de 200.- et au maximum 2'500.-

Le programme que vous devez écrire effectue les calculs conformément aux règles énoncées ci-dessus sur la base du type de piano (piano à queue ou pas), du nombre d'étages à descendre avant le transport puis à remonter à destination, ainsi que du nombre de km à parcourir. Vous devez respecter le format d'affichage représenté ci-dessous (voir Résultats).

On part du principe que toutes les données saisies par l'utilisateur sont correctes.

<u>Résultats</u>

Voici les résultats que vous devez obtenir pour un certain nombre de valeurs test. L'obtention de ces résultats ne garantit évidemment pas que le programme que vous avez conçu est correct et complet. Vous devez également mener des tests d'exécution personnels, adaptés au code que vous avez développé.

Valeurs de test : 1 étage descendu, 2 étages à monter, 3 km à parcourir, pour un piano droit (type n°1) / à queue (type n°2) :

    Prix total à payer pour un piano : 200.- (dont 18.- de transport pour les 3 km)
    
    Prix total à payer pour un piano à queue : 288.- (dont 18.- de transport pour les 3 km)
	
Valeurs de test : 3 étages descendus, 4 étages à monter, 30 km à parcourir, pour un piano droit (type n°1) / à queue (type n°2):

    Prix total à payer pour un piano : 600.- (dont 180.- de transport pour les 30 km)
    
    Prix total à payer pour un piano à queue : 810.- (dont 180.- de transport pour les 30 km)

### Exercice 2
L’ESIG a décidé d'organiser des mini-concerts et vous demande de mettre à disposition un petit outil permettant de calculer le prix à payer pour quelqu'un souhaitant acheter une ou plusieurs places. L'acheteur doit spécifier s'il souhaite des places debout (type n°1) ou des places assises (type n°2), ainsi que le nombre de places désiré. Dans le cas de l'achat de plusieurs places, elles seront donc toutes du même type (toutes assises ou debout). Le prix est calculé comme suit :
> * prix d'une place debout: 20.-
> * prix d'une place assise: 30.-
> * dans le cas où on achète des places assises, on peut obtenir des réductions:
>	* 1 place est gratuite sur l'achat de 5 places ou plus
>	* 2 places sont gratuites sur l'achat de 9 places ou plus

Le programme que vous devez écrire effectue les calculs conformément aux règles énoncées ci-dessus et affiche le montant à payer selon le type et le nombre de places demandés. Vous devez respecter le format d'affichage représenté ci-dessous (voir Résultats).

<u>Résultats</u>

Voici les résultats que vous devez obtenir pour un certain nombre de valeurs test. L'obtention de ces résultats ne garantit évidemment pas que le programme que vous avez conçu est correct et complet. Vous devez également mener des tests d'exécution personnels, adaptés au code que vous avez développé.

    Le prix à payer pour 2 places debout : 40 Frs

    Le prix à payer pour 6 places debout : 120 Frs

    Le prix à payer pour 9 places debout : 180 Frs

    Le prix à payer pour 2 places assises : 60 Frs
    
    Le prix à payer pour 6 places assises : 150 Frs

    Le prix à payer pour 9 places assises : 210 Frs

### Exercice 3
Vous travaillez dans une entreprise de location de véhicules. Vous êtes chargé(e) de réaliser un programme qui calcule le prix de la location d’une voiture. Ce prix dépend de la catégorie du véhicule loué et du nombre de kilomètres parcourus par le client ; les collaborateurs de l’entreprise bénéficient en outre d’un rabais.

Trois catégories de véhicules (numérotées 1, 2 et 3) différentes sont offertes à la location.

Il y a deux types de clients : le type 1 qui représente les collaborateurs de l’entreprise et le type 2 qui représente les autres clients.

Les règles de calcul du prix total de location sont les suivantes :
> *	Pour chaque location, le client doit payer une prise en charge forfaitaire du véhicule ; cette prise en charge se monte à 100.- pour les véhicules de la catégorie 1 et à 200.- pour les autres.
> *	Le prix au kilomètre pour les véhicules de la catégorie 1 se monte à 0.80F, il est fixé à 1.00F pour ceux de la catégorie 2 et vaut 1.50F pour ceux de la catégorie 3.
> *	Les collaborateurs de l’entreprise obtiennent un rabais de 10% sur le prix calculé après prise en charge et kilométrage ; les autres clients n’ont droit à aucun rabais.

Le programme que vous devez écrire calcule le prix à payer et affiche la catégorie du véhicule, le type de client, le nombre de kilomètres parcourus et la somme totale à payer.

Le type du client devra être affiché explicitement (collaborateur ou autre).

On suppose que les données saisies par l’utilisateur sont valides.

<u>Résultats</u>

Voici les résultats que vous devez obtenir pour un certain nombre de valeurs de test. L’obtention de ces résultats ne garantit évidemment pas que le programme que vous avez conçu est correct et complet. Vous devez également mener des tests d’exécution personnels, adaptés au code que vous avez développé.

<table>
    <thead>
        <tr>
            <th>Catégorie</th>
            <th>Type client </th>
            <th>Nb km</th>
            <th>Message à afficher & Prix à payer</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4>1</td>
            <td rowspan=4>1</td>
            <td rowspan=4>10</td>
            <td> Catégorie: 1</td>
        </tr>
        <tr>
            <td>Type de client: collaborateur</td>
        </tr>
        <tr>
            <td>Nombre de kilomètres: 10</td>
        </tr>
        <tr>
            <td>Prix à payer: 97.2</td>
        </tr>        
        <tr>
            <td rowspan=4>2</td>
            <td rowspan=4>1</td>
            <td rowspan=4>20</td>
            <td> Catégorie: 2</td>
        </tr>
        <tr>
            <td>Type de client: collaborateur</td>
        </tr>
        <tr>
            <td>Nombre de kilomètres: 20</td>
        </tr>
        <tr>
            <td>Prix à payer: 198.0</td>
        </tr>         
        <tr>
            <td rowspan=4>3</td>
            <td rowspan=4>1</td>
            <td rowspan=4>15</td>
            <td> Catégorie: 3</td>
        </tr>
        <tr>
            <td>Type de client: collaborateur</td>
        </tr>
        <tr>
            <td>Nombre de kilomètres: 15</td>
        </tr>
        <tr>
            <td>Prix à payer: 200.25</td>
        </tr> 
        <tr>
            <td rowspan=4>1</td>
            <td rowspan=4>2</td>
            <td rowspan=4>100</td>
            <td> Catégorie: 1</td>
        </tr>
        <tr>
            <td>Type de client: autre</td>
        </tr>
        <tr>
            <td>Nombre de kilomètres: 100</td>
        </tr>
        <tr>
            <td>Prix à payer: 180.0</td>
        </tr> 
        <tr>
            <td rowspan=4>2</td>
            <td rowspan=4>2</td>
            <td rowspan=4>80</td>
            <td> Catégorie: 2</td>
        </tr>
        <tr>
            <td>Type de client: autre</td>
        </tr>
        <tr>
            <td>Nombre de kilomètres: 80</td>
        </tr>
        <tr>
            <td>Prix à payer: 280</td>
        </tr>         
        <tr>
            <td rowspan=4>3</td>
            <td rowspan=4>2</td>
            <td rowspan=4>75</td>
            <td> Catégorie: 3</td>
        </tr>
        <tr>
            <td>Type de client: autre</td>
        </tr>
        <tr>
            <td>Nombre de kilomètres: 75</td>
        </tr>
        <tr>
            <td>Prix à payer: 312.5</td>
        </tr>         
    </tbody>
</table>