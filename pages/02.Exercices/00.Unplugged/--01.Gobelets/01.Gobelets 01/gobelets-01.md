---
template: item

---

Activité numéro 1 avec des gobelets, pour commencer en douceur. Il s'agit ici de découvrir le langage

===

##Exercices - Gobelets
###Activité n°1 - Définir le langage
#### Contexte:
* *Activité par groupe de 2 étudiant-e-s*
* *Durée estimée: 30 minutes*
#### Considérations de départ:
1. La pile de Gobelet à gauche contient toujours suffisemment de gobelets.
2. Aide au positionnement des pas:<br/>
![aide au positionnement des pas](aide_positionnement_pas.jpg?cropResize=300,300)

#### Votre travail:
Après exécution de l'algoritme suivant...<br/>
![algo fléché](algo_activite_1.jpg)

...les gobelets se retrouvent dans cette position:<br/><br/>
![résultat de l'algo 1](resultat_algo_activite_1.jpg?cropResize=300,300)

=> Par deux, mettez vous d'accord sur une définition de ce que chaque symbole signifie (recopiez la grille comme bon vous semble):<br/><br/>
![grille à remplir: notre language](grille_notre_language.jpg)

##Attention!
*N'ouvrez pas l'activité Gobelets02 avant d'avoir terminé cette activité!*
