Résoudre à l'aide du système HP! puis écrivez le code correspondant.

### Ex01 - Additionneur infini
Additionne toutes les valeurs reçues jusqu’à l’infini.
N’affiche pas le résultat en sortie.

|In |Out|
|---|---|
|1,2,3,4,5,6|21|
|10,15,17,100|142|


### Ex02a - Exterminateur de zéros:
Ne recopie un entier sur la sortie que s’il n’est pas nul.

|In |Out|
|---|---|
|2|2|
|0||

### Ex02b - Exterminateur de zéros à l'infini:
Ne recopie un entier sur la sortie que s’il n’est pas nul.

|In |Out|
|---|---|
|1,2,3,0,10,0,45,...|1,2,3,10,45,...|

### Ex03 - Exterminateur de négatifs à l'infini:
Ne recopie un entier sur la sortie que s’il est positif.

|In |Out|
|---|---|
|3,4,5,-10,0,10,-1,15,...|3,4,5,0,15,...|

### Ex04 - Exterminateur de négatifs ou nuls à l'infini:
Ne recopie un entier sur la sortie que s’il est strictement positif
(positif et non nul).

|In |Out|
|---|---|
|3,4,5,-10,0,10,-1,15,...|3,4,5,10,15,...|

### Ex05 - Exterminateur de positifs à l'infini:
Ne recopie un entier sur la sortie que s’il est négatif.

|In |Out|
|---|---|
|15,-2,-7,0,15,...|-2,-7,...|

### Ex06 - Conservateur de zéros:
Recopie uniquement les zéros sur la sortie.

|In |Out|
|---|---|
|3,0,1,0,2,9,0,4,0,...|0,0,0,0,...|
|0,3,0,-1,0,2,-9,0,4,0,...|0,0,0,0,0,...|

### Ex07 - Octoplicateur:
Multiplie par huit la valeur entrée.
Affiche le résultat sur la sortie.
__Challenge : maximum 3 additions.__

_Indice_ : 8 est une puissance de 2. Pour obtenir 8 à partir de 1,
ajouter 1 à lui-même, puis ajouter le résultat à lui-même, et
recommencer encore une fois. En effet : 1+1=2, 2+2=4, 4+4=8.

|In |Out|
|---|---|
|3|24|

### Ex08 - Décaplicateur:
Multiplie par dix la valeur entrée.
Affiche le résultat sur la sortie.
__Challenge : maximum 4 additions.__

_Indice_ : 10 = 8 + 2.

|In |Out|
|---|---|
|3|30|

### Ex09 - Heptaplicateur:
Multiplie par sept la valeur entrée.
Affiche le résultat sur la sortie.
__Challenge : utilise uniquement les pièces d’une seule boite.__

_Indice_ : 7 = 8-1.

|In |Out|
|---|---|
|3|21|

### Ex10 - Compteur:
Lis un entier positif.
Affiche tous les entiers plus petits jusqu’à 0 sur la sortie.

|In |Out|
|---|---|
|3|2,1,0|
|7|6,5,4,3,2,1,0|


