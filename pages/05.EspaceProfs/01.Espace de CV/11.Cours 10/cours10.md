---
template: item
published: true
---

# Cours 10

===

* Listes
    * <u>Rappels:</u> voir Cours 9
    * En particulier les scripts [ALP-ListeIntro](./media/ALP-ListeIntro.py) et [ALP-ListeIntro2](./media/ALP-ListeIntro2.py).
    * Correction de la Série 9, exercice T1 (voir [ALP-S9ExT1](./media/ALP-S9ExT1.py))
    * Correction de la Série 9, exercice 5 (voir [ALP-S9Ex5](./media/ALP-S9Ex5.py))
    * Correction de la Série 9, exercice 6 (voir [ALP-S9Ex6](./media/ALP-S9Ex6.py))
        * Discussion de plusieurs versions de la validation des données.
        * Rappel du *short cut* dans l'évaluation des conditions et de ses conséquences sur la conception des tests de continuation des boucles **while**.
        * Fonction [round()](https://docs.python.org/fr/3/library/functions.html#round) pour arrondir un nombre.
    * Correction de la Série 9, exercice 7 (voir [ALP-S9Ex7](./media/ALP-S9Ex7.py))
    * Série 9, exercice supplémentaire (voir [énoncé](./media/Fourni_ALP-S9ExSupp.py) et [corrigé](./media/ALP-S9ExSupp.py))
        * Discussion de plusieurs versions de la fonction existe_valeur_negative()
    * Si vous avez une boucle imbriquée, c’est que vous n’avez pas assez décomposé votre code !
        * <u>Illustration :</u> voir [BoucleImbriquee](./media/BoucleImbriquee.py)
    * Correction de la Série 10, exercice 1 (voir [ALP-S10Ex1](./media/ALP-S10Ex1.py))
    * Correction de la Série 10, exercice 2 (voir [ALP-S10Ex2](./media/ALP-S10Ex2.py))
    * Correction de la Série 10, exercice 3 (voir [ALP-S10Ex3](./media/ALP-S10Ex3.py))
    
**<u>Exercices :</u>** [Série 10](/exercices/code/serie%2010)