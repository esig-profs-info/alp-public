# Série 1, Exercice T3
# Clément Vogt

from turtle import *

longueur = float(input("Saisissez la longueur du rectangle"))

largeur = longueur / 2

forward(longueur)
left(90)
forward(largeur)
left(90)
forward(longueur)
left(90)
forward(largeur)
left(90)