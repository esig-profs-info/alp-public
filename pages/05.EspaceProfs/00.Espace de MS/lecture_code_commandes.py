from gturtle import *

def schtroumpf1():
    fd(20) # fd = forward
    rt(360/3) # rt = right
    fd(20)
    rt(360/3)
    fd(20)
    rt(360/3)
    
def schtroumpf2():
    schtroumpf1()
    rt(180)
    fd(20)
    lt(180)
    schtroumpf1()
    rt(180)
    fd(20)

makeTurtle()   
schtroumpf2()