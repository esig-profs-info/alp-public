---
template: item
published : true
title: Activité en plus pour étudiants en avance
---
## Terrains de sports:
Dans TigerJython, dessinez, à choix, un terrain de football ou un terrain de basketball (vu de dessus).<br/>
Prêtez attention à respecter les proportions.

#### Pour le terrain de basketball, les dimensions sont celles d'un terrain FIBA:
[terrain de basketball sur wikipedia](https://fr.wikipedia.org/wiki/Terrain_de_basket-ball#Dimensions?target=_blank)
#### Pour le terrain de football, les dimensions varient, vous avez donc un peu plus de liberté:
[terrain de football sur wikipedia](https://fr.wikipedia.org/wiki/Loi_1_du_football#Dimensions?target=_blank)