# Série 6, Exercice 2
# Clément Vogt

# Constantes
TYPE_PLACE_DEBOUT = 1
TYPE_PLACE_ASSIS = 2
PRIX_PLACE_DEBOUT = 20
PRIX_PLACE_ASSISES = 30
LIM_1_REDUCTION = 5
LIM_2_REDUCTION = 9
NB_PLACE_GRATUITE_LIM_1 = 1
NB_PLACE_GRATUITE_LIM_2 = 2
NB_PLACES_MIN = 1
NB_PLACES_MAX = 70

# Procédures et fonctions
def donnees_valides(type_places, nb_places):
    est_valide = True
    
    if type_places != TYPE_PLACE_DEBOUT and type_places != TYPE_PLACE_ASSIS:
        print("Le type de places doit être",TYPE_PLACE_DEBOUT,"(debout) ou",TYPE_PLACE_ASSIS , "(assis).")
        est_valide = False
        
    if nb_places < NB_PLACES_MIN:
        print("Le nombre de places doit être strictement positif.")
        est_valide = False
        
    if nb_places > NB_PLACES_MAX:
        print("Le nombre de places ne peut pas excéder ",NB_PLACES_MAX,".",sep="")
        est_valide = False
        
    return est_valide
    
def donnees_entrees():
    type_places = int(input("Entrez le type de places souhaité"))
    nb_places = int(input("Entrez le nombre de places désiré"))
    return type_places, nb_places

def prix_a_payer(type_places,nb_places):
    if type_places == TYPE_PLACE_DEBOUT:
        prix = PRIX_PLACE_DEBOUT * nb_places
    else:
        if nb_places >= LIM_2_REDUCTION:
            reduction = PRIX_PLACE_ASSISES * NB_PLACE_GRATUITE_LIM_2
        elif nb_places >= LIM_1_REDUCTION:
            reduction = PRIX_PLACE_ASSISES * NB_PLACE_GRATUITE_LIM_1
        else:
            reduction = 0
        prix = PRIX_PLACE_ASSISES * nb_places - reduction
    return prix

def afficher_resultats(type_places,nb_places,prix):
    print("Le prix à payer pour ",nb_places, " places ",sep='',end='')
    if type_places == TYPE_PLACE_DEBOUT:
        print("debout",end='')
    else:
        print("assises",end='')
    print(" : ", prix, " Frs",sep='')

# Procédure main()
def main():
    type_places, nb_places = donnees_entrees()
    if donnees_valides(type_places, nb_places):
        prix = prix_a_payer(type_places,nb_places)
        afficher_resultats(type_places,nb_places,prix)

# Appel de la procédure main()
if __name__ == "__main__":
    main()