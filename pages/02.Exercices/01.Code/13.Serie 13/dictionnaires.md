---
template: item
published: true
---

# Série 13
## Objectifs
Comprendre le concept de **dictionnaire**.

Mettre en œuvre le concept de **dictionnaire** pour résoudre des problèmes simples 

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S13Ex1** à **ALP-S13Ex4** et se trouveront dans un dossier nommé **ALP-S13**.

## Énoncé
## Exercice 1

Soit le dictionnaire :
`d = {'nom': 'Dupuis', 'prenom': 'Jacque', 'age': 30}`

Par programmation:

* Corriger l'erreur dans le prénom, la bonne valeur est 'Jacques'.
* Afficher la liste des clés du dictionnaire (avec et sans utiliser la fonction keys()).
Sortie attendue: 
```
nom
prenom
age
```
* Afficher la liste des valeurs du dictionnaire (avec et sans utiliser la fonction values()).
Sortie attendue: 
```
Dupuis
Jacques
30
```
* Afficher la liste des paires clé/valeur du dictionnaire. (avec et sans utiliser la fonction items())
Sortie attendue: 
```
nom : Dupuis
prenom : Jacques
age : 30
```
* Ecrire la phrase "Jacques Dupuis a 30 ans" (en récupérant les valeurs venues du dictionnaire).

## Exercice 2

Soit un programme permettant de gérer la facturation d'un maraîcher...

Créer le dictionnaire nommé `PRIX` qui contient les clés-valeurs suivantes:
```
"bananes": 4,
"pommes": 2,
"oranges": 1.5,
"poires": 3
```

Créer ensuite un autre dictionnaire nommé `STOCK` qui contient les valeurs suivantes (les clés sont identiques au dictionnaire `PRIX`):
```
"bananes": 100,
"pommes": 290,
"oranges": 50,
"poires": 120
```

1. Créer un programme demandant à l'utilisateur le nom d'un fruit, le programme affichera alors son prix et la quantité en stock, respectivement un message s'il n'existe pas
1. En parcourant un des dictionnaires, récupérer le prix et la quantité pour chaque clé trouvée et afficher la valeur du stock:
```
Montants des stocks:
bananes : 400 CHF
pommes : 580 CHF
oranges :  75 CHF
poires : 360 CHF
```

## Exercice 3
Soit les données suivantes:
```python
NOMS = ['Londa','Lenita','Beatrice','Kendrick','Genny','Rolf','Meridith','Hilton','Phylis','Candis','Ron','Lecia','Jacquelyn','Gonzalo','Herlinda','Morgan','Han','Sanjuanita','Allie','Fernande','Anna','Gracia','Lula','Merlyn','Tandy','Adah','Ozella','Laureen','Ricky','Miki']
NB_PLONGEES = [20,120,15,150,5,20,30,60,100,8,20,15,30,79,130,66,24,110,8,23,20,77,60,22,30,80,50,20,10,29]
```
1. Ecrire une fonction qui crée un dictionnaire à partir de ces deux listes, où la clé est le nom de la plongeuse et la valeur le nombre de plongées effectuées.
1. Ecrire un programme qui demande le nom d'une plongeuse en entrée et affiche le nombre de plongées que celle-ci a effectuée, respectivement un message si elle n'existe pas.
1. Ecrire une fonction qui retourne le nom et le nombre de plongées de la plongeuse qui a le plus d'expérience sous forme d'un nouveau dictionnaire. Résultat attendu: `{'nom': 'Kendrick', 'nb_plongees': 150}`.

## Exercice 4
Ecrire une procédure permettant d'afficher, sous la forme d'un histogramme en étoiles, le nombre de lettres majuscules d'une chaîne entrée par l'utilisateur.

Par exemple, la chaîne `"Exo 10 : Le Premier Exemple"` contient `2` fois `"E"`, `1` fois `"L"`, et `"1"` fois `"P"`.

Vous aurez donc, en sortie, quelque chose de la forme:
```
	A: (0)
	B: (0)
	C: (0)
	D: (0)
	E: ** (2)
	F: (0)
    G: (0)
    H: (0)
    I: (0)
    J: (0)
    K: (0)
    L: * (1)
    M: (0)
    N: (0)
    O: (0)
    P: * (1)	
	etc.
```
