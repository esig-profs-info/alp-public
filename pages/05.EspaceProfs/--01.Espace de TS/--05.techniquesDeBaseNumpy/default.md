---
template: item
published : true
title: Techniques de base pour Numpy
---

###Techniques de base pour le parcours de tableau numpy à 2 dimensions;

```
import numpy as np

def affichageParLignes(t):
    print("parcours par lignes puis par colonnes:")
    print("**************************************")
    for li in range(t.shape[0]):
        for col in range(t.shape[1]):
            print("ligne #{} - colonne#{} => valeur={}".format(li,col,t[li,col]))
        print("******fin de la ligne#{}".format(li))

def affichageParColonnes(t):
    print("parcours par colonnes puis par lignes:")
    print("**************************************")
    for col in range(t.shape[1]):
        for li in range(t.shape[0]):
            print("colonne #{} - ligne#{} => valeur={}".format(col,li,t[li,col]))
        print("******fin de la colonne#{}".format(col))
        
def moyennesParLignes(t):
    moyTab = np.zeros(t.shape[0]) #le tableau de résultats, de taille nb de lignes
    for li in range(t.shape[0]):
        for col in range(t.shape[1]):
            moyTab[li] += t[li,col] #on ajoute dans le tableau de résultats (on "somme") les sommes pour la ligne courante
        moyTab[li] = moyTab[li] / t.shape[1] #après avoir parcouru toutes les colonnes, on divise la somme par le nombre de colonnes
    return moyTab
        
def moyennesParColonnes(t):
    moyTab = np.zeros(t.shape[1]) #le tableau de résultats, de taille nb de lignes
    for col in range(t.shape[1]):
        for li in range(t.shape[0]):
            moyTab[col] += t[li,col] #on ajoute dans le tableau de résultats (on "somme") les sommes pour la colonnes courante
        moyTab[col] = moyTab[col] / t.shape[0] #après avoir parcouru toutes les lignes, on divise la somme par le nombre de lignes
    return moyTab

#--main--
tab = np.array([[1,4,5,3,6,3],[2,5,5,4,9,2],[3,6,2,7,1,4]])
affichageParLignes(tab)
print()
affichageParColonnes(tab)
moyennesParLignes = moyennesParLignes(tab)
moyennesParColonnes = moyennesParColonnes(tab)
print("voici le tableau des moyennes par lignes: ")
print(moyennesParLignes)
print("voici le tableau des moyennes par colonnes: ")
print(moyennesParColonnes)

```
