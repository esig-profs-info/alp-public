---
template: item
title: Les chaînes de caractères
body_classes: theorieStrings
published: true
---

Les chaînes de caractères

===

## Les bases
En Python, une chaîne est écrite avec des guillemets doubles, par ex. "Bonjour", ou alternativement avec des guillemets simples comme 'Salut'. Utilisez la fonction `len(s)` pour obtenir la longueur d’une chaîne, et utilisez des crochets pour accéder aux caractères individuels à l’intérieur de la chaîne. Les caractères sont numérotés à partir de 0, et allant jusqu’à la longueur-1.

```
s = 'Hello'
len(s)   ## 5
## Les caractères sont numérotés à partir de 0
s[0]     ## 'H'
s[1]     ## 'e'
s[4]     ## 'o'  -- dernier caractère à longueur-1
s[5]     ## ERROR, index out of bounds
```

*NB:* Les chaînes Python sont "immutables" ce qui signifie qu’une chaîne ne peut jamais être modifiée une fois créée. Utilisez + entre deux chaînes pour les mettre ensemble pour faire une chaîne plus grande.

```
s = 'Hello'
t = 'Hello' + ' hi!'   ## t vaut 'Hello hi!'
```

##Slicing
Une "slice" (autrement dit, une "tranche") en Python est un moyen puissant de se référer à des sous-parties d’une chaîne. La syntaxe est `s[i:j]`, ce qui signifie que la sous-chaîne commence à l’index i, allant jusqu’à mais n’incluant pas l’index j.
```
s = 'Hello'
#    01234    ## Pour montrer les index de la chaîne 'Hello'
s[1:4]  ## 'ell' -- démarre à 1, jusqu'à mais sans inclure 4
s[0:2]  ## 'He'
```
Si le premier numéro de slice est omis, python utilise simplement le début de la chaîne, et de même si le deuxième numéro de slice est omis, la slice traverse la fin de la chaîne.
```
s = 'Hello'
#    01234
s[:2]  ## 'He', sans le premier numéro de slice --> utilise le début de la chaîne
s[2:]  ## 'llo', sans le second numéro de slice --> utilise la fin de la chaîne
```

Une bonne pratique consiste à utiliser le slicing pour faire référence à des parties d’une chaîne et la concaténation avec `+` pour assembler des parties pour créer de plus grandes chaînes.

```
a = 'Hi!'
b = 'Hello'

# Calculer c comme les 2 premiers caractères de a suivis des 2 derniers caractères de b
c = a[:2] + b[len(b) - 2:] #ou c = a[:2] + b[-2:] (expliqué en dessous...)

```
*NB:* Un mauvais index en Python, par ex. `s[100]` pour la chaîne `"Hello"`, est une erreur d’exécution. Cependant, les coupes fonctionnent différemment. Si un numéro d’index dans une slice est hors limites, il est ignoré et la slice utilise le début ou la fin de la chaîne.

**Index négatif:** Comme alternative, Python prend en charge l’utilisation de nombres négatifs pour indexer une chaîne : -1 signifie le dernier caractère, -2 est l’avant-dernier, et ainsi de suite. En d’autres termes -1 est le même que l’index len(s)-1, -2 est le même que len(s)-2. Les index rendent pratique de se référer aux caractères au début de la chaîne, en utilisant 0, 1, etc. Les nombres négatifs fonctionnent de manière analogue pour les caractères à la fin de la chaîne avec -1, -2, etc. travaillant du côté droit.
```
s = 'Hello'
#   -54321     ## index négatifs illustrés
s[-2:]  ## 'lo', la slice commence depuis le 2e avant la fin
s[:-3]  ## 'He', la slice se finit au 3e avant la fin
```

####Des boucles et des Strings
Une façon de pacrourir une chaîne est d’utiliser la fonction range(n) qui donne un nombre, par ex. 5, retourne la séquence 0, 1, 2, 3, 4 (de 0 à n-1). Ces valeurs fonctionnent parfaitement pour indexer dans une chaîne, de sorte que la boucle `for i in range (len(s))` : boucle la variable i à travers les valeurs d’index 0, 1, 2, ... len(s)-1, essentiellement en regardant chaque caractère une fois.
```
s = 'Hello'
result = ''
for i in range(len(s)):
  # Faire qqch avec s[i], ici simplement ajouter le caractère dans le varaiable result
  result = result + s[i]
```

Puisque nous avons le numéro d’index, i, chaque fois à travers la boucle, il est facile d’écrire la logique qui invoque des caractères près de i, par ex. le char à gauche est à i-1.

Voici Une autre façon de se référer à chaque caractère dans une chaîne:
```
s = 'Hello'
result = ''
for c in s:  ## en bouclant comme ça, c sera 'H', puis 'e', ... 
  result = result + c
```
Cette façon de faire est un moyen facile de regarder chaque caractère dans la chaîne, bien qu’il manque une partie de la flexibilité de la forme i/range() plus haut.


*source: https://codingbat.com/doc/python-strings.html*<br/>
*(beaucoup) plus d'informations sur les slices ici: https://zestedesavoir.com/tutoriels/582/les-slices-en-python/*