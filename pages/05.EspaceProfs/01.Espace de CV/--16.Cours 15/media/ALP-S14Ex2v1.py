# Série 14, Exercice 2, Version 1
# Clément Vogt

# Imports
import numpy as np

# Procédures et fonctions
def moyenne_par_ligne(tab2D):
    liste_notes = []
    nb_lignes = tab2D.shape[0]
    nb_colonnes = tab2D.shape[1]
    for num_lig in range(nb_lignes):
        somme = 0
        for num_col in range(nb_colonnes):
            somme += tab2D[num_lig,num_col]
        moyenne = somme / nb_colonnes
        liste_notes.append(moyenne)
    return liste_notes

def moyenne_par_ligne_v2(tab2D):
    liste = []
    for ligne in tab2D:
        somme = 0
        for element in ligne:
            somme += element
        moyenne = somme / len(ligne)
        liste.append(moyenne)
    return liste

def moyenne_par_colonne(tab2D):
    liste = []
    nb_lignes = tab2D.shape[0]
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        somme = 0
        for num_lig in range(nb_lignes):
            somme += tab2D[num_lig,num_col]
        moyenne = somme / nb_lignes
        liste.append(moyenne)
    return liste
          
# Procédure main()
def main():
    tab2D_notes = np.array([[5,4,3],[6,4.5,5.5],[6,6,6],[2.5,3,2],[4,4.5,4.5]]) 
    moyenne_par_etudiant = moyenne_par_ligne(tab2D_notes)
    moyenne_par_epreuve = moyenne_par_colonne(tab2D_notes)
    print(moyenne_par_etudiant)
    print(moyenne_par_epreuve)
   
# Appel de la procédure main()
if __name__ == "__main__":
    main()