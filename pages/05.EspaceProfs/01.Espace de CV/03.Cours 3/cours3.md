---
template: item
published: true
---

# Cours 3

===

* **Rappels**
    * Formant algorithmique de **séquence**.
    * Formant algorithmique d'**alternative** (test).
        * Une alternative peut être considérée comme une nouvelle (super)**instruction** et, en tant que telle, faire partie d'une séquence.
    * Constantes.
    * Problème du test d'un programme 
    * Démarche de résolution de problèmes
    * Procédures et fonctions
    * Démarche cartésienne
    * Structure utilisée dans le cadre de ce cours pour la création de scripts
    * Portée des variables
* **Emploi des tests et tests imbriqués** (voir [ALP-S2Ex1](./media/ALP-S2Ex1.py)) 
    * Tests en séquence (version **moche**).
    * Il faut éviter d'effectuer des tests inutiles - donc imbriquer les tests.
    * Pour distinguer 3 cas, deux tests suffisent.
    * Le formant de luxe **if ... : ... elif ... : ... else: ...** permet d'abréger l'écriture.
    * On peut obtenir le même résultat en organisant les tests différemment - en particulier, on peut optimiser l'ordre des tests en se basant sur des informations supplémentaires que l'on connaît du problème (ce n'est pas toujours le cas).
    * 
        ```python    
        if <expression_booléenne> :
            <instruction1>
        elif <expression_booléenne> :
            <instruction2>
        else:
            <instruction3>
        ```
* **Procédures et fonctions**
    * Une procédure/fonction modélise une **tâche**. Elle est le résultat de l'application de la démarche cartésienne.
    * On ne met jamais dans la même procédure/fonction des calculs et des affichages
    * **Exemples d'emploi:**
        * Solution de l'exercice T1 de la série 2. voir [ALP-S2ExT1](./media/ALP-S2ExT1.py)
        * Solution de l'exercice 1 de la série 3. Illustre aussi l'emploi de [math.pi](https://docs.python.org/fr/3/library/math.html#math.pi): voir [ALP-S3Ex1.py](./media/ALP-S3Ex1.py).
        * Utilité des paramètres: voir [ALP-S3Ex1v2](./media/ALP-S3Ex1v2.py)
    * **<u>Remarque:</u>** la décomposition en tâches (et donc en procédures et fonctions) est l'application d'une méthodologie dont l'application est la seule manière d'arriver à résoudre des problèmes beaucoup plus complexes que les problèmes qui sont posés dans les exercices. Il est donc essentiel que vous abordiez le problème de la résolution de vos exercices en appliquant cette démarche. **Dans les épreuves, votre capacité à mettre en œuvre cette démarche sera évaluée.**
* **Importation d’un module**
    * Un module est un regroupement d’un ensemble de fonctions prêtes à être utilisées par des applications
    * [La bibliothèque standard](https://docs.python.org/fr/3/library/)
    *
        ```python 
        import <nom du module>
        ```
    * <u>Illustration :</u> [ALP-S3Ex1.py](./media/ALP-S3Ex1.py)
* [Illustration des notions](./media/illustration-c03.py)

**<u>Exercices :</u>** [Série 3](/exercices/code/serie%203)
