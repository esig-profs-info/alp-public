---
template: item
published: true
---
# TP Rattrapage - partie A


L'inspiration de ce TP vient des rattrapages d'épreuve que certain·e·s ont dû passer fin novembre.

Voici quelques notions de programmation abordées au cours des deux parties de ce TP :
1. principalement les dictionnaires en Python
    * association entre une valeur simple (chaine) et une autre valeur simple (nombre)
    * association entre une valeur simple (chaine) et une liste
2. mais aussi la création de listes et de dictionnaires
3. et un peu de manipulation de chaînes de caractères.


### Eléments à disposition

Vous noterez que il y a deux fichiers [Rattrapage.py](Rattrapage.py) et [RattrapageHelper.py](RattrapageHelper.py). Le premier est le fichier à compléter ; l'autre contient des fonctions à utiliser et les données. Mais grâce à la clause import dans [Rattrapage.py](Rattrapage.py), vous avez accès à tout dans le fichier à compléter !

## Partie A - Durée des épreuves de rattrapage

Dans cette partie I, nous allons travailler sur un dictionnaire qui associe un cours, c'est-à-dire une épreuve de rattrapage, à sa durée en minutes. Voici les données :

| Cours   | Durée |
|---------|------:|
| FRTC    | 90    |
| MODE    | 180   |
| ALP     | 240   |
| MIRESE  | 90    |
| TEPR    | 90    |
| TQGE    | 60    |
| DROIT   | 90    |
| GESTION | 60    |
| DWEB    | 120   |

Le dictionnaire est déjà créé et s'appelle `dicoDurées`. <font size="2">Vous pouvez le renommer en enlevant l'accent si cela vous gêne.</font>

### Exo A.1 - interaction (simple)
 Définir une procédure Python qui demande à l'utilisateur de saisir un nom de cours et qui affiche sa durée ou un message si le cours est inconnu.
 
### Exo A.2 - obtenir la liste des cours (simple)
 Définir une procédure Python qui affiche la liste des cours, un par ligne.
 
### Exo A.3 - trouver la durée la plus longue
 Définir une fonction Python qui détermine la durée d'épreuve la plus longue et une procédure qui teste cette fonction (appel+affichage du résultat)
 
 *Supplément* : La procédure qui teste affiche la durée *et* le nom du cours correspondant à l'épreuve la plus longue. 
 
 *Coup de pouce* : Grâce au dictionnaire, votre fonction de calcul, même avec le supplément, peut ne renvoyer qu'une valeur.
 
 *Info* : la réponse est 240. Le cours est... ALP !
 
### Exo A.4 - faire la somme des durées
 Ecrire une fonction Python qui calcule la somme des durées de toutes les épreuves. Ecrire une procédure qui teste, c'est-à-dire appelle la fonction qui calcule et affiche le résultat qu'elle renvoie.
  
  *Info* : la réponse est 1020. 
  
### Exo A.5 - compter les durées
 Ecrire une procédure Python qui demande une durée et qui affiche le nombre d'épreuves ayant cette durée.
  
  *Info* : Voici les résultats pour chaque durée dans `dicoDurées` (l'ordre des durées peut être quelconque) :
  
  | durées | nombre |
|:------:|:------:|
|     90 |      4 |
|    180 |      1 |
|    240 |      1 |
|    120 |      1 |
|     60 |      2 |
  
### Exo A.6 - inverser le dictionnaire pour compter les durées
 L'objectif est ici d'obtenir un nouveau dictionnaire qui associe à chaque durée distincte le nombre d'épreuves ayant cette durée.
 C'est une généralisation de A.5.
  
 Une fonction Python devra renvoyer ce nouveau dictionnaire et une procédure devra la tester (appel+affichage).
  
 Le principe général est de parcourir `dicoDurées` et d'obtenir la durée associée à chaque clé. Il faut alors regarder si la durée est déjà dans le nouveau dictionnaire. 
  * Si ce n'est pas le cas, il faut ajouter une nouvelle association entre cette durée et 1 (puisqu'on vient de rencontrer 1 épreuve ayant cette durée, la première). 
  * En revanche, si la durée est déjà répertoriée dans le nouveau dictionnaire, il suffit de récupérer le nombre associé, de l'incrémenter de 1 (car on a rencontré une épreuve de plus ayant cette durée) et changer la valeur associée à la durée dans le nouveau dictionnaire.
  
 *Coup de pouce* : Si vous avez des difficultés avec ce raisonnement, servez-vous de la boucle suivante pour initialiser le nouveau dictionnaire :
  
  ```
  for xxx in set(dicoDurées.values()):
    # associez la clé xxx à 0 dans le nouveau dictionnaire
  ```
  qui permet de parcourir les valeurs de durée *distinctes* dans ```dicoDurées```. Ainsi toutes les clés possibles du nouveau dictionnaire seront connues et initialisées à zéro.

<!--  
### Exo A.7 - inverser le dictionnaire pour lister les cours
 En vous inspirant de l'exercice précédent, écrivez une fonction Python qui devra renvoyer un nouveau dictionnaire et une procédure qui devra la tester (appel+affichage).
  
 Ce nouveau dictionnaire associera les durées (qui deviennent des clés, comme dans l'exercice précédent) avec la *liste* des cours ayant cette durée.

## Partie A - petit casse-tête sur les chaînes de caractères
---------------------------------------------------------------------

### Exo A.8 - "brouillage" d'une chaîne ou *scrambling*

 Vous verrez dans la partie B que les noms des étudiants sont très bizarres. Ils ont été obtenus en *brouillant* les vrais prénoms.
 Le principe du *scrambling* est similaire au *floutage* des visages sur des photos : on voit qu'il y a des personnes mais on ne saurait pas les reconnaître. De même, quand on fait du *data scrambling*, on rend les données de départ méconnaissables.

 Un brouillage a déjà été fait sur les données de la partie B mais nous avons pensé que ça vous amuserait de reproduire la méthode utilisée.

 Cette méthode est simple : pour chaque lettre du nom à brouiller, on remplace :
  * une lettre majuscule par une lettre majuscule
  * une lettre minuscule par une lettre minuscule
  * une voyelle par une voyelle tirée au hasard
  * une consonne par une consonne tirée au hasard

 Pour faire cela, on fournit (dans [RattrapageHelper.py](RattrapageHelper.py)) les fonctions suivantes :
  * ```voyelleAleatoire()```
  * ```consonneAleatoire()```

 et les listes suivantes :
  * ```LISTE_VOYELLES```
  * ```LISTE_CONSONNES```

 Ecrire une fonction Python qui prend en paramètre une chaîne et qui en renvoie une autre après *scrambling* selon les règles ci-dessus.
 Ecrire une procédure de test qui demande une chaîne et affiche le résultat de la fonction de *scrambling*.
-->