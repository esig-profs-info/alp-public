---
template: item
---

# Log ALP (groupe 2)
_Cette page présente les sujets traités, chaque semaine depuis la semaine 46, dans le cours ALP du groupe 2_.

## Semaine 50 (du 07 au 11 décembre)
* correction ER Nov + TP plongée + Exo Chaines de caractères
#### Notions abordées en cours:
* chaine de caractère ressemble à liste de caractère mais différences:
	* immutable chaine[2] = 'b' n'est pas possible
	* ajout de caractère en fin de chaine avec + (concaténation) / pas de append()

###What else:
* corrigé exo 1 CdC publié: [https://www.youtube.com/watch?v=uzLOr64FA0M](https://www.youtube.com/watch?v=uzLOr64FA0M)


## Semaine 49 (du 30 au 04 décembre)
* correction ER Nov + TP plongée + Exo Chaines de caractères
#### Notions abordées en cours:


## Semaine 48 (du 23 au 27 novembre)
* Listes + Slicing + Exo Chaines de caractères
#### Notions abordées en cours:


## Semaine 47 (du 16 au 20 novembre)
#### Notions abordées en cours:
* Connaître, comprendre et savoir expliquer/utiliser Algorithme du décompte
* Connaître, comprendre et savoir expliquer/utiliser Algorithme de la somme
* Connaître, comprendre et savoir expliquer/utiliser Algorithme de la moyenne
* Connaître, comprendre et savoir expliquer/utiliser Algorithme du minimum
* Connaître, comprendre et savoir expliquer/utiliser Algorithme du maxmimum
* Savoir calculer une moyenne pondérée selon différents critères
* Savoir utiliser 3 types de boucles pour parcourir une liste (ou non d'ailleurs):
    * "foreach" --> ```for el in liste:```
    * "while" --> ```while i < len(liste):```
    * "for" --> ```for i in range(len(liste)):```
* Savoir choisir entre les 3 boucles ci-dessus selon la circonstance
* modulo = reste de la division entière, utile par exemple pour checker la parité
* slicing (pour partie 3) : voir [http://esig-sandbox.ch/alp/ressources/quick%20reference](http://esig-sandbox.ch/alp/ressources/quick%20reference)
* moyenne pondérée: exemple algorithme avec notes > 4 qui comptent double

### Rythme à tenir pour cette semaine (47):
* Terminer (seul-e) les exercices listes partie 1 et 2 et 3.
* Commencer le [TP2-association-plongee](https://esig-sandbox.ch/alp/tps/tp-2-association_plong%C3%A9e)
* PyCharm avec GitHub (pas obligatoire)

### What else?
[abonnez-vous à la chaîne youtube du cours](https://www.youtube.com/channel/UC42Va5YtOn8H8M1vpXpfvFw)

<hr/>

## Semaine 46 (du 9 au 13 novembre)
#### Notions abordées en cours:
* Savoir créer un projet **PyCharm**, savoir choisir (au besoin installer) un interpréteur python, dans un environnement virtuel (venv) ou dans un environnement local.
* Savoir ouvrir un projet PyCharm fourni par l'enseignant.
* Savoir utiliser git pour récupérer des projets ou des scripts, en ligne de commande (Git Bash) ou directement dans PyCharm (NON SUJET A EXAMEN!)
* Savoir exécuter un script .py depuis PyCharme (clic droit / run ou add Configuration en haut).
* Concernant les listes: 
    * Maîtriser la notion d'indice (index en anglais).
    * Maîtriser la fonction len().
    * Connaître la notion de range (que veux dire "index out of range" par exemple?).
    * Savoir que le premier élément d'une liste se trouve à l'indice _0 (zéro)_ et que le dernier se trouve à l'indice _len(liste)-1_.
    * Savoir que le language python permet d'utiliser des indices négatifs qui permettent de "compter depuis la fin", par exemple le dernier élément d'une liste peut aussi être atteint en utilisant l'indice _-1_, l'avant-dernier _-2_, etc.
    * Savoir que lorsque l'on définit une fonction qui utilise une liste passée en paramètre, il est souvent conseillé d'effectuer quelques vérifications (longueur de la liste par exemple) afin de ne pas se trouver confrontés à des erreurs comme _"index out of range"_.
* Savoir reconnaître dans un code source fourni des répétitions (redondance)...qui sont un des pires énemis du développeur.
* Savoir utiliser des fonctions ou des structures comme des boucles permettant d'éviter des répétitions (redondance) --> comme un fonction d'affichage par exemple (cf exo liste 1, corrigé intermédiaire et optimal).
* Savoir utiliser le débugger dans PyCharm, notamment voir l'état des variables en cours d'exécution, définir un point d'arrêt et connaître la différence entre "step over" et "step into".

### Rythme à tenir pour cette semaine (46):
* Terminer (seul-e) les exercices listes partie 1 et 2.
* Prendre connaissance et comprendre le corrigé liste partie 1 disponible sur le repo de distribution (bitbucket).
* PyCharm installé et utilisable à la maison.

### What else?
* C'est tout... pour le moment.

<hr/>
