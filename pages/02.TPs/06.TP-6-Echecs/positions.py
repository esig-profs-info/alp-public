# Un dictionnaire qui contient tous les symboles admis pour une ligne d'une position FEN
# avec le nombre de cases qu'ils occupent. Pour vérifier si une lettre
DICT_SYMBOLE_NBCASES = {'1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8,
                        'p': 1, 'r': 1, 'n': 1, 'b': 1, 'q': 1, 'k': 1,
                        'P': 1, 'R': 1, 'N': 1, 'B': 1, 'Q': 1, 'K': 1}

if __name__ == '__main__':
    fens_valides = ["rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
                    "8/8/5k2/6qK/8/8/8/8 w - - 0 1",
                    "r1bqkbnr/pppp1ppp/2n5/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 3 3"]

    fens_invalides = [
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPZPP/RNBQKBNR w KQkq - 0 1",  # symbole invalide 'Z'
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKNR w KQkq - 0 1",  # ligne RNBQKNR pas de longueur 8
        "8/8/5k1/6qK/8/8/8/8 w - - 0",  # ligne 5k1 pas de longueur 8
        "8/8/5k2/6qK/8/8/8 w - - 0 1",  # manque une ligne
        "8/8/5k2/7qK/8/8/8/8 w - - 0 1",  # ligne 7qK pas de longueur 8
        "rnbqkbnr/ppppppppp|8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",  # '|' à la place '/'
        "r1bqkbnr/pppp1.0ppp/2n5/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 3 3",  # symboles invalides '.' et '0'
        "rnbqkbnr/pppppppp/8/8/10/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"]  # ligne '10' pas de longueur 8 et symbole '0'



