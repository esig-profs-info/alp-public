---
template: item
published: true
---

# Cours 11-12

===

* **Les chaînes de caractères**
    * [Ressource](/ressources/chaines%20de%20caracteres)
    * Ressources supplémentaires
        * [w3schools](https://www.w3schools.com/python/python_strings.asp)
        * [Pour approfondir le slicing](https://stackoverflow.com/questions/509211/understanding-slice-notation)
    
**<u>Exercices :</u>** [Série 11](/exercices/code/serie%2011) et [Série 12](/exercices/code/serie%2012)