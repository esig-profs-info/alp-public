from gturtle import * 

makeTurtle()

def creerMarche():
    fd(20)
    rt(90)
    fd(20)
    lt(90)

def creerEscalier(nbMarches):
    repeat nbMarches: 
        creerMarche()

creerEscalier(7)

