# Série 5, Exercice T4v2
# Clément Vogt

# Imports
from turtle import *

# Procédures et fonctions
def deplacer_ver_position_initiale():
    hideturtle() #pour éviter de voir la tortue bouger
    penup() #pour éviter un trait non demandé
    setpos(-400, 0) #pour démarrer plus à gauche
    pendown() #pour réactiver le dessin de la tortue
    showturtle() #pour afficher la tortue elle-même       

def un_trait():
    color('blue')
    dot(10)
    forward(50)
    dot(10)

# Procédure main()
def main():
    nb_traits = int(input("Entrez un nombre de traits"))
    
    deplacer_ver_position_initiale()
    
    for i in range(0,nb_traits):
        if i % 2 == 0:
            left(45)
            un_trait()
        else:
            right(90)
            un_trait()
            left(45)

# Appel de la procédure main()
main()