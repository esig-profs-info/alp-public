---
template: blog
content:
    items: '@self.children'
    order:
        by: folder
        dir: asc
---
# Exercices de programmation en python
