---
template: item
published: true
---

#TP2 - miniTP - liste

===

[fichier source](./minitp2.py)

## Votre travail:
compléter le fichier source aux enbdroits marqués "todo"

Toutes les informations utiles sont dans les commentaires

sorties produites:
```
la liste [1,2,3,4,5,6,7,8,9,10] contient 3 nombres plus petits que la valeur pivot 4 et 6 plus grands
la liste [0,1,7,1,5,9,2,6,5,3,34,0,23,11,75,23,55,4,12,10,4,67,99] contient 6 nombres plus petits que la valeur pivot 4 et 15 plus grands
la liste [1000,32,-10,0,9012,234,642,9,-9,-10,98,22,-33] contient 5 nombres plus petits que la valeur pivot 4 et 8 plus grands
```
