---
template: item
published: true
---

# Cours 4

===

* **Démarche de résolution de problèmes** (rappel)
    * [Document](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)
    * Mise en œuvre: correction d'exercices des Série 3 et 4 (voir [ALP-S3Ex2](./media/ALP-S3Ex2.py) et [ALP-S4Ex1](./media/ALP-S4Ex1.py)).

**<u>Exercices :</u>** [Série 4](/exercices/code/serie%204)