---
template: item
published: false
---

Un expression logique est une expression qui s'évalue soit à `True` soit à `False`. On les utilise comme condition pour les instructions `if` ou `while`.
Par exemple:
```python
if age >= 21 and age <= 65:
 ...
 ```
Nous pourrions également écrire ceci de la manière suivante:
```python
a = age >= 21
b = age <= 65
if a and b:
...
```

## Tables
Pour chacune des expressions logiques ci-dessous, créez leur table de vérité.

Par exemple, pour l'expression `a and b`, table de vérité s'écrit comme suit:

| a     | b     | a and b |
|-------|-------|----------|
| True  | True  | True     |
| True  | False | False    |
| False | True  | False    |
| False | False | False    |


1. `schtroumpf_bleu and schtroumpf_rouge`
1. `a or b`
1. `not a`
1. `a and not b`
1. `(a and b) or c` 
1. `a or b and not c`
1. `(a or b) and not c`
1. `(a and not b) or (b and not a)`

Pour les expressions composées, il est possible d'écrire des tables de vérité avec plus de colonnes afin de décomposer, par exemple, pour la dernière ci-dessus:

| a     | b     | a and not b | b and not a |(a and not b) or (b and not a) |
|-------|-------|----------|-------|-------|----------|
| True  | True  | False     | False | False


## Avec des valeurs
Nous pouvons utiliser des tables de vérité pour comprendre et vérifier des expressions logiques utilisant de comparateurs.
Pour quelles valeurs des variables ces expressions seront `True`?

1. `(nombre > 50 and nombre < 60) or isInteger(nombre/5)`
2. `a > 50 or not b < 60 and a > b`
3. `not c == b and b > a and c == 30 or b == -1`
4. `d*d == e and not e > d and d >= 0`

## Est-ce possible?

Pour chacune des expression ci-dessous, dites si leur résultat vaut forcément True, False, ou si c'est inconnu (dépendant de la valeur des variables). Pour pouvoir y répondre, le plus sûr est de créer la table de vérité et vérifier les résultats possibles.
1. `to_be or not to_be`
1. `a and not a`
1. `(a or not b) and b`
1. `(a or not b) or b`
1. `(nombre > 10) and (nombre < 10)`
1. `(nombre > 10) or (nombre < 10)`




