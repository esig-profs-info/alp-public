# Série 8, Exercice 3, Version 2
# Clément Vogt

# Procédures et fonctions
# En définissant une méthode générale d'affichage d'un caractère un certain nombre de fois
# on se donne finalement les moyens, non seulement de résoudre notre problème des 4 triangles
# avec une seule procédure, mais encore on peut faire face facilement à des changement de
# spécifications et/ou résoudre d'autres problèmes du même type (p. ex: losange).
# Procédures et fonctions
def donnees_valides(type_triangle,nb_lignes):
    if type_triangle >= 1 and type_triangle <= 5 and nb_lignes > 0:
        return True
    print("Vos données sont invalides.")
    return False

def afficher_char(ch,nb_fois):
    for cpt in range(1,nb_fois+1):
        print(ch,end='')
        
def triangle1(nb):
    for num_lig in range(1,nb+1):
        afficher_char("*",num_lig)
        print()
        
def triangle2(nb):
    for num_lig in range(1,nb+1):
        afficher_char("*",nb - num_lig + 1)
        print()
    
def triangle3(nb):    
    for num_lig in range(1,nb+1):
        afficher_char(" ",nb - num_lig)
        afficher_char("*",num_lig)
        print()
    
def triangle4(nb):    
    for num_lig in range(1,nb+1):
        afficher_char(" ",num_lig - 1)
        afficher_char("*",nb - num_lig + 1)
        print()

def losange(nb):
    nombre = nb
    if nb % 2 == 0:
        nombre = nombre + 1 # Remarque: ne jamais modifier les données du problème directement, d'où la variable nombre
        print("nb doit être impair, on fait donc un losange de ",nombre," lignes.",sep='')
        
    moitie = nombre // 2 + 1
    
    for num_lig in range(1,moitie+1):
        afficher_char(" ",moitie - num_lig)
        afficher_char("*",2 * num_lig - 1)
        print()
    
    for num_lig in range(moitie+1,nombre+1):
        afficher_char(" ",num_lig - moitie)
        afficher_char("*",2 * (nombre - num_lig + 1)-1)
        print()

# Procédure main()
def main():
    type_triangle = int(input("Entrez un type de triangle"))
    nb = int(input("Entrez un nombre de lignes"))
    if donnees_valides(type_triangle,nb):
        if type_triangle == 1:
            triangle1(nb)
        elif type_triangle == 2:
            triangle2(nb)
        elif type_triangle == 3:
            triangle3(nb)
        elif type_triangle == 4:
            triangle4(nb)
        else:
            losange(nb)

# Appel de la procédure main()
if __name__ == "__main__":
    main()