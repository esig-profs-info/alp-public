# Série 5, Exercice 4
# Clément Vogt

# Procédures et fonctions
def afficher_terme(n,numero):
    resultat = numero * n
    print(numero," x ",n, " = ", resultat,sep='')

# Procédure main()
def main():
    k = int(input("Entrez une valeur pour k"))
    n = int(input("Entrez une valeur pour n"))
    
    print("Livret de ",n,sep='')
    for nb in range(1,k+1):
        afficher_terme(n,nb)

# Appel de la procédure main()
if __name__ == "__main__":
    main()