# Série 9, Exercice T1
# Clément Vogt

# Imports
from turtle import *

# CONSTANTES
COULEURS = ['yellow', 'green', 'blue', 'red']

# Procédures et fonctions
def initialisation():
    speed(-1)

# def couleur_suivante(couleur):
#     if couleur == COULEURS[0]:
#         couleur = COULEURS[1]
#     elif couleur == COULEURS[1]:
#         couleur = COULEURS[2]
#     elif couleur == COULEURS[2]:
#         couleur = COULEURS[3]
#     else:
#         couleur = COULEURS[0]   
#     return couleur
 
# Procédure main()
def main():
    longueur_initial = int(input("Entrez une longueur initiale"))
    increment = int(input("Entrez un incrément"))
    longueur_maximale = int(input("Entrez une longueur maximale"))
    
    initialisation()
    
    # couleur = COULEURS[0]
    longueur = longueur_initial
    cpt = 0
    while longueur <= longueur_maximale:
        color(COULEURS[cpt % len(COULEURS)])
        forward(longueur)
        right(90)
        # couleur = couleur_suivante(couleur)
        longueur += increment
        cpt+=1
#         if cpt == len(COULEURS):
#             cpt = 0
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()