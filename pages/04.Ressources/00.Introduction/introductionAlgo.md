---
template: item
title: Introduction (définition algorithme)
---

Quelques ressources pour comprendre ce qu'est un algorithme

===

#### Une définition introductive sur le site pise.info:
[voir la page](http://pise.info/algo/introduction.htm?target=_blank)

#### Une définition plus avancée:
[voir la page](http://villemin.gerard.free.fr/Wwwgvmm/Logique/IAalgorD.htm?target=_blank)

#### Une interview de la RTS très intéressante (CQFD):
![écouter l'interview](cqfd-cest_quoi_un_algorithme.mp3?autoplay=0)