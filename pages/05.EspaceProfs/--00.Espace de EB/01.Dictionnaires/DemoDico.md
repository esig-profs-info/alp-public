---
template: item
published: true
---

# Pour accéder à la démonstration sur les dictionnaires


###### *la nouveauté*
* Fichier notebook lui-même : [notebook sur les *dictionnaires*](./media/Démo_dico.ipynb)
* Le pdf (non interactif !) du notebook : [pdf du notebook](media/Démo_Dico%20%28Notebook%29.pdf)

<br>

###### *au besoin - même chose que pour le slicing*
* Fichier .pdf  : [Instructions à suivre pour l'installation](../00.Slicing/media/Comment%20exécuter%20un%20notebook%20Jupyter.pdf) 
* Fichier à décompacter selon les instructions : [EduPython version 3.0, lien sur le Drive eduge.ch](https://drive.google.com/file/d/11v2ExZy0WKVz772SxjSYR0JoERj0KI1i/view?usp=sharing) <br> ou
    [site de téléchargement au besoin](https://edupython.tuxfamily.org/)

