---
template: item
published: true
---

# Cours 5

===

* **Fonction [range()](https://docs.python.org/fr/3/library/stdtypes.html#range)**
    * La fonction range() renvoie une séquence de nombres.
    * Syntaxe : **range(start, stop, step)**
    * Paramètres :
        * **start** (optionnel) : Un nombre entier spécifiant la valeur de départ. La valeur par défaut est 0.
        * **stop** (obligatoire) : Un nombre entier spécifiant la valeur de fin. Cette valeur n’est pas incluse dans la séquence.
        * **step** (optionnel) : Un nombre entier spécifiant l'incrémentation (ou la décrémentation…). La valeur par défaut est 1.
    * Attention, la dernière valeur d’un range est toujours la valeur qui précède celle spécifiée par le paramètre stop.   range(1,10) s’arrête à 9 et range(10,1,-1) s’arrête à 2.
* **Boucle for**
    * Une boucle permet de répéter un certain nombre de fois l'exécution d'une tâche. Cette tâche peut être une instruction élémentaire ou alors une tâche plus complexe.
    * La boucle for permet de parcourir une séquence contenant plusieurs éléments.
    * 
        ```python
        for élément in séquence :
            <instructionN>
        ```
    * <u>Rappel:</u> **%**
    * Illustration de **for** et **%** en corrigeant les exercices 4 et T4 de la Série 5
        * Voir [ALP-S5Ex4](./media/ALP-S5Ex4.py)
        * Voir [ALP-S5ExT4](./media/ALP-S5ExT4.py) et [ALP-S5ExT4v2](./media/ALP-S5ExT4v2.py)
* [Illustration des notions](./media/illustration-c05.py)

**<u>Exercices :</u>** [Série 5](/exercices/code/serie%205)