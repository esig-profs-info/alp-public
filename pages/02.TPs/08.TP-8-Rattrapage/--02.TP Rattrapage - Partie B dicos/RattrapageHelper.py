from random import choice

LISTE_VOYELLES = ['A', 'E', 'I', 'O', 'U', 'Y']
LISTE_CONSONNES = sorted(list(set( chr(c) for c in range(ord('A'), ord('Z')+1) )-set(LISTE_VOYELLES)))

dicoDurées = {'FRTC':    90, 'MODE':    180, 'ALP': 240, 'MIRESE' :  90,
              'TEPR':    90, 'TQGE':    60, 'DROIT' :   90, 'GESTION' : 60,
              'DWEB' :    120}

dicoNoms = {'ALP' : ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 'AREGAZRRO', 'HUJICY', 'YVEWOW', 'MIJGYH'],
'MIRESE' : ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH'],
'MODE' : ['LJUOSOP', 'HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'ZEEJ'],
'DROIT' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ'],
'DWEB' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'BUQOH-AKHSO', 'GUFUJU', 'SIO', 'ZEEJ'],
'FRTC' : ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 'AREGAZRRO', 'HUJICY', 'YVEWOW', 'MIJGYH'],
'TQGE' : ['LJUOSOP', 'HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ'],
'GESTION' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ'],
'TEPR' : ['GUKO-PAYQYJ', 'LJUOSOP', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'RYHXAH', 'ZETHODE', 'VEMI', 'UDVO', 'BUQOH-AKHSO', 'MIJGYH', 'SIO', 'ZEEJ']}


def scramble(s):
    sScrambled = ''
    for car in s:
        if estVoyelle(car.upper()):
            carAlea = voyelleAleatoire()
        elif estConsonne(car.upper()):
            carAlea = consonneAleatoire()
        else: # autre caractère
            carAlea = car
        if car.isupper():
            sScrambled += carAlea.upper()
        else:
            sScrambled += carAlea.lower()
    return sScrambled


def estVoyelle(lettre):
    return lettre in LISTE_VOYELLES

def estConsonne(lettre):
    return lettre in LISTE_CONSONNES
            
def voyelleAleatoire():
    return lettreAleatoireDans(LISTE_VOYELLES)

def consonneAleatoire():
    return lettreAleatoireDans(LISTE_CONSONNES)

def lettreAleatoireDans(liste):
    return choice(liste)

