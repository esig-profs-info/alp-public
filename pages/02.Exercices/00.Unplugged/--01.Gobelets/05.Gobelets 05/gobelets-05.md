---
template: item
published : true
---

Activité numéro 5 avec des gobelets, pour corser (encore) un peu l'affaire et utiliser des fonctions, des paramètres et des structures conditionnelles.

===

##Exercices - Gobelets
###Activité n°5 - Simplification du code
#### Contexte:
* Activité par groupe de 2

#### Concernant la correction de l'activité 4:
Pas de correction fournie, vous devez trouver un consensus dans la classe (avec l'aprobation de votre enseignant).


#### Partie 1
Trouvez le résultat des algorithmes suivants, puis, à l'aide de fonctions, de paramètres, de boucles et de structures conditionnelles, simplifiez **au maximum** le code

##### Algo 1
```
↑ ↺ → → ↓ ← ←
↑  → → ↓ ← ←
↑ ↺ → → → → ↓ ← ← ← ←
↑ → → → → ↓ ← ← ← ←
↑ ↺ → → → → → → ↓ ← ← ← ← ← ←
↑ → → → → → → ↓ ← ← ← ← ← ←
↑ ↺ → → → → → → → → ↓ ← ← ← ← ← ← ← ←
↑ → → → → → → → → ↓ ← ← ← ← ← ← ← ←
```

##### Algo 2
```
↑ → → ↓ ← ←
↑ → → → → ↓ ← ← ← ←
↑ → → → ↓ ← ← ←
↑ ↺ → → → ↓ ← ← ←
↑ → → → → → → ↓ ← ← ← ← ← ←
↑ → → → → → → → → ↓ ← ← ← ← ← ← ← ←
↑ → → → → → → → ↓ ← ← ← ← ← ← ←
↑ ↺ → → → → → → → ↓ ← ← ← ← ← ← ←
↑ → → → → → → → → → → ↓ ← ← ← ← ← ← ← ← ← ←
↑ → → → → → → → → → → → → ↓ ← ← ← ← ← ← ← ← ← ← ← ←
↑ → → → → → → → → → → → ↓ ← ← ← ← ← ← ← ← ← ← ←
↑ ↺ → → → → → → → → → → → ↓ ← ← ← ← ← ← ← ← ← ← ←
```
[c](corrige_gobelets_05_partie1_photo2.pdf)

#### Partie 2
Ecrivez un code le plus efficace possible à votre goût pour obtenir le résultat suivant:
![photo pyramide gobelets 05](pyramide_gobelets_05.jpg?cropResize=350,350)