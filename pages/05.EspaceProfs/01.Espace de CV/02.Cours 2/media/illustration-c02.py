# Cours 2 - Illustration
# Clément Vogt
print("\n---------------------------\nCours 2 - Illustration\n---------------------------")

# ---------------------------------------------------------------------------
# Alternative: Emboîtement, principe des poupées russes.
# ---------------------------------------------------------------------------
print("\nAlternative: Emboîtement, principe des poupées russes.")

print("DEBUT")
mon_chiffre = 2
if mon_chiffre == 2:
    print("La valeur de mon_chiffre est égal à 2")
else:
    if mon_chiffre >= 10:
        print("La valeur de mon_chiffre est supérieure ou égale à 10")
    else:
        print("La valeur de mon_chiffre est inférieure à 10 et est différente de 2")
print("FIN")

# ---------------------------------------------------------------------------
# Structure utilisée dans le cadre de ce cours pour la création de scripts
# ---------------------------------------------------------------------------
print("\nStructure utilisée dans le cadre de ce cours pour la création de scripts")

# Imports (facultatif): D'éventuels imports de module que vous auriez besoin dans le contexte du problème
import math

# Constantes (facultatif): Constantes : D'éventuelles constantes que vous auriez besoin de créer dans le contexte du problème.
MA_CONSTANTE = ""

# Procédures (facultatif): D'éventuelles procédures ou fonctions que vous auriez besoin de créer dans le contexte du problème.
def ma_procedure():
    print("Ma procédure")
    
# Procédure main() (obligatoire): Point d'entrée pour l'exécution de votre script
def main():
    print("Instructions...") # Vos instructions....
    ma_procedure()
    
# Appel de la procédure main() (obligatoire) => Doit toujours être la dernière instruction de votre script.
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Procédures sans paramètres
# ----------------------------------------------------------------------------
print("\nProcédures sans paramètres")

def ma_procedure(): # Déclaration de la procédure
    print("Ma procédure")

def main():
    ma_procedure() # Appel de la procédure
    
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Procédures avec paramètres
# ---------------------------------------------------------------------------
print("\nProcédures avec paramètres")

def ma_procedure(p1,p2): # p1, p2 sont des paramètres d'entrée, appelés paramètres formels. Ces derniers sont les valeurs que la procédure doit recevoir, c’est avec ces valeurs qu’elle effectue ses calculs et ses traitements. 
    print(p1,"et",p2)

def main():
    ma_procedure("Clément","Vogt") # Clément et Vogt sont les paramètres effectifs. Lors de l'appel de la procédure, il faut lui fournir comme paramètres les valeurs avec lesquelles elle doit effectivement travailler: les paramètres effectifs
    prenom = "Clément"
    nom = "Vogt"
    ma_procedure(prenom,nom) # il est possible de désigner une variable comme paramètre effectif lors de l'appel de la procédure. Cependant, c'est bien la valeur contenue dans la variable qui est transmise à la procédure et non la variable elle-même.

if __name__ == "__main__":
    main()
    
# ---------------------------------------------------------------------------
# Fonctions sans paramètres
# ---------------------------------------------------------------------------
print("\nFonctions sans paramètres")

def ma_fonction(): # Déclaration de la fonction
    return "Ma fonction" # Retourne le résultat à la procédure appelante

def main():
    valeur_retournee = ma_fonction() # Appel de la fonction. Le résultat retourné par la fonction ma_fonction() est stocké dans une variable nommée valeur_retournee 
    print(valeur_retournee) 
    
if __name__ == "__main__":
    main()
    
# ---------------------------------------------------------------------------
# Fonction qui retourne plusieurs résultats
# ---------------------------------------------------------------------------
print("\nFonction qui retourne plusieurs résultats")

def ma_fonction():
    prenom = "Clément"
    nom = "Vogt"
    return prenom, nom # Pour retourner plusieurs valeurs il faut les séparer par des virgules

def main():
    p, n = ma_fonction() # Pour stocker chaque valeur retournée dans une variable, il faut déclarer autant de variables qu'il y a de valeurs retournées et les séparer par des virgules.
    print("Prénom: ", p, " - Nom: ",n,sep="")
    
if __name__ == "__main__":
    main()    

# ---------------------------------------------------------------------------
# Portée des variables
# ---------------------------------------------------------------------------
print("\nPortée des variables")

MARGE_SOUHAITEE = 0.2 # La constante MARGE_SOUHAITEE est globale car elle est déclarée au niveau du script, il est donc possible d'y accéder partout dans le programme (pour autant qu'elle ait été déclarée avant...). En général, les constantes sont toujours globales et se déclare en tête de script.

ma_variable_globale="Je suis une variable globale" # La variable ma_variable_globale est globale car elle est déclarée au niveau du script, il est donc possible d'y accéder partout dans le programme (pour autant qu'elle ait été déclarée avant...).

def main():
    prix_revient = 10 # La variable prix_revient est une variable locale à la procédure main() car elle a été déclarée à l'intérieur de cette dernière. Elle ne sera donc visible que dans cette procédure (dans l'espace local).
    prix_vente_ht = prix_revient + MARGE_SOUHAITEE
    print("Le prix de vente est ", prix_vente_ht," CHF.",sep="")
    # global ma_variable_globale # => Utilisation du mot-clé global afin de pouvoir modifier la valeur d'une variable globale en dehors de l'espace où elle a été déclarée. En effet, les variables globales sont accessibles en modification uniquement dans leur espace, si on y accède depuis un autre espace, elles sont  accessibles en lecture. Si l'on souhaite les modifier en dehors de l'espace où elles ont été déclarées, il faut utiliser le mot clé global.
    # ma_variable_globale = "Je suis une variable globale trop cool!"
    # print(ma_variable_globale) # Affichage de ma_variable_globale

if __name__ == "__main__":
    main()

print(ma_variable_globale)