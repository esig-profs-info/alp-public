---
template: item
title: Numpy
body_classes: theorieNumpy
published: true
---

Numpy

===

## Voici quelques exemples:

```
import numpy as np

tab2D = np.array([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])

liste2D = [[1,2,3],[4,5,6],[7,8,9],[10,11,12]]

print("La liste  :")
print(liste2D)
print()

print("Le tableau :")
print(tab2D)
print()

nb_lignes = tab2D.shape[0]
nb_colonnes = tab2D.shape[1]
print ("nombre de lignes = ", nb_lignes)
print ("nombre de colonnes = ", nb_colonnes)
print()

print("Les éléments :")
for num_lig in range(nb_lignes):
    for num_col in range(nb_colonnes):
        print (tab2D[num_lig][num_col], end=" ")
print()

for num_lig in range(len(tab2D)):
    for num_col in range(len(tab2D[num_lig])):
        print (tab2D[num_lig][num_col], end=" ")
print()

for ligne in tab2D:
    for elem in ligne:
        print(elem, end=" ")
print()

def afficher_une_ligne(ligne):
    for elem in ligne:
        print(elem, end=" ")

for ligne in tab2D:
    afficher_une_ligne(ligne)
print()

def afficher_une_ligne_version_avec_indice(num_lig):
    for num_col in range(len(tab2D[num_lig])):
        print (tab2D[num_lig, num_col], end=" ")

for num_lig in range(len(tab2D)):
    afficher_une_ligne_version_avec_indice(num_lig)
print()

print("\nLes éléments par colonne :")
for num_col in range(nb_colonnes):
    for num_lig in range(nb_lignes):
        print (tab2D[num_lig, num_col], end=" ")
print()

somme = 0
for num_lig in range(nb_lignes):
    for num_col in range(nb_colonnes):
        somme = somme + tab2D[num_lig][num_col]
print()
print("somme des éléments", somme)
```