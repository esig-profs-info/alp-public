def a(x):
    print(x*x)

def b(x,y):
    a(x*y)
    a(y)

def c(x,y):
    b(y+2,x)
  
print("a(5):")
a(5)

print("b(2,3):")
b(2,3)

print("c(2,2):")
c(2,2)