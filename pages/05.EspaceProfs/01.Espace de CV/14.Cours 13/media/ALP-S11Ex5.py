# Série 11, Exercice 5
# Clément Vogt

# Constantes
DESCENDANT = 0

# Procédures et fonctions
def afficher_char(ch,nb_fois):
    for _ in range(nb_fois):
        print(ch,end='')
        
# Version 1
def afficher_en_escalier_v1(chaine,type_affichage):    
    if type_affichage == DESCENDANT:
        for i in range(0,len(chaine)):
            afficher_char(" ",i)
            print(chaine[i])   
    else:
        nb_char = len(chaine)
        for i in range(0,nb_char):
            afficher_char(" ",nb_char-i-1)
            print(chaine[i])

# Version 2
def afficher_en_escalier_v2(chaine,type_affichage):
    if type_affichage == DESCENDANT:
        for i in range(0,len(chaine)):
            print(i*" ",chaine[i],sep='')
    else:
        nb_char = len(chaine)
        for i in range(nb_char):
            print((nb_char-i-1)*" ",chaine[i],sep='')
    
# Procédure main()
def main():
    afficher_en_escalier_v1("abracadabra",0)
    afficher_en_escalier_v1("abracadabra",1)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()