# Cours 1 - Illustration
# Clément Vogt
print("\n---------------------------\nCours 1 - Illustration\n---------------------------")

# ---------------------------------------------------------------------------
# Déclaration de variables
# ---------------------------------------------------------------------------
ma_chaine = "Salut" # Déclaration d'une variable de type str (chaîne de caractère) et assignation de la valeur Salut à celle-ci

# ---------------------------------------------------------------------------
# Opérations de base
# ---------------------------------------------------------------------------
print("\nOpérations de base")

nb = 2**3 # Elévation de 2 à la puissance 3
print(nb)

# ---------------------------------------------------------------------------
# Procédure d'affichage : print()
# ---------------------------------------------------------------------------
print("\nProcédure d'affichage : print()")

print("Hello World") # Print avec un seul paramètre

print("Hello World, 2+2 font", 4, "...") # Print avec plusieurs paramètres de différents types

print("Hello World, 2+2 font ", 4, " ...", sep="") # Print avec modification du séparateur d'éléments par défaut

resultat = 4
print("Hello World, 2+2 font", resultat, "...",sep="/") # Comme ci-dessus mais le resultat n'est pas écrit "en dur" => Utilisation d'une variable resultat.

print("Hello World, 2+2 font", 4,end=" ") # Print avec modification de ce qui est imprimé à la fin. Dans cette illustration le but est de modifier le séparateur de lignes par défaut afin de ne pas faire de retour à la ligne.
print("...")

print("Hello World, 2+2 font ",4,sep="",end=" ") # Il est évidemment possible d'utiliser le paramètre sep et le paramètre end dans la même instruction.
print("...")

# ---------------------------------------------------------------------------
# Saisie de données par l’utilisateur : input()
# ---------------------------------------------------------------------------
print("\nSaisie de données par l’utilisateur : input()")

nom = input("Entrez un nom: ")

# Les données récupérées sont toujours considérées par Python comme étant des chaînes de caractères.
# Attention lorsque la donnée d'entrée n'est pas une chaîne de caractères
val_num_un = input("Entrez une valeur numérique: ") # Inscrivez 2
val_num_deux = input("Puis une deuxième numérique: ") # Inscrivez 3
res = val_num_un + val_num_deux
print(res) # Résultat 23 - Pourquoi ?: Car les entrées 2 et 3 sont considérées comme des caractères et non comme des chiffres. L'opérateur + sur des caractères a pour effet de concaténer (mettre bout à bout) ces derniers et non de les additionner. Si l'on souhaite les additionner, il faut au préalable les convertir en type numérique

# ---------------------------------------------------------------------------
# Conversion des types de données
# ---------------------------------------------------------------------------
print("\nConversion des types de données")

val_num_un = float(input("Entrez une valeur numérique: ")) # Inscrivez 2
val_num_deux = float(input("Puis une deuxième numérique: ")) # Inscrivez 3
res = val_num_un + val_num_deux
print(res) # Résultat 5.0 - Cette fois l'addition a été effectuée étant donné que les variables a et b ont été transformées en type numérique (cast). Evidemment, il faut saisir une donnée numérique en entrée sinon on provoque une erreur d'exécution...

# ---------------------------------------------------------------------------
# L'alternative (if ... : ...)
# ---------------------------------------------------------------------------
print("\nL'alternative (if ... : ...)")

print("DEBUT")
mon_chiffre = 2
if mon_chiffre == 2: # Ici, nous sommes dans le cas où mon_chiffre est égal à 2
    print("VRAI") # Pour spécifier que cette instruction se trouve dans la branche de l'alternative mon_chiffre == 2, on l'indente.
    print("La valeur de mon_chiffre est égale à 2")
print("FIN")

# ---------------------------------------------------------------------------
# L'alternative (if ... : ... else: ... )
# ---------------------------------------------------------------------------
print("\nL'alternative (if ... : ... else: ... )")

print("DEBUT")
mon_chiffre = 2
if mon_chiffre == 2: # Ici, nous sommes dans le cas où mon_chiffre est égal à 2
    print("VRAI") # Pour spécifier que cette instruction se trouve dans la branche de l'alternative mon_chiffre == 2, on l'indente.
    print("La valeur de mon_chiffre est égale à 2")
else: # Ici, nous sommes dans tous les autres cas. Dans cet exemple, nous somme dans le cas où mon_chiffre est différent de 2.
    print("FAUX")
    print("La valeur de mon_chiffre est différente de 2")
print("FIN")

# ---------------------------------------------------------------------------
# Indentation
# ---------------------------------------------------------------------------
print("\nIndentation")

print("DEBUT")
mon_chiffre = 2
if mon_chiffre == 2:
    print("VRAI")
    print("La valeur de mon_chiffre est égale à 2")
else:
    print("FAUX")
print("La valeur de mon_chiffre est différente de 2") # Ici, nous avons mal indenté notre instruction, celle-ci n'est donc pas considérée comme à l'intérieur du "else". Cela aura pour effet que cette instruction s'exécutera à chaque fois même quand mon_chiffre sera égal à 2. Pour corriger le problème, il aurait fallu l'indenter avec 4 espaces.
print("FIN")

# ---------------------------------------------------------------------------
# Opérateurs logiques : not, and, or
# ---------------------------------------------------------------------------
print("\nOpérateurs logiques : not, and, or")

mon_chiffre = 2
if not mon_chiffre == 2: # L'opérateur logique "not" inverse le résultat, renvoie False si le résultat est True
    print("La valeur de mon_chiffre est différente de 2")
else:
    print("La valeur de mon_chiffre est égale à 2")

mon_chiffre = 2
if mon_chiffre > 0 and mon_chiffre <= 3: # L'opérateur logique "and" renvoie True si toutes les opérandes sont True, False sinon.
    print("Mon chiffre est égal à 1, 2 ou 3")
else:
    print("Mon chiffre est différent de 1, 2 ou 3")
  
mon_chiffre = 2
if mon_chiffre <= 0 or mon_chiffre > 3: # L'opérateur logique "or" renvoie True si l'une des opérandes est True, False sinon.
    print("Mon chiffre est différent de 1, 2 ou 3")
else:
    print("Mon chiffre est égal à 1, 2 ou 3")

# ---------------------------------------------------------------------------
# Tortue
# ---------------------------------------------------------------------------
print("\nTortue")

from turtle import *

back(100)
right(90)
forward(100)
left(90)
forward(100)

exitonclick()