---
template: item
published: true
---

# Cours 8

===

* **Boucles for**: révision 
    * <u>Rappel:</u> on emploie une boucle lorsqu'on s'aperçoit qu'il y a un traitement à effectuer plusieurs fois. 
    * Lorsqu'**<u>on sait</u>** combien de fois on va devoir itérer et que le pas est constant: boucle **for**.
    * <u>Illustrations:</u> correction de l'exercice 2 ([ALP-S7Ex2](./media/ALP-S7Ex2.py)) et de l'exercice 4 ([ALP-S7Ex4](./media/ALP-S7Ex4.py)) de la Série 7.
* **Remarques**
    * Dans [ALP-S7Ex2](./media/ALP-S7Ex2.py): la réflexion sur les caractéristiques des nombres pairs permet d'obtenir du code plus performant.
    * Dans [ALP-S7Ex4](./media/ALP-S7Ex4.py): la réflexion sur le problème à résoudre permet d'obtenir du code plus performant.
* **Boucle while**
    * Lorsqu'**<u>on ne sait pas</u>** (avant de commencer la boucle) combien de fois on va devoir boucler: boucle **while**.
    * 
        ```python
        while condition :
            <instructionN>
        ```    
    * C'est le troisième formant algorithmique.
    * Comme les autres formants, il peut être considéré dans son ensemble comme une instruction et donc faire partie d'une séquence.
    * Caractéristiques: 
        * La séquence sous contrôle de la boucle peut n'être jamais exécutée (si l'expression booléenne qui contrôle la boucle est fausse lorsqu'on arrive dessus pour la première fois).
        * Il est fondamental que la séquence itérée modifie une des variables qui fait partie de l'expression booléenne qui contrôle la boucle, sinon on se trouve en présence d'une boucle infinie (si aucune variable de l'expression booléenne n'est modifiée, celle-ci ne peut pas changer de valeur de vérité et on ne ressortira donc plus de la boucle). 
        * On peut (quelquefois) interrompre une boucle infinie en appuyant sur les touches <span style="color:red">CTRL+C</span>.
    * Forme générale de la boucle:
    
        ![](./media/Boucle0.png)
    
* **Méthodologie de construction d'une boucle:** 
    1. Identifier la variable de pilotage de la boucle
    2. Définir la valeur initiale de la variable de pilotage (opération **Init** dans le schéma ci-dessous)
    3. Définir la condition de la boucle en fonction de la variable de pilotage (**Cond** dans le schéma ci-dessous)
    4. Définir l'actualisation de la variable de pilotage - la valeur qu'elle devra avoir au prochain tour de la boucle (opération **Actualisation** dans le schéma ci-dessous)
    5. Définir la séquence itérée - celle qu'on répète (opération **Séquence** dans le schéma ci-dessous)
* Le schéma algorithmique qu'on considère est donc le suivant: 

    ![](./media/Boucle1.png)

* <u>Illustrations:</u> [ALP-S8Ex1](./media/ALP-S8Ex1.py) à comparer avec [ALP-S8Ex2](./media/ALP-S8Ex2.py)
* Une aide pour comprendre l'algorithme de la suite de Fibonacci: [Vidéo](https://www.youtube.com/watch?v=SOY6izpc9C0)
* [Illustration des notions](./media/illustration-c08.py)

**<u>Exercices :</u>** [Série 8](/exercices/code/serie%208)