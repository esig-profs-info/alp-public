from gturtle import * 

makeTurtle()

def creerMarche():
    fd(20)
    rt(90)
    fd(20)
    lt(90)

def creerEscalier():
    repeat 7: 
        creerMarche()

creerEscalier()

