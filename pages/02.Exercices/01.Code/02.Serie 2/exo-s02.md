---
template: item
published: true
---

# Série 2
## Objectifs
Approfondir le concept d'alternative : Mettre en œuvre des **alternatives imbriquées** dans la résolution de problèmes.

**Décomposer** la solution d'un problème en **tâches**.

Mettre en pratique les concepts de **procédures**, de **fonction** et de **paramètre**.


===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

[Tortue: Liste des opérations](https://docs.python.org/fr/3/library/turtle.html#turtle-methods)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S2Ex1** à **ALP-S2Ex4** et se trouveront dans un dossier nommé **ALP-S2**.

## Énoncé

### Exercice T1
Développez une procédure i_grec() qui dessine un Y comme dans l’illustration A.
L’angle entre les deux branches est de 60° et chaque segment mesure 10.

Développez une deuxième procédure double_i_grec() qui dessine un double Y
comme dans l’illustration B. Le tronc mesure 20. Pour dessiner les deux branches,
utilisez la procédure i_grec() développée précédemment.

![](./media/igrec.png)

Continuez de travailler dans la même logique et développez une procédure
quadruple_i_grec() qui dessine la figure de l’illustration C. Le tronc mesure 40
et chaque branche est dessinée avec double_i_grec().

### Exercice 1
À partir de la donnée d'un **nombre entier**, afficher l'un des trois textes ci dessous en fonction de sa valeur :
> 	"Le nombre est négatif"
>
> 	"Le nombre est nul"
>
> 	"Le nombre est positif"

<u>Exemples d’exécution :</u>

![](./media/image1.png)
![](./media/image2.png)
![](./media/image3.png)

### Exercice T2
À partir de la donnée d'un **nombre de côtés** et d'un **périmètre**, dessiner l'une des formes géométrique suivante:
1. triangle
2. carré
3. pentagone

### Exercice 2
L'entreprise dans laquelle vous travaillez vous demande de réaliser un programme qui calcule le salaire brut d'un employé à partir de son **salaire de base** et de l'**année d'engagement** de l'employé. Les employés bénéficient d'une prime qui se rajoute au salaire de base en fonction de l'ancienneté (nombre d'années pendant lesquelles l'employé a travaillé dans l'entreprise). Cette prime est calculée de la manière suivante :

> * Les employés engagés depuis moins de 5 ans ne reçoivent aucune prime.
> * Les employés ayant travaillé entre 5 ans et 10 ans (y compris) dans l'entreprise reçoivent une prime correspondant à 3% de leur salaire de base.
> * Les employés engagés depuis plus de 10 ans reçoivent une prime correspondant à 7% de leur salaire de base.

Le programme que vous devez écrire calcule la prime et affiche le **salaire de base**, le **nombre d'années** pendant lesquelles l'employé a travaillé dans l'entreprise, le **montant de la prime** ainsi que le **salaire brut**.

<u>Exemples d’exécution *(avec 2020 comme année courante)*:</u> 

![](./media/image4.png)
![](./media/image5.png)
![](./media/image6.png)
![](./media/image7.png)
  
### Exercice 3
La compagnie d'aviation **Les Ailes** vous demande de réaliser un programme qui sera mis à la disposition des voyageurs dans les aéroports. Ce programme devra permettre aux clients d'interroger le système sur le prix du billet ; celui ci varie en fonction du type de voyageur ainsi que du nombre de vols déjà effectués avec la compagnie au cours de l'année de la manière suivante :

> Les employés de la compagnie (type de voyageur n°1) bénéficient d'un rabais de 20% sur le prix de base du billet, les membres du club Cigogne (type n°2) obtiennent un rabais de 10% sur le prix de base du billet, les autres voyageurs (type n°3) n'obtiennent pas de rabais.
> 
> De plus, si le voyageur a déjà volé avec la compagnie lors de l'année courante, il bénéficie d'un rabais supplémentaire (sur le prix de base) proportionnel au nombre de vols effectués; celui ci se monte à 10% pour un vol, à 15% pour 2, 3, ou 4 vols et à 20% pour 5 vols ou plus.

Le programme que vous êtes chargés de réaliser calculera et affichera le prix effectif du billet à partir de la donnée du prix de base, du type de voyageur et du nombre de vols effectués au cours de l'année.

<u>Exemples d’exécution :</u>

![](./media/image8.png)
![](./media/image9.png)
![](./media/image10.png)
![](./media/image11.png)
![](./media/image12.png)

### Exercice 4
Un cinéma désire automatiser la distribution de ses billets d'entrée. Le prix du billet varie en fonction de l'âge du spectateur ainsi qu'en fonction du numéro de tarif applicable de la manière suivante :

> Les enfants jusqu'à l'âge de 7 ans y compris payent 5 Frs ; les jeunes jusqu'à 18 ans y compris payent 10 Frs ; pour les autres, le prix est de 15 Frs. De plus, une réduction de 20% est faite sur ce prix le lundi (tarif n°1), une réduction de 10% est faite le mardi et le jeudi (tarif n°2), les autres jours (tarif n°3), il n'y a pas de réduction.

Le programme que vous êtes chargés de réaliser calculera et affichera le prix à payer par le client à partir de la donnée de l'âge du spectateur et du numéro de tarif.

<u>Exemples d’exécution :</u>

![](./media/image13.png)
![](./media/image14.png)
![](./media/image15.png)
![](./media/image16.png)
  


