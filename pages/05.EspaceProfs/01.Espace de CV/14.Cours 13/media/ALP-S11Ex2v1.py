# Série 11, Exercice 2
# Clément Vogt
# Version 1: Création d'une fonction existe() pour savoir si un caractère est présent dans une chaîne de caractères.

# Constantes
MINUSCULES = "abcdefghijklmnopqrstuvwxyz"
MAJUSCULES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
NUM = "0123456789"

# Procédures et fonctions
def existe(caractere,chaine):
    for element in chaine:
        if element == caractere:
            return True
    return False
    
def compte_et_affiche_types_de_car(s):
    cpt_min = 0
    cpt_maj = 0
    cpt_chiffre = 0
    cpt_autre = 0
    for element in s:
        if existe(element,MINUSCULES):
            cpt_min += 1
        elif existe(element,MAJUSCULES):
            cpt_maj += 1
        elif existe(element,NUM):
            cpt_chiffre += 1
        else:
            cpt_autre+= 1        
    afficher_resultats(cpt_min,cpt_maj,cpt_chiffre,cpt_autre)

def afficher_resultats(nb_min,nb_maj,nb_chiffre,nb_autre):
    print("La chaîne contient",nb_min,"minuscule",end="")
    if nb_min > 1:
        print("s",end="")
    print(",",nb_maj,"majuscule",end="")
    if nb_maj > 1:
        print("s",end="")
    print(",",nb_chiffre,"chiffre",end="")
    if nb_chiffre > 1:
        print("s",end="")
    print(" et",nb_autre,"autre",end="")
    if nb_autre > 1:
        print("s",end="")
    print(" symbole",end="")
    if nb_autre > 1:
        print("s",end="")    
    print(".")

# Procédure main()
def main():
    compte_et_affiche_types_de_car("P@#yn26at^&i5ve")
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()