---
template: item
---

Activité numéro 4 avec des gobelets, pour corser un peu l'affaire et découvrir des nouvelles instructions.

===

##Exercices - Gobelets
###Activité n°4 - Ecriture du code (avec de nouveaux challenges)
#### Contexte:
* Activité par groupe de 2

#### Concernant la correction de l'activité 3:
Pas de correction fournie, vous devez trouver un consensus dans la classe (avec l'aprobation de votre enseignant).

#### Votre travail:
Vous trouverez ci-dessous des photos représentant des configuration de gobelets. Ecrivez les algorithmes correspondant à chaque photo.<br/>

*NB: on admet toujours qu'une (ou deux) pile(s) de gobelets en nombre suffisant se trouve collée à gauche de chaque configuration proposée...*




#####Photo n°1:
Une pile de gobelets verts et une pile de gobelets rouges à gauche de la pyramide:<br/>
![photo gobelet avancé 1](pyramide_gobelets_avance_1.jpg?cropResize=350,350)
#####Photo n°2:
Une pile de gobelets mélangés au départ (on imagine qu'elle contient un nombre suffisant, mais limité de gobelets). Vous remarquerez qu'il est nécessaire de traiter différement des gobelets verts des gobelets rouges... A vous de proposer une **syntaxe** fonctionnelle.<br/>
![photo gobelet avancé 2](pyramide_gobelets_avance_2.jpg?cropResize=350,350)
#####Photo n°3:
Une pile de gobelets mélangés au départ (on imagine qu'elle contient un nombre suffisant, mais limité de gobelets):<br/>
![photo gobelet avancé 3](pyramide_gobelets_avance_3.jpg?cropResize=350,350)
#####Photo n°4:
*tous les gobelets sont de la même couleur...*<br/>
![photo gobelet avancé 4](pyramide_gobelets_avance_4.jpg?cropResize=600,600)
#####Photo n°5:
*une rangée sur 2 (rouge en bas, puis verts, puis rouge, etc)*<br/>
![photo gobelet avancé 5](pyramide_gobelets_avance_5.jpg?cropResize=600,600)