from gturtle import *

def aller_retour10():
    fd(10)
    bk(10)

def igrec():
    fd(10)
    lt(30)
    aller_retour10()
    rt(60)
    aller_retour10()
    lt(30)
    bk(10)
    
def double_igrec():
    fd(20)
    lt(30)
    igrec()
    rt(60)
    igrec()
    lt(30)
    bk(20)
    
def quadruple_igrec():
    fd(40)
    lt(30)
    double_igrec()
    rt(60)
    double_igrec()
    lt(30)
    bk(40)
    
    
makeTurtle()
quadruple_igrec()
hideTurtle()

    