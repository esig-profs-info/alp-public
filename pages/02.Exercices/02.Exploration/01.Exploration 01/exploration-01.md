---
template: item
---

Premiers contacts avec la console python

===

# Exploration 01

[Formulaire d'évaluation formative](https://forms.gle/BchwvP73nSStPPBr6)

Que sera l'output de python pour chacune des commandes ci-dessous? Expliquez et argumentez.

Gardez à l'esprit que l'important n'est pas de savoir exactement que sera affiché, mais de *comprendre pourquoi*.

```python
>>> 10
```

```python
>>> +10
```

```python
>>> 10+
```

```python
>>> -10
```

```python
>>> 10-3
```
