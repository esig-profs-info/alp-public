---
template: item
published: false
---

Dessinez les résultats des extraits de code ci-dessous. 
L'importation du module `gturtle` et l'appel à `makeTurtle()` n'est pas montré, mais bel et bien fait.


### Extrait 01
```python
repeat 3:
    forward(100)
    right(120)
```


### Extrait 02
```python
repeat 3:
    right(90)
    repeat 2:
        forward(10)
        left(45)
    forward(10)

```