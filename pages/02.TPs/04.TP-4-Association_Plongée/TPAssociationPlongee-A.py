from __future__ import print_function

def formaterReel(nombreReel):
    return '{:.2f}'.format(nombreReel)

def exercice1():
    # écrire ici le code de l'exercice 1
    'TODO'
    
def exercice2(jeuDonnees):
    
    if jeuDonnees == 1: 
        # Données de test 1        
        noms = ['Anne', 'Carole', 'Mike']
        poids = [72.5, 72.5, 72.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [1, 1, 1]
        nbPoids1kg = [0, 1, 2]
    elif jeuDonnees == 2:
        # Données de test 2
        noms = ['Magda', 'John', 'Céline', 'Oleko']
        poids = [72.5, 72.5, 73.5, 60]
        poidsCombi = [150, 100, 100, 200]
        volume = [76.4, 73.4, 75.1, 62]
        nbPoids500g = [2, 0, 2, 1]
        nbPoids1kg = [3, 1, 1, 2]
    elif jeuDonnees == 3: 
        # Données de test 3        
        noms = ['Anne', 'Carole', 'Mike']
        poids = [72.5, 72.5, 72.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [1, 1, 2]
        nbPoids1kg = [3, 1, 2]
    else:
        print('Jeu de données inexistant: ', jeuDonnees)
    
    print('#### Jeu de données no.', jeuDonnees, '####')
    
    # écrire ici le code de l'exercice 2

if __name__ == '__main__':
    
    
    exercice = 2 # Indique quel exercice vous souhaitez tester.
    if 1 == exercice:
        exercice1()
    elif 2 == exercice:
        exercice2(1)
    else:
        print('Exercice inexistant:', exercice)

 
