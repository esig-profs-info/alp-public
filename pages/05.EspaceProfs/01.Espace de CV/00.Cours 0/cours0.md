---
template: item
published: true
---

# Cours 0

===

* Accueil des étudiants
* [Présentation du cours](./media/Presentation_du_cours.pdf) (objectifs, outils, méthodes de travail, organisation du cours)
* Introduction du sujet à l’aide :
    * de l’activité « [Dilemme des 4 gallons](./media/Dilemme_des_4_gallons.pdf) » (merci Thomas!)
    * du film « [Netflix, 2019. en bref : Coder, pourquoi ?](https://www.netflix.com/ch-fr/title/80216752) »
* Petit historique des langages de programmation (plus de 2'500 langages différents) - [Présentation](./media/Algorithmique_et_programmation.pdf) - [Tiobe Index](https://www.tiobe.com/tiobe-index/)
* Un programme est un document, respectant une syntaxe, soumis à des conventions
* Découverte du langage [Python](https://www.python.org/)
    * Créé au début des années 90
    *  Multiplateformes
    *  Utilisé pour le scripting, la datascience et même le web
    *  Langage de bas niveau vs <u>langage de haut niveau</u>
    * Compilé vs <u>Interprété</u>
    * Typage dynamique fort
* Découverte de l'environnement [Thonny](https://thonny.org/)
    * Développé par l’Université de Tartu, Estonia
    * Le shell (interpréteur de commande)
    * L’éditeur de script
    * Exécuter (<span style="color:red">F5</span>) / Déboguer (<span style="color:red">CTRL+F5</span>) - [code](./media/ExempleDebug.py)
    * Fichiers python (.py)
* <u>Illustration:</u> « Hello World » dans le shell ainsi que dans l’éditeur de script - [code](./media/HelloWorld.py)
* [Exercices dans le shell](./media/Python_pour_debutants.pdf) (merci Luc-André!)
* Une pensée toute particulière pour Peter Daehne et Michel Kuhne
