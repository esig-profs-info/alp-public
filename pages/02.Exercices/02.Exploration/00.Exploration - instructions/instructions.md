---
template: item
---

Instructions pour les exercices d'exploration

===

# À la découverte de python

Cette section contient des séries avec des exemples de code python que vous allez analyser afin de comprendre différents concepts de programmation importants.

## Déroulement possible

Cette exploration peut se passer de différentes manières. Voici les instructions pour une séance d'Apprentissage par l'Autonomie.

### Évaluation formative initiale

Vous allez commencer par répondre individuellement à un questionnaire dont le lien se trouve en haut de page. Ce questionnaire a deux buts:

1. Vous mettre en état pour l'exploration en mobilisant vos connaissances préalables.
2. Permettre à votre enseignant d'ajuster son cours en fonction de vos progrès.

Ce n'est donc pas du tout grave si vous n'arrivez pas à "bien" répondre aux questions. Cela n'aura aucune influence sur vos notes.

### Exploration en équipe

Par équipe de 3, vous allez chercher à comprendre les morceaux de code donnés.

Vous pouvez utiliser toutes les ressources à votre disposition: une console python, le web, des livres, ...

Il est crucial que vous notiez les réponses que votre groupe a trouvé.

L'objectif n'est pas de savoir répondre aux questions, mais bien de comprendre les concepts! Ceci vous permettra des les utiliser pour créer des programmes.

### Mise en commun

Les différents équipes mettront en commun leurs trouvailles afin de créer une réponse commune de la classe.

Votre enseignant animera les discussions, mais il ne vous corrigera pas: à vous et vos collègues de bien vérifier vos réponses lors de la phase exploration!

### Évaluation formative "après-coup" et utilisation

Votre progression sera évalué individuellement. Cette évaluation peut prendre différentes formes mais ne sera jamais notée.

Vous allez également pouvoir appliquer les concepts appris dans d'autres contexte, typiquement pour créer des beaux programmes!
