# Série 13, Exercice 1
# Clément Vogt

# Procédure main()
def main():
    # Créer le dictionnaire
    d = {'nom': 'Dupuis', 'prenom': 'Jacque', 'age': 30}
    
    # Corriger l'erreur dans le prénom
    d["prenom"] = "Jacques"
    
    # Afficher la liste des clés du dictionnaire
    for k in d:
        print(k)
    print()
    
    for k in d.keys():
        print(k)
    print()
    
    # Afficher la liste des valeurs du dictionnaire
    for k in d:
        print(d[k]) # équivalent à print(d.get(k))
    print()
    
    for v in d.values():
        print(v)
    print()
    
    # Afficher la liste des paires clé/valeur du dictionnaire.
    for k in d:
        print(k,":",d[k])
    print()
    
    for k,v in d.items():
        print(k,":",v)
    print()
    
    # Ecrire la phrase "Jacques Dupuis a 30 ans" en récupérant les valeurs venues du dictionnaire
    print(d["prenom"],d["nom"],"a",d["age"],"ans")
        
# Appel de la procédure main()
if __name__ == "__main__":
    main()