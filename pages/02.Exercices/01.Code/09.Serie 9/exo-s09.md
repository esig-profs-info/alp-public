---
template: item
published: true
---

# Série 9
## Objectifs
Comprendre le concept de **liste**.

Mettre en œuvre le concept de **boucle** pour résoudre des problèmes simples et traiter des **listes**.

Mettre en œuvre le concept de **boucle** dans le cas où on ne connaît pas le nombre de fois que la boucle peut être exécutée.

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S9Ex1** à **ALP-S9Ex7** et se trouveront dans un dossier nommé **ALP-S9**.

## Énoncé

## Exercice 1
Dans cet exercice, vous allez écrire des fonctions permettant d'extraire des données d'une liste

1. Ecrire une fonction `premier_de_la_liste(liste)` qui retourne la première valeur de la liste passée en paramètre
2. Ecrire une fonction `dernier_de_la_liste(liste)` qui retourne la dernière valeur de la liste passée en paramètre.
3. Ecrire une fonction `avant_dernier_de_la_liste(liste)` qui retourne l'avant-dernière valeur de la liste passée en paramètre.
4. Ecrire une fonction `nieme_valeur_de_la_liste(liste, n)` qui retourne la nième valeur de la liste (`n` et `liste` passés en paramètres).

Testez vos fonctions avec les listes suivantes à insérer en constantes dans vos programmes:</br>
```Python
L10_INT = [-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] #liste de 10 nombres entiers

L2_STR = ["alice", "bob"] #liste de deux chaînes de caractères 

L1_FLO = [-3.5] #liste de un seul nombre réel 

L0 = [] #liste vide
```

**Questions:** 

a. *Que se passe-t-il si la liste passée en paramètre est vide ?* </br>
b. *Que se passe-t-il si pour une liste de 2 éléments, on demande le 3ème ?*

*Dans une deuxième temps*, à partir de vos réflexions sur ces deux questions, ajouter une validation des données au moyen d'une fonction à résultat booléen.

## Exercice T1

Reprenez l'exercice T1 de la série 8 (spirale colorée). À la place des couleurs codées en dur ou sous forme de constantes individuelles,
utilisez une constante contenant la liste des couleurs:
```COULEURS = ['yellow', 'green', 'blue', 'red']```
Essayez au maximum de simplifier votre code grâce à cette liste.

## Exercice 2
Ecrire une procédure `afficher_liste(ma_liste)` qui affiche chaque élément d'une liste sur une nouvelle ligne. Réutilisez les listes de l'Exercice 1.

## Exercice 3
Ecrire une procédure `afficher_positifs(nombres)` qui affiche les éléments strictement positifs d'une liste de nombres. Réutilisez la liste des 10 nombres entiers de l'Exercice 1.

## Exercice 4
Afficher le nombre d'éléments d'une liste d'entiers `ma_liste` dont la valeur est compris entre 2 valeurs saisies, `val_min` et `val_max` (inégalités strictes).

Notez bien: la position des éléments dans la liste n'a pas d'importance, c'est uniquement leur valeur qui est prise en compte!

<!--Dans cet exercice, les valeurs fournies (`valMin` et `valMax`) sont toujours données dans l'ordre, à savoir: la plus petite d'abord, la plus grande ensuite. Il n'est donc pas nécessaire de tester que les deux valeurs données soient dans le bon ordre puisqu'elles le sont de fait.-->

#### Données de test:
Vous reconnaîtrez la liste de 10 entiers de l'Exercice 1.

ma_liste | val_min | val_max | nombre d'éléments
   ---|---|---|:---:
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | 3 | 5 | 1
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | -1 | 1 | 1
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | -10 | 9 | 10
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8]| 2 | 7 | 2
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | 5 | 5 | 0

<!--
## Exercice 5 -  JE SUGGÈRE CET EXO EN DEMO DE "FOR EACH"
Python intègre une fonction nommée `len(list)` qui retourne la longueur de la liste passée en paramètre (le nombre d'éléments qu'elle contient).
Ecrire votre propre fonction `longueur(list)` qui compte le nombre d'éléments d'une liste sans utiliser la fonction `len(list)`. Comparez ensuite votre résultat avec celui de la fonction `len(list)`. Réutilisez les listes de l'Exercice 1 pour vos tests.-->

<!--
## Exercice 5 - JE SUGGÈRE CET EXO EN SERIE 10 - c'est la recherche
Python intègre la possibilité de connaître la position (l'index) d'un élément dans une liste. La syntaxe pour l'utiliser est la suivante: `liste.index(element)`
<br/>**Par exemple:**
```python
liste = ["un","quatre","six","8"]
indexDuSix = liste.index("six")
print(indexDuSix) #affiche 2
```
Ecrire une fonction nommée `monIndex(liste, e)` qui retourne l'index de l'élément `e` s'il est contenu dans la liste, sinon `-1`. Ceci, sans utiliser la fonction `index(element)`.

Comparez ensuite votre résultat avec celui de la fonction `index(element)`. Notamment, observez ce qu'il se passe si l'élément est contenu plusieurs fois dans la liste. Réutilisez les listes de l'Exercice 1 pour vos tests.-->


### Exercice 5
À partir de la donnée des relevés de température pour les 7 jours de la semaine, calculer au moyen de 2 fonctions différentes et afficher :
* La moyenne des températures.
* Le nombre de températures inférieures à 15 degrés.
```Python
RELEVES = [11.8, 14.4, 18.6, 16.5, 11.5, 12.3, 9.1] 
# temp. moyennes relevées à Genève du 30.09 au 6.10.2021
# https://prevision-meteo.ch/climat/journalier/geneve-cointrin/2021-10
```


### Exercice 6
À partir de la donnée des relevés des notes de 16 élèves d'une classe, calculer au moyen de 4 fonctions différentes et afficher :
* La moyenne des notes arrondie à une décimale après la virgule.
* Le nombre de notes inférieures à la moyenne (4.0).
* La meilleure note. 
* Le numéro d'ordre de la meilleure note (l'indice de la position de la note dans la liste).

```Python
NOTES = [3, 6, 5.5, 4.5, 2.5, 4, 5, 4, 3, 4, 2.5, 4.5, 5, 5, 4, 3]
# notes d'un vrai groupe
```
<!--
### Exercice 7  - JE TROUVE DOUBLE EMPLOI AVEC LES 2 PRECEDENTS
Compléter le script [Fourni_ALP-S9Ex3](./media/Fourni_ALP-S9Ex3.py) de sorte que les différentes procédures-fonctions respectent les spécifications données dans leurs commentaires. Vous remplacerez les commentaires <span style="color:green">''' A COMPLETER '''</span> par les éléments nécessaires.

<u>Exemple d’exécution :</u>

![](./media/image1.png) 

<u>Contrainte impérative :</u> la procédure main() ainsi que les noms des procédures/fonctions à compléter ne doivent pas être modifiés.-->

## Exercice 7
L’ESIG organise une soirée pour la fête de l'Escalade. Elle vous demande de lui fournir un petit outil permettant de vérifier que le nombre de personnes admises respecte la limite imposée par les normes de sécurité.

Pour cela, écrire une procédure `controle_escalade`` qui prendra deux paramètres :
 - une liste de nombres correspondant aux tailles des différents groupes qui veulent entrer (donc des nombres entiers strictement positifs).
 - une *jauge* c'est-à-dire un nombre (entier évidemment) qui indique le nombre maximal de personnes (donc un nombre entier strictement positif).

Cf. [sorties attendues.](./media/exo7_sorties.pdf)
Le principe est que le parcours de la liste s'interrompt une fois qu'on atteint la jauge. Les données sont ainsi faites qu'une liste contient toujours assez de personnes pour atteindre ou dépasser la jauge.

*Dans une deuxième temps*, à partir de votre solution et de vos réflexions, demandez-vous si votre solution interdit le dépassement de la jauge ou en autorise le dépassement en laissant entrer le dernier groupe traité. Les deux types de solution sont acceptés et vous pourrez alors écrire une variante de votre procédure qui adopte l'autre comportement.