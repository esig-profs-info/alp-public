# Série 14, Exercice 3
# Clément Vogt

# Imports
import numpy as np

# Procédures et fonctions
def moyenne_avec_poids_une_ligne(num_lig,tab2D,poids_par_colonne):
    somme = 0
    cpt = 0
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        somme += poids_par_colonne[num_col] * tab2D[num_lig,num_col]
        cpt += poids_par_colonne[num_col]
    return somme / cpt

def moyenne_avec_poids_par_ligne(tab2D,poids_par_colonne):
    liste = [] 
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        moyenne = moyenne_avec_poids_une_ligne(num_lig,tab2D,poids_par_colonne)
        liste.append(moyenne)
    return liste    
        
# Procédure main()
def main():
    tab2D_notes = np.array([[5,4,3],[6,4.5,5.5],[6,6,6],[2.5,3,2],[4,4.5,4.5]])
    poids_note = [1,2,2] 
    moyenne_avec_poids_par_etudiant = moyenne_avec_poids_par_ligne(tab2D_notes,poids_note) 
    print(moyenne_avec_poids_par_etudiant)

# Appel de la procédure main()
if __name__ == "__main__":
    main()