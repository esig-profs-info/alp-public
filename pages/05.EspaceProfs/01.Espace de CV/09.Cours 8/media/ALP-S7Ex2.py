# Série 7, Exercice 2
# Clément Vogt

# ---------------------------------------------------------------------------
# Méthode1: on passe en revue tous les entiers. Pour chaque entier on teste sa parité, s'il est pair on l'affiche.
# ---------------------------------------------------------------------------
def main():
    n = int(input("Entrez un nombre entier"))
    
    print("1) Entiers pairs compris entre 0 et ",n,sep='')
    for nb in range(n,0-1,-1):
        if nb % 2 == 0: # On teste si nb est pair (càd si le reste de la division par 2 vaut 0. 
            print(nb)
            
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Méthode 2: on exploite une propriété des nombres pairs qui est d'être à distance 2 (à partir de 0).
#   Cette méthode est meilleure car on fait moins de travail doublement:
#     1) On fait 2 fois moins de tours dans la boucle, donc on exécute deux fois moins d'instructions.
#     2) De plus dans la Méthode 1, à chaque fois, on exécute une instruction de division (%) qui coûte cher en temps de calcul.
#   Attention: il faut partir d'un nombre pair; donc considérer le cas où le nombre saisi n est impair.
# ---------------------------------------------------------------------------
def main():
    n = int(input("Entrez un nombre entier"))

    print("2) Entiers pairs compris entre 0 et ",n,sep='')
    if n % 2 == 0:
        debut = n
    else:
        debut = n-1
    for nb in range(debut,0-1,-2):
        print(nb)

if __name__ == "__main__":
    main()