# Série 12, Exercice 1
# Clément Vogt

# Procédures et fonctions
def premiers3(s):
    return s[:3]

def derniers3(s):
    return s[-3:]

def afficher_resultats(premiers,derniers):
    print("Les 3 premiers caractères =",premiers)
    print("Les 3 derniers caractères =",derniers)

# Procédure main()
def main():
    chaine = input("Entrez une chaine de caractères")
    premiers = premiers3(chaine)
    derniers = derniers3(chaine)
    afficher_resultats(premiers,derniers)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()