import numpy as np
import matplotlib as mpl
mpl.use('TkAgg') # hack apparemment nécessaire sur Mac
import matplotlib.pyplot as plt

def plotanomalies(title, years, anomalies):
    plt.title(title)
    plt.xlabel("year")
    plt.ylabel("degrees C +/- from average")
    plt.bar(years, anomalies, color='blue')
    plt.show()

np.set_printoptions(suppress=True)  # Désactive la notation scientifique

data = np.loadtxt("temp-anomalies.csv",delimiter=',') # chargement des données

# Exemple d'utilisation de la fonction plotanomalies:
plotanomalies(
    'Janvier',
    data[:,0], # première colonne (années) récupérée par slicing
    data[:,1] # deuxième colonne (anomalies pour janvier)#
)


