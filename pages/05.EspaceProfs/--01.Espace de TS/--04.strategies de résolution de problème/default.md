---
template: item
published : true
title: Stratégie de résolution de problème
---

### Une stratégie?

Il est très important de décider d'une stratégie et d'une architecture avant de commencer à coder le programme qui va résoudre le problème énoncé.
Nous proposons 3 temps:
1. définition de la liste des "todo", c'est à dire les "choses à faire" pour résoudre le problème.
1. listing des fonctions que nous allons créer sur la base des "todo"
1. création d'une architecture (quelles variables, quelles fonctions, quels paramètres, à quels endroits, etc?)

### Ressources
Voici un fichier excel vous permettant de décider de votre architecture:
[fichier_excel](grille_architecture_programme.xlsx)
