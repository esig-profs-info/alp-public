---
template: item
published: true
---

# Épreuve de septembre

===

* **Rappels:**
    * L’épreuve a lieu le mardi 26.09.2023 à 08h15
    * La durée du test est de ~80 minutes
    * Tous les documents personnels (y compris électroniques) sont autorisés pour la partie plug
    * Aucun document personnel n'est autorisé pour la partie unplug
    * Tout accès au réseau est formellement interdit
    * Le contrôle est obligatoirement réalisé sur le matériel mis à disposition par l'école (les ordinateurs personnels sont interdits)
* **Révision**
    * Alternatives: Tests, test imbriqués et formants de luxe (elif)
    * Procédures et fonctions
    * Exercices [Lire du code](/exercices/unplugged/lire%20du%20code) jusqu'à "Fonctions" inclus.
    * ... c'est à dire, cours 1 à 4 avec les séries d'exercices correspondantes.