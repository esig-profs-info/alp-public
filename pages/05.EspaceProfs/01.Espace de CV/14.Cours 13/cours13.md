---
template: item
published: true
---

# Cours 13

===

* **Correction des séries 11 et 12**
    * Série 11, exercice 1 (solutions: [v1](./media/ALP-S11Ex1v1.py) - [v2](./media/ALP-S11Ex1v2.py))
    * Série 11, exercice 2 (solutions: [v1](./media/ALP-S11Ex2v1.py) - [v2](./media/ALP-S11Ex2v2.py) - [v3](./media/ALP-S11Ex2v3.py))
    * Série 11, exercice 3 ([solution](./media/ALP-S11Ex3.py))
    * Série 11, exercice 4 ([solution](./media/ALP-S11Ex4.py))
    * Série 11, exercice 5 ([solution](./media/ALP-S11Ex5.py))
    * Série 12, exercice 1 (solutions: [vStandard](./media/ALP-S12Ex1.py) - [vPlus](./media/ALP-S12Ex1Plus.py))
    * Série 12, exercice 2 ([solution](./media/ALP-S12Ex2.py))
    * Série 12, exercice 3 ([solution](./media/ALP-S12Ex3.py))
* **Les dictionnaires**
    * Un dictionnaire est une collection qui est ordonnée (>= v3.7), modifiable et qui n'autorise pas les doublons
    * [w3schools](https://www.w3schools.com/python/python_dictionaries.asp)
    
**<u>Exercices :</u>** [Série 13](/exercices/code/serie%2013)