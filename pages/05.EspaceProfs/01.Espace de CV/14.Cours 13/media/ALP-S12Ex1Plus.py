# Série 12, Exercice 1 Plus
# Clément Vogt

# Procédures et fonctions
def premiersn(s,nb_caracteres):
    return s[:nb_caracteres]

def derniersn(s,nb_caracteres):
    return s[-nb_caracteres:]

def afficher_resultats(premiers,derniers,nb_caracteres):
    print("Les",nb_caracteres,"premiers caractères =",premiers)
    print("Les",nb_caracteres,"derniers caractères =",derniers)

# Procédure main()
def main():
    chaine = input("Entrez une chaine de caractères")
    nb_caracteres = int(input("Entrez un nombre de caractères"))
    premiers = premiersn(chaine,nb_caracteres)
    derniers = derniersn(chaine,nb_caracteres)
    afficher_resultats(premiers,derniers,nb_caracteres)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()