---
template: item
published: true
---

TP3 - Course de l'Escalade

===

![logo escalade](escalade.png?cropResize=300,300)
## Contexte du TP
La course de l'escalade vous mandate pour créer un programme capable d'analyser des résultats de la courses fournits sous forme de texte.<br/>
Chaque coureur doit parcourir 3 tours d'une boucle de 2.441 km (donc 7.323 km au total.)
Les données arrivent sous la forme d'un tableau à 2 dimensions contenant les chaînes de caractères suivantes:

```
"6m42" "6m55" "7m02"
"6m51" "7m13" "7m01"
"7m05" "7m23" "7m32"
"6m49" "7m02" "6m57"
```

Les colonnes contiennent les temps chronométrés pour chaque tour, dans l'ordre<br/>
Les lignes contiennent les temps de chaque coureur (pour chaque tour), dans l'ordre des dossards suivants:
```
101
302
5004
502
```

Voici encore les prénoms des coureurs (selon leur dossard):
```
101 => "Julien"
302 => "Bob"
502 => "Moussa"
5004 => "Edward"
```

## Votre mandat:

1. Trouver et afficher le vainqueur de la course comme suit:
```
Le vainqueur de la course est Julien (Dossard 101) avec un temps total de 20 minutes et 39 secondes
```
1. Trouver et afficher le tour le plus rapide:
```
Le tour le plus rapide a été effectué par Julien (Dossard 101) en 6 minutes et 42 secondes (tour no 1)
```
1. Calculer les moyennes des temps effectués pour chaque tour, les stocker dans un tableau 1d, puis, en parcourant le tableau 1d, afficher les valeurs:
```
voici les moyennes de temps pour chaque tour:
tour no 1 : 6 minutes 52 secondes
tour no 2 : 7 minutes 8 secondes
tour no 3 : 7 minutes 8 secondes
```

1. Calculer les moyennes des temps effectués pour chaque coureur, les stocker dans un tableau 1d, puis, en parcourant le tableau 1d, afficher les valeurs:
```
voici les moyennes de temps pour chaque coureur:
Julien (Dossard 101) : 6 minutes 53 secondes
Bob (Dossard 302) : 7 minutes 2 secondes
Edward (Dossard 5004) : 7 minutes et 20 secondes
Moussa (Dossard 502) : 6 minutes et 56 secondes
```

## Méthode de travail
1. Lister les 'todo', c'est à dire les choses à faire pour arriver aux résultats
1. A partir de ces 'todo' définir une architecture pour le programme: 
 * Quelles variables dans le main?
 * Quelles fonctions / procédures (avec ques paramètres, quels valeurs de retours, quels affichages)

 ## Pour vous aider
Voici une proposition de todos:
![todos](todos.jpg?cropResize=1200,1200)

Voici une proposition de début d'architecture, à vous de compléter:
![architecture](archi.jpg?cropResize=1200,1200)

Voici les valeurs calculées dans Excel:
![datas](datas.jpg?cropResize=1200,1200)

Voici comment déclarer un taleau à 2 dimensions de 6 lignes et 4 colonnes rempli de la valeur zéro:
```
import numpy as np
tab2d = np.zeros(shape=(6,4))
```

Voici comment contourner (temporairement) le problème de la conversion du tableau de String en tableau d'entier:
```
import numpy as np
def convertir(tab2dString):    
    #à modifier plus tard...
    return np.array([[402,415,422],[411,433,421],[425,443,452],[409,422,417]])
```
