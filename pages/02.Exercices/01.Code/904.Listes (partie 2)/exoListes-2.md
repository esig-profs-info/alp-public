---
template: item
published: true
---

Deuxième série d'exercices traitant des algorithmes de base sur les listes.

===

## Exercice 1
Ecrire une fonction qui retourne le plus petit élément d'une liste.

Dans la plupart des langages, il existe déjà des fonctions qui font cela (`min` en python). Le but ici est de réécrire cet algorithme par vous-même afin de comprendre les idées clés qui s'appliquent également dans d'autres situations.

Testez votre fonction (au moins) avec les exemples suivants:

liste |résultat
---:|:---:
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] |1
[10, 9, 8, 7, 6, 4, 3, 2, 1] |1
[-22, 2, 34, -4, 51, 12, 45, 8, -17] |-22
[-22, 2, 34, -444444444444444444444444444444444444444444444, 51, 12, 45, 8, -17] |-444444444444444444444444444444444444444444444
[10, 8, 6, 4.5, 2, 1, 0.9, 5, 7, 9] |0.9
[1, 2, 1, 24, 5, 24, 7, 12, 9, 10] |1
['Dorothée', 'Margo', 'Alfred', 'Aladin', 'Ben'] |"Aladin"
['a', 'b', 'A', 'B'] |"A"
[1] |1
"bizarre" |"a"
"bizarre?!" |"!"
[] | Que devrait-il se passer d'après vous?


## Exercice 2
Ecrire une fonction qui retourne le plus grand élément d'une liste.

Testez avec les listes ci-dessus et comparez vos résultats avec la fonction `max` fourni par python.

## Exercice 3
Ecrire une fonction qui détermine le plus grand et le plus petit élément d'une liste (toujours sans utiliser les fonctions min et max de python).

Est-ce possible de faire cela avec une seule boucle? Est-ce souhaitable? Pourquoi?

## Exercice 4
Ecrire une fonction qui retourne le plus grand nombre *pair* d'une liste d'entiers.

Si la liste ne contient aucun entier pair, il faut afficher un message et retourner `None`

liste |résultat
---:|:---:
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] |10
[11, 10, 9, 8, 7, 6, 4, 3, 2, 1] |10
[-22, 2, 34, -4, 51, 12, 45, 8, -17] |34
[] | "Aucun entier pair dans la liste" / None
[1, 2, 1, 24, 5, 24, 7, 12, 9, 10] |24
[1, 2.6] | "Aucun entier pair dans la liste" / None
[2] |2
[0, 1] |0

## Exercice 5
Calculer la moyenne arithmétique d'une liste de 10 nombres entiers.

Organisez votre code proprement, bien entendu :-)


liste |résultat
---:|:---:
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] |5.50
[10, 9, 8, 7, 6, 4, 3, 2, 1] |5.56
[-22, 2, 34, -4, 51, 12, 45, 8, -17] |12.11
[-22, 2, 34, -444444444, 51, 12, 45, 8, -17] |-49382703.44
[10, 8, 6, 4.5, 2, 1, 0.9, 5, 7, 9] |5.34
[1, 2, 1, 24, 5, 24, 7, 12, 9, 10] |9.50
[1] |1.00
[] |0.00



## Exercice 6
Calculer la moyenne pondérée d'une liste de nombres entiers. 

Les nombres inférieurs ou égaux à 0 ne comptent pas.
Les nombres supérieurs ou égaux à 1 et inférieurs ou égaux à 25 comptent 1 fois.
Les nombres supérieurs ou égaux à 26 et inférieurs ou égaux à 50 comptent 2 fois.
Tous les autres nombres comptent 3 fois.


	
liste |résultat
---:|:---:
[-1, 1] |1.000
[-1, 25, 26] |25.667
[50, 51] |50.600
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] |5.500
[10, 9, 8, 7, 6, 5, 4, 3, 2, 1] |5.500
[-22, 2, 34, -4, 51, 12, 45, 8, -17, 14] |31.545
[10, 8, 6, 4, 2, 1, 3, 5, 7, 9] |5.500
[1, 2, 1, 24, 5, 24, 7, 12, 9, 10] |9.500



## Exercice 7
Compter combien il y a de nombres inférieurs ou égaux à 15 dans une liste quelconque de nombres entiers.


liste |résultat
---:|:---:
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] |10
[10, 9, 8, 7, 6, 5, 4, 3, 2, 1] |10
[-22, 2, 34, -4, 51, 12, 45, 8, -17, 14] |7
[100, 80, 60, 40, 20, 100, 30, 50, 70, 90] |0
[1, 2, 1, 24, 5, 24, 7, 12, 9, 10] |8


## Exercice 8*

Déterminer et afficher les deux plus petits nombres d'une liste de 10 nombres entiers. Les deux nombres seront affichés dans l'ordre croissant.

## Exercice 9*

Déterminer et afficher les deux plus grands nombres d'une liste de 10 nombres entiers. Les deux nombres seront affichés dans l'ordre décroissant.


<!--
idees:

Exercices:
coder le count() sans utiliser count()
coder le index(el, start, end) pour retourner l'index d'un élément dans une partie de la liste 
Pour chaque éléments d'une liste: afficher son type en utilisant la fonction type
    compter le nombre d'éléments de type entier(p.ex) dans une liste (en utlisant  type(element)==int )
livret sous forme de liste => implique append()

-->
