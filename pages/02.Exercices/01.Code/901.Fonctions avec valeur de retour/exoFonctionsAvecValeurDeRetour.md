---
template: item
published: true
---

Exercices traitant les fonctions avec return

===

<!--todo: lister les pré-requis-->
### Attention: 
pour coder dans TigerJython, ajouter la ligne suivante en haut de votre code:
```
from __future__ import print_function
```

## Exercice 1
Ecrire une fonction maxParmi2(a,b) qui prend 2 nombres (entiers ou réels) a et b en paramètre et retourne le plus grand des deux. Afficher ensuite le résultat (en dehors de la fonction) ou tester le résultat dans la console avec les données ci-dessous:

#### Données de test (à taper dans la console)
   a | b | valeur retournée
   ---|---|:---:
   1|2|2
   5|0|5
   2|2|2
   -1|-2|-1
   0.5|0.6|0.6

## Exercice 2
Ecrire une fonction maxParmi3(a,b,c) qui prend 3 nombres entiers, a, b et c en paramètre et retourne le plus grand des trois. Afficher ensuite le résultat (en dehors de la fonction) ou tester le résultat dans la console avec les données ci-dessous:<br/>
**Attention:** il est demandé d'appeler la fonction maxParmi2() depuis maxParmi3(). 

#### Données de test (à taper dans la console)
   a | b | c | valeur retournée
   ---|---|---|:---:
   1 |2 |3 |3
1 |3 |2 |3
3 |1 |2 |3
1 |1 |1 |1
-1 |0 |4 |4
1 |3 |3 |3
-2 |-4 |-10 |-2
0.5 |-0.7 |-3 |0.5

<!--todo: générer les sorties produites-->

## Exercice 3
Ecrire une fonction `estDivisiblePar(a,b)` qui prend deux entiers en paramètre et qui retourne `True` si, et seulement si, `a` est divisible par `b`.

#### Données de test (à taper dans la console)
a |b |résultat
---:|---:|:---:
13 |3 |False
12 |3 |True
-4 |2 |True
-4 |3 |False
0 |100 |True
0 |12 |True
3 |4 |False
100 |1 |True


## Exercice 4
Ecrire une fonction `estPair(n)` qui prend un entier en paramètre et retourne `True`si, et seulement si, `n` est un nombre pair.

#### Données de test (à taper dans la console)
n |résultat
---:|:---:
0 |True
3 |False
-4 |True
10 |True
-1 |False
