# Série 13, Exercice 3
# Clément Vogt

# Constantes
NOMS = ['Londa','Lenita','Beatrice','Kendrick','Genny','Rolf','Meridith','Hilton','Phylis','Candis','Ron','Lecia','Jacquelyn','Gonzalo','Herlinda','Morgan','Han','Sanjuanita','Allie','Fernande','Anna','Gracia','Lula','Merlyn','Tandy','Adah','Ozella','Laureen','Ricky','Miki']
NB_PLONGEES = [20,120,15,150,5,20,30,60,100,8,20,15,30,79,130,66,24,110,8,23,20,77,60,22,30,80,50,20,10,29]

# Procédures et fonctions
def cree_dictionnaire(liste1,liste2):
    d = {}
    for i in range(len(liste1)):
        d[liste1[i]] = liste2[i] # équivalent à d.update({liste1[i]: liste2[i]})
    return d

def plongeuse_plus_experimentee(plongeuses):
    plus_exp = None
    for k in plongeuses:
        if plus_exp is None or plongeuses[k] > plongeuses[plus_exp]:
            plus_exp = k
    return {'nom': plus_exp, 'nb_plongees': plongeuses[plus_exp]}

# Procédure main()
def main():
    plongeuses = cree_dictionnaire(NOMS,NB_PLONGEES)
   
    nom = input("Saisissez le nom d'une plongeuse")
   
    if nom in plongeuses:
        print(nom,":",plongeuses[nom],"plongées")
    else:
        print("Ce nom est inconnu.")
    
    plus_exp = plongeuse_plus_experimentee(plongeuses)
    print(plus_exp)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()