---
template: item
published: true
---

TP1 - Choux, Chèvres et Loups

===

![chouxchevreloup](image.jpg?cropResize=300,300)
## Contexte du TP
L'office fédéral de la faune et de la flore alpine (offfa) vous mandate pour créer un programme capable de simuler l'évolution du nombre de chèvres, de choux et de loups en Suisse. Les employés ont pu récolter les données suivantes:<br/>

1. les chèvres mangent des choux
1. les loups mangent des chèvres
1. les choux ne mangent personne

#### il peuvent aussi affirmer que, ***chaque mois***:

1. 700 choux poussent pour chaque cm d'eau de pluie tombée.
1. Chaque chèvre mange 15 choux entiers.
1. Si, après que les choux aient poussé, il n'y en a pas assez pour les chèvres (15 par chèvre), les chèvres diminuent leur population de 20% (prendre la valeur entière du calcul en utilisant la fonction `int()`, en effet, une demi-chèvre ou un dixième de chèvre n'est pas viable). Par ailleurs, dans ce cas, tous les choux restants sont mangés.
1. Si, durant le mois toutes les chèvres ont assez de choux à manger, alors elles augmentent leur population de 20% (même remarque qu'au-dessus concernant la valeur entière).
1. Les loups, eux, mangent chacun une chèvre (par mois). Leur tactique est d'attendre de voir si les chèvres ont augmenté ou baissé leur population avant de mener leur attaque.
1. S'il y a assez de chèvres pour tous les loups, leur population augmente de 30% (en valeur entière), sinon, elle décline de 30% (en valeur entière)...et évidemment toutes les chèvres restantes sont mangée s'il n'y en a pas assez.

## Votre travail
### Etape 1
Produire l'affichage du nombre de choux/chèvres/loups chaque mois sur une période d'une année sachant que les populations initiales sont les suivantes:<br/><br/>

| population de: | nb d'individus|
| --- | ---: |
|  choux | 25'000 |
|  chèvres | 1'000 |
|  loups | 100 |
  Par ailleurs, la pluie est très régulière dans la région et tombe à hauteur de **30 cm/mois**

Votre programme devrait produire l'affichage suivant:
 ```
 fin du mois #1 => nb de choux: 31000 / nb de chèvres: 1100 / nb de loups: 130
 fin du mois #2 => nb de choux: 35500 / nb de chèvres: 1190 / nb de loups: 169
 fin du mois #3 => nb de choux: 38650 / nb de chèvres: 1259 / nb de loups: 219
 fin du mois #4 => nb de choux: 40765 / nb de chèvres: 1291 / nb de loups: 284
 fin du mois #5 => nb de choux: 42400 / nb de chèvres: 1265 / nb de loups: 369
 fin du mois #6 => nb de choux: 44425 / nb de chèvres: 1149 / nb de loups: 479
 fin du mois #7 => nb de choux: 48190 / nb de chèvres: 899 / nb de loups: 622
 fin du mois #8 => nb de choux: 55705 / nb de chèvres: 456 / nb de loups: 808
 fin du mois #9 => nb de choux: 69865 / nb de chèvres: 0 / nb de loups: 565
 fin du mois #10 => nb de choux: 90865 / nb de chèvres: 0 / nb de loups: 395
 fin du mois #11 => nb de choux: 111865 / nb de chèvres: 0 / nb de loups: 276
 fin du mois #12 => nb de choux: 132865 / nb de chèvres: 0 / nb de loups: 193
 ```

Pour réussir cet affichage, il vous est demandé de créer une fonction d'affichage des 3 populations (qui ne fait aucun calcul) et, bien sûr un programme principal qui s'occupera de calculer les données et d'appeler votre fonction d'affichage<br/>

Une fois cette partie terminée, enregistrer votre code sous ***tp1-ccl-etape1_nom_prenom.py***

### Etape 2

*Pour l'étape 2 copier votre fichier et rennomez-le **tp1-ccl-etape2_nom_prenom.py***

L'offfa vous demande maintenant de modifier votre programme afin qu'il affiche les résultats suivants:

1. Le nombre de mois nécessaire à l'extinction des loups.
1. Le numéro du mois à partir duquel les loups commencent leur déclin.
1. Les numéros des mois à partir desquelles les chèvres et les choux commencent leur déclin. <br/>

**NB:** Si une population ne décline jamais, alors on affichera "aucun déclin en xx mois". On entend pas déclin le début de la baisse de population (une baisse d'un mois à un autre autrement dit).

#### Sorties produites pour l'étape 2:
```
Aucune ^_^ faîtes-vous confiance et comparez vos résultats avec vos cammarades.
```
