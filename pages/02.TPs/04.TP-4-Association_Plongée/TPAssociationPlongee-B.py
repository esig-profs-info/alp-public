from __future__ import print_function

def formaterReel(nombreReel):
    return '{:.2f}'.format(nombreReel)

def exercice1():
    noms = ['Londa','Lenita','Beatrice','Kendrick','Genny','Rolf','Meridith','Hilton','Phylis','Candis','Ron','Lecia','Jacquelyn','Gonzalo','Herlinda','Morgan','Han','Sanjuanita','Allie','Fernande','Anna','Gracia','Lula','Merlyn','Tandy','Adah','Ozella','Laureen','Ricky','Miki']
    numeros = [2,3,1,5,7,9,10,12,46,8,20,21,28,32,44,6,11,55,33,60,50,53,45,38,63,66,77,37,29,24]
    niveau = [1,3,3,2,0,1,1,2,3,0,1,1,1,2,3,2,1,3,0,1,1,2,2,1,1,3,2,1,0,2]
    nb_plongees = [20,120,15,150,5,20,30,60,100,8,20,15,30,79,130,66,24,110,8,23,20,77,60,22,30,80,50,20,10,29]	
    nb_cours = [10,50,10,5,2,10,12,40,99,3,10,10,14,14,30,19,10,20,2,13,12,14,12,11,8,13,17,8,10,13]


def exercice2():
    numeros = [100,120,130,121,122,133,144,155,166,177,108,201,303,101,103,203,199,211,199,185]	
    nb_plongees = [3,50,15,5,6,30,10,22,33,2,3,0,7,99,102,5,23,88,17,35]


if __name__ == '__main__':
    
    
    exercice = 2 # Indique quel exercice vous souhaitez tester.
    if 1 == exercice:
        exercice1()
    elif 2 == exercice:
        exercice2(1)
    else:
        print('Exercice inexistant:', exercice)

 
