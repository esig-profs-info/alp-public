# Introduction au concept de liste
# Clément Vogt

# Procédure main()
def main():
    num0 = 1
    num1 = 2
    num2 = 3
    num3 = 4
    num4 = 5
    
    # On veut afficher les 5 données: seule solution si on a 5 variables
    print(num0,end=' ')
    print(num1,end=' ')
    print(num2,end=' ')
    print(num3,end=' ')
    print(num4,end=' ')
    
    # On ne peut pas faire de boucle car chaque instruction porte sur une variable différente
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()