# Série 8, Exercice 2
# Clément Vogt

# Procédures et fonctions
def donnees_valides(fin):
    if fin >= 0:
        return True
    print("Vos données sont invalides.")
    return False

def afficher(numero,valeur):
    print("F(",numero,") = ",valeur,sep='')

# Procédure main()
# Dans cet exercice, on ne sait pas à l'avance combien de fois on va tourner dans la boucle: donc while 
def main():
    fin = int(input("Entrez une valeur de fin"))
    
    if donnees_valides(fin):
        print("Voici les termes de la suite de Fibonacci inférieurs ou égaux à: ",fin,":",sep='')
        
        f0 = 0
        f1 = 1
        
        if fin>=0:
            afficher(0,f0)
        if fin>=1:
            afficher(1,f1)
            
        fn = f0+f1
        nb = 2
        while fn <= fin:
            afficher(nb,fn)
            nb += 1 # Identique à nb = nb + 1
            f0 = f1 # Attention à l'ordre dans lequel on fait cette assignation et la suivante
            f1 = fn
            fn = f0+f1
        
# Appel de la procédure main()
if __name__ == "__main__":
    main()