---
template: item
published: false
---

# Série 9
## Objectifs
Comprendre le concept de **liste**.

Mettre en œuvre le concept de **boucle** pour résoudre des problèmes simples et traiter des **listes**.

===

## Notions pour la série
[Résumé des notions](/espaceprofs/espace%20de%20cv/cours%209)

[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S9Ex1** à **ALP-S9Ex3** et se trouveront dans un dossier nommé **ALP-S9**.

## Énoncé

### Exercice 1
À partir de la donnée des relevés de température pour les 7 jours de la semaine, calculer et afficher :
* La moyenne des températures.
* Le nombre de températures inférieures à 0.


### Exercice 2
À partir de la donnée des relevés des notes de 10 élèves d'une classe, calculer et afficher :
* La moyenne des notes arrondie à une décimale après la virgule.
* Le nombre de notes inférieures à la moyenne (4.0).
* La meilleure note et le numéro d'ordre de cette note (l'indice de la note dans la liste).

<u>Contrainte impérative :</u> les notes saisies seront validées au moyen d'une fonction à résultat booléen.


### Exercice 3
Compléter le script [Fourni_ALP-S9Ex3](./media/Fourni_ALP-S9Ex3.py) de sorte que les différentes procédures-fonctions respectent les spécifications données dans leurs commentaires. Vous remplacerez les commentaires <span style="color:green">''' A COMPLETER '''</span> par les éléments nécessaires.

<u>Exemple d’exécution :</u>

![](./media/image1.png) 

<u>Contrainte impérative :</u> la procédure main() ainsi que les noms des procédures/fonctions à compléter ne doivent pas être modifiés.
