---
template: item
published: true
---

# Série 14
## Objectifs
Utiliser la librairie **NumPy**

Mettre en œuvre le concept de **tableau à deux dimensions (2D)** pour résoudre des problèmes simples

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S14Ex1** à **ALP-S14Ex6** et se trouveront dans un dossier nommé **ALP-S14**.

## Énoncé
### Exercice 1

Créez un tableau numpy en 2 dimensions représentant les notes qu'une classe a obtenues pour un cours. Chaque ligne correspond à un-e étudiant-e et chaque colonne à une épreuve.

Vous pouvez utilisez des données de votre choix, mais faites attention à avoir *un nombre différent* de colonnes et de lignes!

1. Quel est le type de données à utiliser?
2. Demander à l'utilisateur de saisir un numéro d'étudiant-e et afficher les notes obtenues correspondantes, ainsi que la moyenne, la meilleure et la moins bonne note de cet-te étudiant-e.
3. Demander à l'utilisateur de saisir une épreuve et afficher les notes obtenues correspondantes, ainsi que la moyenne, la meilleure et la moins bonne note pour cette épreuve.

### Exercice 2

Sur la base du même tableau, créer deux listes. L'une doit contenir les moyennes pour chaque étudiant-e et l'autre la moyenne pour chaque épreuve. Afficher ces listes.

### Exercice 3

Ajoutez une liste simple contenant les poids pour chaque épreuve. Ecrire un programme qui donne la moyenne d'un-e étudiant-e en fonction d'un tableau de résultats en 2D et d'une liste de poids.

### Exercice 4

Ecrire un programme qui permet de répondre aux questions suivantes:
1. Pour quelle épreuve la classe a eu la meilleure moyenne? et la pire?
2. Combien de notes suffisantes, respectivement insuffisantes y a-t-il eu pour chaque épreuve?

Veillez à bien séparer les calculs des affichages (aucun affichage dans les fonctions de calcul).

### Exercice 5

#### La table de Pythagore
La table de Pythagore est un tableau à deux dimensions de 12 lignes et de 12 colonnes:

|   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| **0** | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| **1** | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 |
| **2** | 0 | 2 | 4 | 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20 | 22 | 24 |
| **3** | 0 | 3 | 6 | 9 | 12 | 15 | 18 | 21 | 24 | 27 | 30 | 33 | 36 |
| **4** | 0 | 4 | 8 | 12 | 16 | 20 | 24 | 28 | 32 | 36 | 40 | 44 | 48 |
| **5** | 0 | 5 | 10 | 15 | 20 | 25 | 30 | 35 | 40 | 45 | 50 | 55 | 60 |
| **6** | 0 | 6 | 12 | 18 | 24 | 30 | 36 | 42 | 48 | 54 | 60 | 66 | 72 |
| **7** | 0 | 7 | 14 | 21 | 28 | 35 | 42 | 49 | 56 | 63 | 70 | 77 | 84 |
| **8** | 0 | 8 | 16 | 24 | 32 | 40 | 48 | 56 | 64 | 72 | 80 | 88 | 96 |
| **9** | 0 | 9 | 18 | 27 | 36 | 45 | 54 | 63 | 72 | 81 | 90 | 99 | 108 |
| **10** | 0 | 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 | 90 | 100 | 110 | 120  
| **11** | 0 | 11 | 22 | 33 | 44 | 55 | 66 | 77 | 88 | 99 | 110 | 121 | 132 |
| **12** | 0 | 12 | 24 | 36 | 48 | 60 | 72 | 84 | 96 | 108 | 120 | 132 | 144 |

<br/>
Chaque cellule de ce tableau contient le produit de l'indice ligne et de l'indice colonne correspondants à la position de la cellule dans le tableau. Par exemple la cellule dont le numéro de ligne est 2 et le numéro de colonne est 3 contiendra le produit 2 * 3, c'est-à-dire 6.

1. Écrire une fonction qui affiche cette table.
1. Écrire une fonction qui prend en paramètre le nombre de ligne et le nombre de colonne puis qui retourne un tableau numpy à 2 dimensions contenant les valeurs.

    *<u>Indication  :</u> Pour créer une grille vide, vous pouvez utiliser la méthode `np.zeros((nb_lignes,nb_colonnes),dtype)` qui créera un tableau comportant nb_lignes, nb_colonnes. Le paramètre dtype spécifie quant à lui le type des données qui sont contenues dans le tableau, dans notre cas ce sont des **int**.*
1. Écrire une fonction qui prend en paramètre un tableau numpy à 2 dimensions contenant les valeurs et  qui affiche la table (avec les en-têtes)

### Exercice 6

#### Carrés magiques d'ordre 3

Un carré d'ordre 3 est un tableau d'entiers à deux dimensions constitué de 3 lignes et de 3 colonnes.
Chaque cellule de ce tableau doit contenir un nombre entier compris entre 1 et 9, à un seul exemplaire.
Le carré est dit magique si la somme calculée sur ses trois lignes, ses trois colonnes, et ses deux diagonales principales est la même.

Cette somme, pour les carrés magiques d'ordre 3, est 15.

Le but de cet exercice est de déterminer si un carré d'ordre 3 donné est magique ou non.

Pour ce faire, il s'agira d'écrire une fonction booléenne qui admettra un carré en entrée, et qui renverra True si le carré est magique, et False sinon.

Par conséquent, cette fonction aura l'entête suivant:

```def est_magique(mon_carre_de_3par3)```

Vous pouvez tester votre fonction avec les données suivantes:

[carre1.txt](./carre1.txt)
[carre2.txt](./carre2.txt)
[carre3.txt](./carre3.txt)
[carre4.txt](./carre4.txt)

```
6	1	8
7	5	3
2	9	4	

6	7	2
1	5	9
8	3	4
			
6	7	5
1	2	9
8	3	4
		
4	3	8
1	5	9
2	7	6

```



