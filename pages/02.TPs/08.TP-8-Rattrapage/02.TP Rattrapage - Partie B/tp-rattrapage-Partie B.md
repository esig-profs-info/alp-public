---
template: item
published: true
---
# TP Rattrapage - partie B

Vous allez désormais utiliser une structure de données supplémentaire, ``tab_rattrapages``.

Il s'agit d'un tableau 2D où les lignes correspondent aux étudiant-e-s et les colonnes aux cours. 
Une cellule contient ``True`` si, et seulement si, 
l'étudiant-e correspondant à la ligne doit rattraper le cours correspondant à la colonne.

Au besoin récupérer la nouvelle version de [RattrapageHelper.py](RattrapageHelper.py).

Voici une représentation lisible de ce tableau:

|             | ALP   | DROIT   | DWEB   | FRTC   | GESTION   | MIRESE   | MODE   | TEPR   | TQGE   |
|:------------|:------|:--------|:-------|:-------|:----------|:---------|:-------|:-------|:-------|
| AREGAZRRO   | True  | False   | False  | True   | False     | False    | False  | False  | False  |
| BUQOH-AKHSO | True  | True    | True   | True   | True      | True     | True   | True   | True   |
| GUFUJU      | False | False   | True   | False  | False     | False    | False  | False  | False  |
| GUKO-PAYQYJ | False | True    | True   | False  | True      | False    | False  | True   | False  |
| HOEC        | True  | False   | False  | True   | False     | True     | True   | False  | True   |
| HUJICY      | True  | False   | False  | True   | False     | False    | False  | False  | False  |
| LJUOSOP     | False | True    | True   | False  | True      | False    | True   | True   | True   |
| MIJGYH      | True  | True    | False  | True   | True      | True     | True   | True   | True   |
| PYGOOPY     | True  | True    | True   | True   | True      | True     | True   | True   | True   |
| RYHXAH      | False | True    | True   | False  | True      | True     | True   | True   | True   |
| SIO         | False | True    | True   | False  | True      | False    | False  | True   | True   |
| UDVO        | True  | True    | False  | True   | True      | True     | True   | True   | True   |
| VEMI        | False | True    | True   | False  | True      | False    | True   | True   | True   |
| VLEZUR      | True  | True    | True   | True   | True      | True     | True   | True   | True   |
| YVEWOW      | True  | False   | False  | True   | False     | False    | False  | False  | False  |
| ZASFA       | True  | True    | True   | True   | True      | True     | True   | True   | True   |
| ZEEJ        | False | True    | True   | False  | True      | False    | True   | True   | True   |
| ZETHODE     | True  | False   | False  | True   | False     | True     | False  | True   | False  |


### Exo B.1 - Interaction
Ecrire une procédure Python qui demande à l'utilisateur un nom, puis un cours 
et qui indique si oui ou non l'étudiant doit faire le rattrapage. 
Afficher un message si le cours ou l'étudiant sont inconnus.

Quelques exemples:
```
Saisir étudiant-e: GUFUJU
Saisir cours: ALP

GUFUJU n'a pas besoin de faire le rattrapage ALP.


Saisir étudiant-e: HOEC
Saisir cours: FRTC

HOEC doit faire le rattrapage FRTC.


Saisir étudiant-e: HOEC
Saisir cours: MMA

Il n'y a pas de cours MMA à l'ESIG.


Saisir étudiant-e: MMA
Saisir cours: ALP

Il n'y a pas d'étudiant MMA à l'ESIG.
```


### Exo B.2 - Le plus d'étudiants
 Ecrire une fonction Python qui détermine le cours où le plus d'étudiants doit passer le rattrapage.
 Ecrire aussi une procédure de test (appel+affichage).

 >*Info* : la réponse est ```le cours TEPR``` (avec 13 étudiants)

 *Supplément* : Ecrire une variante qui indique le cours **et** le nombre d'étudiants. 
 Pour cela, que devez-vous modifier : la fonction de calcul et/ou la procédure d'affichage ?


### Exo B.3 - Dénombrer les étudiants
 Ecrire une fonction Python qui crée un dictionnaire associant à chaque cours le nombre d'étudiants qui doivent passer le rattrapage.
 Ecrire aussi une procédure de test (appel+affichage).

 >*Info* : voici les résultats (ordre non significatif)

|  Cours  | Nombre d'étudiants   |
|---------|:--------------------:|
| ALP     |                   11 |
| MIRESE  |                    9 |
| MODE    |                   11 |
| DROIT   |                   12 |
| DWEB    |                   11 |
| FRTC    |                   11 |
| TQGE    |                   12 |
| GESTION |                   12 |
| TEPR    |                   13 |
|         |                      |


 *Supplément* : Ecrire une variante de la fonction de l'Exo B.2 qui utilise ce dictionnaire.


### Exo B.4 - Liste des étudiants
 Ecrire une fonction Python qui établit la liste des étudiants qui doivent passer un ou plusieurs rattrapages. 
 Chaque nom ne doit apparaître qu'une fois.
 Ecrire aussi une procédure de test (appel+affichage).

 >*Info* : voici le résultat (ordre non significatif)
 ```
 ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 
  'AREGAZRRO', 'HUJICY', 'YVEWOW', 'MIJGYH', 'RYHXAH', 'LJUOSOP', 'VEMI',
  'ZEEJ', 'GUKO-PAYQYJ', 'SIO', 'GUFUJU']
 ```

### Exo B.5- Dictionnaire des durées par étudiant
 Pour cette question, vous aurez besoin du dictionnaire ```dicoDurées``` de la partie A. 
 Ecrire une fonction Python qui range (et renvoie), dans un nouveau dictionnaire, la durée *totale* de rattrapage que doit passer chaque étudiant concerné.
 Ecrire aussi une procédure de test (appel+affichage).

 >*Info* : voici le résultat (ordre non significatif)

|   étudiant  | durée (en minutes) |
|:------------|:---------------:|
| HOEC        |             660 |
| PYGOOPY     |            1020 |
| ZASFA       |            1020 |
| VLEZUR      |            1020 |
| ZETHODE     |             510 |
| UDVO        |             900 |
| BUQOH-AKHSO |            1020 |
| AREGAZRRO   |             330 |
| HUJICY      |             330 |
| YVEWOW      |             330 |
| MIJGYH      |             900 |
| RYHXAH      |             690 |
| LJUOSOP     |             600 |
| VEMI        |             600 |
| ZEEJ        |             600 |
| GUKO-PAYQYJ |             360 |
| SIO         |             420 |
| GUFUJU      |             120 |

### Exo B.6 - Temps de rattrapage le plus élevé
 Pour cette question, il sera beaucoup plus simple d'avoir recours au dictionnaire créé dans l'exercice précédent B.5. 
 Ecrire une fonction Python qui détermine la durée de rattrapage la plus élevée.
 Ecrire aussi une procédure de test (appel+affichage).

 >*Info* : la réponse est ```1020```

 *Supplément* : Modifier la fonction d'une façon ou d'une autre pour répondre à la question : qui sont les étudiants concernés par cette durée de rattrapage maximale ? (sous la forme d'une liste)

 >*Info* : la réponse est

 ```Les étudiants passant 1020 minutes de rattrapage sont PYGOOPY, ZASFA, VLEZUR, BUQOH-AKHSO.```

