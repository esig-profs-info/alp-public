---
template: item
published: true
---

# Cours 2

===

* **Rappels** 
    * Commentaires
    * Variable.
    * Types simples.
    * Déclaration de variables.
    * Opérations.
        * Assignation.
        * Expressions.
    * Procédures d'affichage
    * Saisie de données par l’utilisateur
    * Conversion des types de données
    * L'alternative
    * L’indentation
* **Formants algorithmiques** 
    * Expriment / définissent l'ordre d'exécution des instructions.
    * Il n'y en a que trois avec lesquels on peut exprimer l'algorithme de résolution de n'importe quel problème.
        * Séquence.
        * Alternative.
        * (Boucle).
    * Une alternative peut être considérée comme une nouvelle (super)**instruction** et, en tant que telle, faire partie d'une séquence.
    * Emboîtement, principe des poupées russes.
* <u>Illustration:</u> Exercice T3 de la série 1 (voir [ALP-S1ExT3](./media/ALP-S1ExT3.py))
* <u>Illustration:</u> Exercice 5 de la série 1 version 1 (voir [ALP-S1Ex5v1](./media/ALP-S1Ex5v1.py))
* « **Constantes** » - avantages 
    * Un léger changement de spécifications peut nous amener à parcourir tout le code pour mettre à jour les valeurs constantes qui changent.
    * Il y a un grand risque d'erreur.
    * Il est possible de regrouper toutes ces valeurs en tête de script en déclarant des **constantes**.
    * Une déclaration de constante permet de nommer une valeur constante et de se référer au nom dans le programme.
    * En cas de changement de spécification, il n'y a plus qu'un seul lieu où effectuer les modifications.
    * Conventions de formation du nom:
        * Nom signifiant, exprimant la valeur contenue par la constante.
        * Le nom est entièrement écrit en majuscules et les mots sont séparés par des signes soulignés (_).
        * Exemple : TAUX_RABAIS
    * <u>Illustration:</u> Exercice 5 de la série 1 version 2 (voir [ALP-S1Ex5v2](./media/ALP-S1Ex5v2.py))
* **Problème du test d'un programme** 
    * Toujours tester tous les cas de figure possibles avec des données pour lesquelles on peut facilement vérifier les résultats produits
* **Démarche de résolution de problèmes** (toujours quatre étapes): 
    * Lire l’énoncé
    * Identifier les données (ce qui détermine les variables contenant les saisies de l’utilisateur => input())
    * Identifier les résultats (ce qui détermine les variables contenant les résultats ainsi que les valeurs à afficher)
    * Établir la relation qui lie les données aux résultats (ce qui détermine, et c’est en général le plus difficile, la séquence d’instructions qui résout le problème)
* **Procédures et fonctions** - séquences nommées 
    * Modélisent une tâche nommée. Elles sont le résultat de l'application de la démarche cartésienne.
    * Concept fondamental dans la démarche d'analyse et de développement
    * Une procédure/fonction définit un <u>algorithme</u> (une suite d'opérations dont l'exécution permet d'obtenir les résultats par transformations successives des variables contenant les données en les variables contenant les résultats).
    * Il est possible de transmettre des informations à une procédure/fonction en lui passant des <u>paramètres</u>
    * Leur nom est entièrement écrit en minuscules et les mots sont séparés par des signes soulignés (_).
    * **Procédure**: 
        * Représente une tâche, un traitement spécifique; elle n'a **pas de résultat**.
        * Son nom commence par un **verbe à l'infinitif** <u>signifiant</u>, exprimant la tâche réalisée par la procédure. Exemple : afficher_resultats()
        * Elle peut recevoir des paramètres en entrée (ce sont les données avec lesquelles elle travaille : données du problème, …).
        * Ces paramètres doivent être déclarés dans l'entête de la procédure; ce sont les **paramètres formels** (=noms par lesquels on les désigne dans le code de la procédure).
        * Lors de l'appel de la procédure, il faut lui fournir comme paramètres les valeurs avec lesquelles elle doit effectivement travailler: les **paramètres effectifs**.
        * 
            ```python
            def nom_de_la_procedure(<paramètres formels>):
                <déclarations de variables locales>
                <instructions>
            ```
    * **Fonction**: 
        * Procédure spéciale qui effectue un calcul et **retourne un résultat**.
        * Son nom indique la **valeur** retournée.
        * Elle peut recevoir des paramètres en entrée (ce sont les données avec lesquelles elle travaille : données du problème, …).
        * Ces paramètres doivent être déclarés dans l'entête de la fonction; ce sont les **paramètres formels** (=noms par lesquels on les désigne dans le code de la fonction).
        * Le **type du résultat** dépend du résultat retourné.
        * Le résultat est retourné au moyen de l'instruction return.
        * La fonction doit être appelée <u>dans une expression</u> (=dans un calcul ou à droite d'un symbole d'assignation); il faut lui fournir comme paramètres les valeurs avec lesquelles elle doit effectivement travailler: les **paramètres effectifs**.
        * Elle peut donc être employée partout où une variable du type du résultat peut être employée.
        * Il faut voir une fonction comme une **variable dont la valeur est obtenue par un algorithme de calcul** (dépendant éventuellement de paramètres).
        * Il est possible de retourner plusieurs résultats en les séparant par des virgules (création d’un tuple)
        * 
            ```python
            def nom_de_la_fonction(<paramètres formels>):
                <déclarations de variables locales>
                <instructions>
                return <valeur>
            ```
* **Démarche cartésienne** 
    * Si la tâche est élémentaire, la coder.
    * Sinon, décomposer la tâche en sous-tâches plus élémentaires et appliquer la démarche à chacune de ces tâches.
    * <u>Illustration:</u>
        * Exercice 6 de la série 1 (voir [ALP-S1Ex6v2](./media/ALP-S1Ex6v2.py))
        * Exercice 2 de la série 2 (voir [ALP-S2Ex2v1](./media/ALP-S2Ex2v1.py) et [ALP-S2Ex2v2](./media/ALP-S2Ex2v2.py))
    * [Document](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)
* **Structure utilisée dans le cadre de ce cours pour la création de scripts**
    * Procédure main()
    * Appel de main() doit toujours être la dernière instruction de votre script
    * [Squelette](/ressources/squelette/squelette.py)
* **Portée des variables**
    * Variable locale : peut être utilisée que dans la procédure ou le bloc où elle est définie
    * Variable globale : peut être utilisée dans tout le programme
    * Définir les variables le plus localement possible
* [Illustration des notions](./media/illustration-c02.py)

**<u>Exercices :</u>** [Série 2](/exercices/code/serie%202)