---
template: item
published: false
---

# Fonctions (commandes personnalisées) - Lecture

## Question 1

![](code1.png)

1.1 Qu&#39;est-ce qui s&#39;affiche **après que la ligne 6 a été exécuté**  (mais avant l&#39;exécution de la ligne suivante) ? Dessinez. Indiquez également la position et l&#39;orientation de la tortue actuelle :

1.2 Qu&#39;est-ce qui s&#39;affiche **à la fin du programme**? Dessinez. Indiquez également la position et l&#39;orientation de la tortue actuelle :

## Question 2

![](code2.png)

2.1 Qu&#39;est-ce qui s&#39;affiche après que la ligne 12 a été exécuté (mais avant l&#39;exécution de la ligne suivante) ? Dessinez. Ajoutez également la position et l&#39;orientation de la tortue actuelle :

2.2 Qu&#39;est-ce qui s&#39;affiche après que la ligne 15 a été exécuté (mais avant l&#39;exécution de la ligne suivante) ? Dessinez. Ajoutez également la position et l&#39;orientation de la tortue actuelle :

2.3 Qu&#39;est-ce qui s&#39;affiche à la fin du programme ? Dessinez. Ajoutez également la position et l&#39;orientation de la tortue actuelle :

## Question 3

![](code3.png)

3.1 Qu&#39;est-ce qui s&#39;affiche à la fin du programme? Dessinez. Ajoutez également la position et l&#39;orientation de la tortue actuelle :