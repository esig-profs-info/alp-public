# Série 14, Exercice 6
# Clément Vogt

# Imports
import numpy as np

# Constantes
NB_MIN = 1
NB_MAX = 9
NB_DIMENSIONS = 2
NB_COL_LIG_CARRE_3PAR3 = 3
SOMME_CARRE_3PAR3 = 15

# Procédures et fonctions
def donnees_valides(tab2D):
    return len(tab2D.shape) == NB_DIMENSIONS and tab2D.shape[0] == tab2D.shape[1] == NB_COL_LIG_CARRE_3PAR3 and validation_valeurs_elements(tab2D) and validation_nb_occurences_elements(tab2D)

def validation_valeurs_elements_une_ligne(ligne):
    for element in ligne:
        if element < NB_MIN or element > NB_MAX:
            return False
    return True

def validation_valeurs_elements(tab2D):
    for ligne in tab2D:      
        if validation_valeurs_elements_une_ligne(ligne) == False:
            return False
    return True

def calculer_nb_occurences_elements_une_ligne(ligne,occurences):
    for element in ligne:
        if element in occurences:
            occurences[element] += 1
        else:
            occurences[element] = 1

def nb_occurences_elements(tab2D):
    occurences = {}
    for ligne in tab2D:
        calculer_nb_occurences_elements_une_ligne(ligne,occurences)
    return occurences

def validation_nb_occurences_elements(tab2D):
    occurences = nb_occurences_elements(tab2D)
    for k in occurences:
        if occurences[k] > 1:
            return False
    return True

def somme_une_ligne(ligne):
    somme = 0
    for element in ligne:
        somme += element
    return somme

def est_magique_par_ligne(tab2D):
    for ligne in tab2D:
        if somme_une_ligne(ligne) != SOMME_CARRE_3PAR3:
            return False
    return True

def somme_une_colonne(num_col,tab2D):
    somme = 0
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        somme += tab2D[num_lig,num_col]
    return somme

def est_magique_par_colonne(tab2D):
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):      
        if somme_une_colonne(num_col,tab2D) != SOMME_CARRE_3PAR3:
            return False
    return True  

def est_magique_par_diagonale(tab2D):
    sum_haut_gauche=0
    sum_haut_droite=0
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        sum_haut_gauche+=tab2D[num_lig][num_lig]
        sum_haut_droite+=tab2D[num_lig][nb_lignes-num_lig-1]
    if sum_haut_gauche == sum_haut_droite == SOMME_CARRE_3PAR3:
        return True
    else:
        return False
    
def est_magique(mon_carre_de_3par3):
    return donnees_valides(mon_carre_de_3par3) and est_magique_par_ligne(mon_carre_de_3par3) and est_magique_par_colonne(mon_carre_de_3par3) and est_magique_par_diagonale(mon_carre_de_3par3)

# Procédure main()
def main():
    print("--- Résultats ---")
    for no in range(1,5):
        mon_carre_de_3par3 = np.loadtxt("carre"+str(no)+".txt", dtype=int)
        if est_magique(mon_carre_de_3par3):
            res = " est"
        else:
            res = " n'est pas"
        print("Le carré n°",no,res," magique.",sep="")
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()
