import numpy as np


dicoDurées = {'FRTC':    90, 'MODE':    180, 'ALP': 240, 'MIRESE' :  90,
              'TEPR':    90, 'TQGE':    60, 'DROIT' :   90, 'GESTION' : 60,
              'DWEB' :    120}


etudiantes = ['AREGAZRRO', 'BUQOH-AKHSO', 'GUFUJU', 'GUKO-PAYQYJ', 'HOEC',
              'HUJICY', 'LJUOSOP', 'MIJGYH', 'PYGOOPY', 'RYHXAH',
              'SIO', 'UDVO', 'VEMI', 'VLEZUR', 'YVEWOW',
              'ZASFA', 'ZEEJ', 'ZETHODE']

cours = ['ALP', 'DROIT', 'DWEB', 'FRTC', 'GESTION', 'MIRESE', 'MODE', 'TEPR', 'TQGE']


# Tableau indiquant si l'étudiant-e correspondant à la ligne doit rattraper
# l'épreuve du cours correspondant à la colonne.
# Les lignes et colonnes suivent l'ordre des listes ci-dessous (etudiantes et cours).
tab_rattrapages = np.array(
    [[True, False, False, True, False, False, False, False, False],
     [True, True, True, True, True, True, True, True, True],
     [False, False, True, False, False, False, False, False, False],
     [False, True, True, False, True, False, False, True, False],
     [True, False, False, True, False, True, True, False, True],
     [True, False, False, True, False, False, False, False, False],
     [False, True, True, False, True, False, True, True, True],
     [True, True, False, True, True, True, True, True, True],
     [True, True, True, True, True, True, True, True, True],
     [False, True, True, False, True, True, True, True, True],
     [False, True, True, False, True, False, False, True, True],
     [True, True, False, True, True, True, True, True, True],
     [False, True, True, False, True, False, True, True, True],
     [True, True, True, True, True, True, True, True, True],
     [True, False, False, True, False, False, False, False, False],
     [True, True, True, True, True, True, True, True, True],
     [False, True, True, False, True, False, True, True, True],
     [True, False, False, True, False, True, False, True, False]]
)
