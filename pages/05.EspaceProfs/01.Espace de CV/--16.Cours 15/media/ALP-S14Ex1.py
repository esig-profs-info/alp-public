# Série 14, Exercice 1, Version 1
# Clément Vogt

# Imports
import numpy as np

# CONSTANTE
TYPE_ETUDIANT = 1

# Procédures et fonctions
def afficher_une_ligne(num_lig,tab2D):
    print("Notes:",end=" ")
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        print(tab2D[num_lig,num_col], end=" ")
    print()
    
def afficher_une_ligne_v2(num_lig,tab2D):
    print("Notes:",end=" ")
    for elem in tab2D[num_lig]:
        print (elem, end=" ")
    print()

def afficher_une_colonne(num_col,tab2D):
    print("Notes:",end=" ")
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        print(tab2D[num_lig,num_col], end=" ")
    print()

def moyenne_une_ligne(num_lig,tab2D):
    somme = 0
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        somme += tab2D[num_lig,num_col]
    return somme / nb_colonnes

def moyenne_une_colonne(num_col,tab2D):
    somme = 0
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        somme += tab2D[num_lig,num_col]
    return somme / nb_lignes

def maximum_une_ligne(num_lig,tab2D):
    note_max = tab2D[num_lig,0]
    nb_colonnes = tab2D.shape[1]
    for num_col in range(1,nb_colonnes):
        note_courante = tab2D[num_lig,num_col]
        if  note_courante > note_max:
            note_max = note_courante     
    return note_max

def maximum_une_colonne(num_col,tab2D):
    note_max = tab2D[0,num_col]
    nb_lignes = tab2D.shape[0]
    for num_lig in range(1,nb_lignes):
        note_courante = tab2D[num_lig,num_col]
        if  note_courante > note_max:
            note_max = note_courante     
    return note_max 

def minimum_une_ligne(num_lig,tab2D):
    note_min = tab2D[num_lig,0]
    nb_colonnes = tab2D.shape[1]
    for num_col in range(1,nb_colonnes):
        note_courante = tab2D[num_lig,num_col]
        if  note_courante < note_min:
            note_min = note_courante     
    return note_min

def minimum_une_colonne(num_col,tab2D):
    note_min = tab2D[0,num_col]
    nb_lignes = tab2D.shape[0]
    for num_lig in range(1,nb_lignes):
        note_courante = tab2D[num_lig,num_col]
        if  note_courante < note_min:
            note_min = note_courante     
    return note_min

def afficher_resultats(type_affichage,numero,tab2D,moyenne,note_max,note_min):
    if type_affichage == TYPE_ETUDIANT:
        print("--- Notes de l'étudiant n°",numero," ---",sep="")
        afficher_une_ligne(numero,tab2D)
    else:
        print("\n --- Notes de l'épreuve n°",numero," ---",sep="")
        afficher_une_colonne(numero,tab2D)    
    print("Moyenne:",moyenne)
    print("Meilleure note:",note_max)
    print("Moins bonne note:",note_min)

# Procédure main()
def main():
    tab2D_notes = np.array([[5,4,3],[6,4.5,5.5],[6,6,6],[2.5,3,2],[4,4.5,4.5]])
    num_etu = int(input("Entrez un numéro d'étudiant"))
    num_epr = int(input("Entrez un numéro d'épreuve")) 
    moyenne_etu = moyenne_une_ligne(num_etu,tab2D_notes)
    note_max_etu = maximum_une_ligne(num_etu,tab2D_notes)
    note_min_etu = minimum_une_ligne(num_etu,tab2D_notes)
    afficher_resultats(1,num_etu,tab2D_notes,moyenne_etu,note_max_etu,note_min_etu)  
    moyenne_epr = moyenne_une_colonne(num_epr,tab2D_notes)
    note_max_epr = maximum_une_colonne(num_epr,tab2D_notes)
    note_min_epr = minimum_une_colonne(num_epr,tab2D_notes)
    afficher_resultats(0,num_epr,tab2D_notes,moyenne_epr,note_max_epr,note_min_epr)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()