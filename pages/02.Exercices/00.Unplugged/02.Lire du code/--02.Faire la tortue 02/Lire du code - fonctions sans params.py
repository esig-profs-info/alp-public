from gturtle import *

def setup():
    makeTurtle()
    speed(-1)

def finalize():
    hideTurtle()
    
def b():
   forward(10)
   left(90)

def c():
    forward(20)
    right(90) 

def a():
    repeat 4:
        b()
        c()
        b()

setup()
a()
finalize()