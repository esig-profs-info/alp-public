"""Quiz préparé par Fabricio pour tester votre compréhension du fillPath"""

from gturtle import *

makeTurtle()


setPenColor("sandybrown")
setFillColor("sandybrown")
startPath()
forward(100)
right(90)
penUp()
forward(50)
penDown()
forward(50)
right(90)
penUp()
forward(20)
left(90)
penDown()
forward(100)
fillPath()





