---
template: item

---

Activité numéro 2 avec des gobelets. Il s'agit ici d'être capable de lire du code

===

##Exercices - Gobelets
###Activité n°2 - Lecture du code
#### Contexte:
* Activité par groupe de 2 étudiant-e-s

#### Ce que l'on retient de l'activité précédente:
À l’intérieur d’un robot, il y a un **ordinateur**. Cet ordinateur va pouvoir fonctionner grâce à un **processeur**. Pour que le robot puisse fonctionner, il va falloir qu’il comprenne et parle **le même langage que le processeur**. Ce **langage**, nous l’appellerons : **langage de programmation**.<br/>

On entend par **algorithme** la méthode composée d'une (ou plusieurs) séquence(s) d'**instructions** simples qui ser(ven)t à résoudre un **problème** donné. Cet algorithme peut être traduit dans différents **langages de programmation** afin de pouvoir être éxécutés sur un ordinateur (par l'intermédiaire, notamment, du **processeur**).<br/><br/>

##### Question:
le processeur d'un ordinateur, comprend-il les langages de programmation? Argumentez!


#### Voici la correction du langage (lors de l'activité 1) + une nouvelle instruction:
![nouvelle grille langage](grille_notre_language_plus_1.jpg)
...à vous de la définir :)
#### Ensuite...:
Par deux, à tour de rôle et en s’entraidant, vous allez lire et exécuter le code de chacun des algorithmes ci-dessous. Dessinez le résultat (en schématisant les gobelets). Vous pouvez vous aider des gobelets gentiment mis à disposition par votre enseignant.

#####Algo n°1:
![algo fléché 1](algo1_bis.jpg)
#####Algo n°2:
![algo fléché 2](algo2.jpg)
#####Algo n°3:
![algo fléché 3](algo3.jpg)
#####Algo n°4:
![algo fléché 4](algo4.jpg)
#####Algo n°5:
![algo fléché 4](algo5.jpg)
