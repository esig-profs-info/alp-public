---
template: item
published: true
---

hors-série - facile

===

## Exercice 1
À partir de la donnée du rayon d'un cercle, faites calculer et afficher la surface et la circonférence de ce cercle.

#### Tips
Pour avoir une valeur précise de Pi, vous pouvez utiliser l'import suivant en haut de votre programme, ensuite vous pourrez utiliser la constante pi:
```
from math import pi
```

#### Sorties produites:
**Avec donnée de 1.0 cm en entrée pour le rayon:**
```
Calcul de la surface et de la circonférence d'un cercle étant donné son rayon

Donnée:
Rayon du cercle: rayon = 1.0 cm

surface du cercle : 3.141592653589793 cm2
Circonférence du cercle : 6.283185307179586 cm
```

**##### **Avec donnée de 10 en entrée pour le rayon:**
```
Calcul de la surface et de la circonférence d'un cercle étant donné son rayon

Donnée:
Rayon du cercle: rayon = 10 cm

surface du cercle : 314.1592653589793 cm2
Circonférence du cercle : 62.83185307179586 cm
```

**Avec donnée de 23.4 en entrée pour le rayon:**
```
Calcul de la surface et de la circonférence d'un cercle étant donné son rayon

Donnée:
Rayon du cercle: rayon = 23.4 cm

surface du cercle : 1720.2104733996268 cm2
Circonférence du cercle : 147.0265361880023 cm
```

#### En plus...
Au lieu d'entrer la valeur du rayon en dur dans votre programme, demandez-là plutôt à l'utilisateur (vous pouvez utiliser la fonction input() de python).<br/>
A quel problème êtes-vous alors confronté(e)?

## Exercice 2
À partir de la donnée de la base et de la hauteur d'un triangle, calculer et afficher sa surface.

#### Sorties produites:
##### Avec donnée de 1.0 cm en entrée pour le rayon:
```
Calcul de la surface d'un triangle étant donné sa base de 1.0 cm et une hauteur de 1.0 cm

Données:
  Base du triangle: base = 1.0 cm
  Hauteur du triangle: hauteur = 1.0 cm

  Surface du triangle: surface = 0.5 cm2
```

##### Avec donnée de 10 en entrée pour le rayon:
```
Calcul de la surface d'un triangle étant donné sa base de 3.0 cm et une hauteur de 4.0 cm

Données:
  Base du triangle: base = 3.0 cm
  Hauteur du triangle: hauteur = 4.0 cm

  Surface du triangle: surface = 6.0 cm2
```

##### Avec donnée de 23.4 en entrée pour le rayon:
```
Calcul de la surface d'un triangle étant donné sa base de 34.2 cm et une hauteur de 21.3 cm

Données:
  Base du triangle: base = 34.20000076293945 cm
  Hauteur du triangle: hauteur = 21.29999923706055 cm

  Surface du triangle: surface = 364.2299950790402 cm2
```

#### En plus...
Au lieu d'entrer les valeurs de la base et de la hauteur en dur dans votre programme, demandez-les plutôt à l'utilisateur (vous pouvez utiliser la fonction input() de python).

## Exercice 3
Ecrire un programme qui calcule les racines carrées de nombres fournis par l'utilisateur (en entrée). Il(le programme) s'arrêtera lorsqu'on lui fournira la valeur 0. Il refusera les valeurs négatives. Son exécution se présentera ainsi:

```
Donnez un nombre positif: 2
sa racine carrée est 1.4142135623730951
donnez un nombre positif: -1
positif svp!
donnez un nombre positif: 5
sa racine carrée est 2.23606797749979
donnez un nombre positif : 0
Ok, au revoir :-(
```
**NB:**<br/>
Rappelons que la fonction sqrt (qui nécessite l'import du [module math](https://docs.python.org/3/library/math.html?traget=_blank)) retourne la racine carrée de la valeur qu'on lui passe en paramètre (en argument).<br/><br/>
*NB: plusieurs solutions sont possibles*