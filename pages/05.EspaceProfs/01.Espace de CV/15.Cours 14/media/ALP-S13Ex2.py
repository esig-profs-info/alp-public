# Série 13, Exercice 2
# Clément Vogt

# Constantes
PRIX = {"bananes": 4, "pommes": 2, "oranges": 1.5,"poires": 3}
STOCK = {"bananes": 100, "pommes": 290, "oranges": 50, "poires": 120}

# Procédure main()
def main():
    fruit = input("Saisissez le nom d'un fruit")
    
    if fruit in PRIX:
        print("Les",fruit,"ont un prix de",PRIX[fruit],"CHF et il y en a",STOCK[fruit],"en stock.")
    else:
        print("Le fruit demandé est inconnu dans notre catalogue.")
    
    print("\nMontants des stocks:")
    for k in PRIX:
        print(k,":",round(PRIX[k]*STOCK[k]),"CHF")
         
# Appel de la procédure main()
if __name__ == "__main__":
    main()