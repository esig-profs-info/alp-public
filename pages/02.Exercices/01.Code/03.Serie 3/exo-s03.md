---
template: item
published: true
---

# Série 3
## Objectifs
Approfondir le concept d'**alternative** : Mettre en œuvre le **formant de luxe** (elif).

**Décomposer** la solution d'un problème en **tâches**.

Mettre en pratique les concepts de **procédures**, de **fonction** et de **paramètre**.


===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

[La bibliothèque standard](https://docs.python.org/fr/3/library/)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S3Ex1** à **ALP-S3Ex5** et se trouveront dans un dossier nommé **ALP-S3**.

## Énoncé

### Exercice 1
À partir de la donnée du rayon d'un cercle (valeur entière), calculer et afficher l'aire et la circonférence de ce cercle.

(Rappel : on ne met jamais dans la même procédure des calculs et des affichages)

(Remarque : vous trouverez dans le module [math](https://docs.python.org/fr/3/library/math.html) la valeur de pi)

<u>Exemples d’exécution :</u>

![](./media/image1.png)

### Exercice 2
Le magasin PTIPRI souhaite avoir un programme qui lui imprime les factures des clients achetant deux articles simultanément, et obtenant ainsi un rabais de 50% sur l'article le moins cher. Voici les 3 parties qui doivent apparaître sur la facture : 
> * l'entête doit indiquer le nom du magasin, avec le numéro du client
> * il faut ensuite indiquer le prix des articles achetés, ainsi que le prix à payer
> * les conditions générales sont imprimées en bas de facture.

Le programme que vous êtes chargés de réaliser affichera donc cette facture sur la base du numéro de client et du prix des deux articles achetés.

<u>Exemples d’exécution :</u> 

![](./media/image2.png)
![](./media/image3.png)
  
### Exercice 3
Refaites l'exercice ALP-S1Ex5 (Entreprise de vente de pièces détachées) en séparant correctement les calculs de l'affichage des résultats. 

<u>Rappel de l'énoncé :</u> Une entreprise de vente de pièces détachées désire automatiser le calcul des factures de ses clients. La somme à payer varie en fonction du prix unitaire et du nombre de pièces commandées de la manière suivante : 
> * Une commande de 100 pièces ou moins ne bénéficie d'aucun rabais.
> * Une commande de plus de 100 pièces bénéficie d'un rabais de 10%.

Votre programme contiendra donc les 2 procédures suivantes : calcul_rabais et afficher_prix_a_payer.

<u>Exemples d’exécution :</u>

![](./media/image4.png)
![](./media/image5.png)

### Exercice 4
Suite à la votation d'hier sur la nouvelle constitution pour Genève, le canton va reverser aux communes le Fonds Réservé pour l'Adoption de la Nouvelle Constitution. Ce FRANC sera réparti en fonction du nombre de votants par commune, et du score réalisé, selon les critères suivants :
> * les communes reçoivent l'un des montants de base suivants, selon le nombre de votes :
>	* toutes les communes recevront au minimum CHF 5'000.-
>	* ou alors CHF   8'000.- s'il y a plus de 1000 votants
>	* ou alors CHF 12'000.- s'il y a plus de 2000 votants
> * selon le score réalisé, un montant peut être rajouté au montant de base :
>	* 2'000.- de plus si le OUI l'a emporté
>	* et un montant supplémentaire de CHF 4'000.- est encore versé s'il y a eu plus de 65% de OUI.
	
Le programme que vous devez écrire effectue les calculs conformément aux règles énoncées ci-dessus et affiche le montant versé pour une commune d'après le nombre de OUI et de NON. Vous devez respecter le format d'affichage représenté ci-dessous.

<u>Exemples d’exécution :</u>

![](./media/image6.png)
![](./media/image7.png)
![](./media/image8.png)
![](./media/image9.png)
![](./media/image10.png)

### Exercice 5
Une entreprise de vente de pièces détachées désire automatiser le calcul des factures pour ses clients. La somme à payer varie en fonction du nombre de pièces de la manière suivante :

> Les clients achetant au moins 50 pièces bénéficient d'un rabais de 3%, ceux qui achètent au moins 100 pièces obtiennent un rabais de 5%, enfin, ceux qui achètent 1000 pièces ou plus obtiennent un rabais de 10%.
> 
> Sur le prix obtenu, les clients doivent payer la TVA qui se monte à 8,2% pour des lots comprenant jusqu'à 250 pièces y compris, à 6,3% pour les lots comprenant jusqu'à 500 pièces y compris et à 4,5% pour les lots de plus de 500 pièces.

Le programme que vous êtes chargés de réaliser calcule la somme à payer à partir de la donnée du nombre de pièces et du prix unitaire de la pièce. Il affiche le nombre de pièces, le prix d'une pièce, le taux de rabais, le taux de TVA ainsi que la somme à payer.

<u>Exemples d’exécution :</u>

Vous devez mener des tests d'exécution personnels, adaptés au code que vous avez développé.



