---
template: item
published: true
---

# Cours 7

===

* **Correction Série 6**
    * Adjonction de la validation des données à 2 exercices de la Série 4: 
        * On applique les conditions définies dans les spécifications (l'énoncé).
        * Les valeurs limites sont évidemment définies par des constantes qu'on pourra ainsi modifier en cas de changement de spécifications.
        * Voir [ALP-S6Ex1](./media/ALP-S6Ex1.py) et [ALP-S6Ex2](./media/ALP-S6Ex2.py)
    * <u>Rappel:</u> la validation peut être testée en **cascade** ou en **séquence**. Si, on choisit en **séquence** toutes les erreurs sont mentionnées (alors qu'en **cascade**, il n'y a que la première erreur détectée qui est mentionnée).
* **Boucle for**
    * Illustration de **for** et **%** en corrigeant les exercices 1 des séries 5 et 7
        * Voir [ALP-S5Ex4](./media/ALP-S5Ex4.py) et [ALP-S7Ex1](./media/ALP-S7Ex1.py)
        * Pour [ALP-S7Ex1](./media/ALP-S7Ex1.py), observer qu'il y a deux variantes possibles, l'une plus performante que l'autre
    * Indications sur les problèmes posés par l'exercice 2 (en cas de valeur non paire de **n**).

**<u>Exercices :</u>** [Série 7](/exercices/code/serie%207)