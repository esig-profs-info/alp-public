---
template: item
published: true
---

# Cours 14

===

* **Correction de la série 13**
    * Série 13, exercice 1 ([solution](./media/ALP-S13Ex1.py))
    * Série 13, exercice 2 ([solution](./media/ALP-S13Ex2.py))
    * Série 13, exercice 3 ([solution](./media/ALP-S13Ex3.py))
    * Série 13, exercice 4 ([solution](./media/ALP-S13Ex4.py))
* **Tableau à deux dimensions (2D)**
    * Un tableau à deux dimensions (2D) est un tableau (à une dimension) dont chaque composante est un tableau (à une dimension).
    * Nous allons utiliser NumPy pour travailler avec ces derniers
        * NumPy signifie "Numerical Python"
        * NumPy est une librairie Python utilisée pour travailler avec des tableaux.
    * En Python, les listes jouent le rôle de tableau mais elles sont lentes à traiter. NumPy vise à fournir un objet tableau qui est jusqu'à 50 fois plus rapide que les listes Python traditionnelles.
        * Les tableaux sont très fréquemment utilisés en science des données (data science), où la vitesse et les ressources sont très importantes.
    * Schéma illustrant un tableau à deux dimensions (2D):
    
        ![](./media/tab2D_1.PNG)
        ![](./media/tab2D_2.PNG)
    * Ressources:
        * [Demo](/ressources/numpy%20demo)
        * [w3schools](https://www.w3schools.com/python/numpy/)
        
**<u>Exercices :</u>** [Série 14](/exercices/code/serie%2014)