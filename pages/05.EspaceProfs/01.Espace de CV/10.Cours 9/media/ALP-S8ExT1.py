# Série 8, Exercice T1
# Clément Vogt

# Imports
from turtle import *

# CONSTANTES
COULEUR_1 = "yellow"
COULEUR_2 = "green"
COULEUR_3 = "blue"
COULEUR_4 = "red"

# Procédures et fonctions
def initialisation():
    speed(-1)

def couleur_suivante(couleur):
    if couleur == COULEUR_1:
        couleur = COULEUR_2
    elif couleur == COULEUR_2:
        couleur = COULEUR_3
    elif couleur == COULEUR_3:
        couleur = COULEUR_4
    else:
        couleur = COULEUR_1
        
    return couleur
 
# Procédure main()
def main():
    longueur_initial = int(input("Entrez une longueur initiale"))
    increment = int(input("Entrez un incrément"))
    longueur_maximale = int(input("Entrez une longueur maximale"))
    
    initialisation()
    
    couleur = COULEUR_1
    longueur = longueur_initial
    while longueur <= longueur_maximale:
        color(couleur)
        forward(longueur)
        right(90)
        couleur = couleur_suivante(couleur)
        longueur += increment
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()