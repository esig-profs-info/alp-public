---
template: item
published: false
---

Dessinez les résultats des extraits de code ci-dessous.

L'importation du module `gturtle` et l'appel à `makeTurtle()` ne sont pas montrés, mais bel et bien faits.


### Extrait 01
```python
def a():
   forward(100)
   setPenColor("red")

a()
```

### Extrait 02
```python
def b():
   forward(10)
   left(90)

def c():
    forward(20)
    right(90) 

def a():
    repeat 4:
        b()
        c()
        b()
a()
```

### Extrait 03
```python
def b():
   forward(10)
right(90)
def c():
    right(45) 
def a():
    repeat 2:
        b()
        c()
    b()
a()
```

### Extrait 04
```python
def b():
   forward(10)
   right(90)
def c():
    right(45) 
def a():
    repeat 2:
        b()
        c()
    b()
```

### Extrait 05
```python
def avancerNfois(x, distance):
    repeat x:
        back(distance)

def tournerXfois(n,angle):
    repeat n:
        forward(angle)
        right(n)

repeat 4:
    avancerNfois(2, 10)
    tournerXfois(1, 90)
```

### Extrait 06
```python
def a(b):
    b(b)

def b(a):
    repeat a:
        c(a)

def c(a):
    forward(a)
    right(360/a)

a(6)
```


