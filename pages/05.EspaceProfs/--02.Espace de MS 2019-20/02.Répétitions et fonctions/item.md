
Des solutions possibles pour Exercice 1 de [Tortue, Répétition, Exo 1](http://www.tigerjython.fr/franz/index.php?inhalt_links=navigation.inc.php&inhalt_mitte=turtle/wiederholung.inc.php).



## Avec boucle, sans fonction
```python

from gturtle import * 

makeTurtle()

repeat 7:
    fd(20)
    rt(90)
    fd(20)
    lt(90)
```

## Sans boucle, avec fonction sans paramètre

===

```python
from gturtle import * 

makeTurtle()

def creerMarche():
    fd(20)
    rt(90)
    fd(20)
    lt(90)
    
creerMarche()
creerMarche()
creerMarche()
creerMarche()
creerMarche()
creerMarche()
creerMarche()

```


## Avec boucle et fonction sans paramètre
```python
from gturtle import * 

makeTurtle()

def creerMarche():
    fd(20)
    rt(90)
    fd(20)
    lt(90)
   
repeat 7: 
    creerMarche()

```

## Avec foncton creerEscalier (appel de fonction depuis le corps d'une autre fonction)
```python
from gturtle import * 

makeTurtle()

def creerMarche():
    fd(20)
    rt(90)
    fd(20)
    lt(90)

def creerEscalier():
    repeat 7: 
        creerMarche()

creerEscalier()



```

## Avec un paramètre pour le nombre de marches
```python
from gturtle import * 

makeTurtle()

def creerMarche():
    fd(20)
    rt(90)
    fd(20)
    lt(90)

def creerEscalier(nbMarches):
    repeat nbMarches: 
        creerMarche()

creerEscalier(7)

```

## Avec un paramètre pour la longueur des marches
```python
from gturtle import * 

makeTurtle()

def creerMarche(longueur):
    fd(longueur)
    rt(90)
    fd(longueur)
    lt(90)

def creerEscalier(nbMarches):
    repeat nbMarches: 
        creerMarche(5)

creerEscalier(8)



```