# Série 2, Exercice 2
# Clément Vogt
# Version 2: La prime est calculée au moyen d'une seule procédure paramétrée

# Constantes
ANNEE_COURANTE = 2020
LIM_ANNEES_SANS_PRIME = 5
LIM_ANNEES_PRIME_1 = 10
TAUX_PRIME_1 = 0.03 # Même chose que 3/100
TAUX_PRIME_2 = 0.07

# Procédures et fonctions
def donnees_entree():
    salaire_base = float(input("Entrez un salaire de base"))
    annee_engagement = int(input("Entrez une année d'engagement"))
    return salaire_base, annee_engagement
    
def calcul_prime(sal,taux):
    p = sal * taux
    return p

def afficher_resultats(sal_base, nb_a, prime, sal_brut):
    print("Salaire de base: " , sal_base , sep='')
    print("Nombre d'années: " , nb_a , " ans" , sep='')
    print("Prime: " , prime , sep='')
    print("Salaire brut: " , sal_brut , sep='')

# Procédure main()
def main():
    salaire_base, annee_engagement = donnees_entree()

    anciennete = ANNEE_COURANTE - annee_engagement
    
    if anciennete < LIM_ANNEES_SANS_PRIME:
        prime = 0
    else:
        if anciennete <= LIM_ANNEES_PRIME_1:
            prime = calcul_prime(salaire_base,TAUX_PRIME_1) 
        else:
            prime = calcul_prime(salaire_base,TAUX_PRIME_2)
            
    salaire_brut = salaire_base + prime
    
    afficher_resultats(salaire_base,anciennete,prime,salaire_brut)

# Appel de la procédure main()
if __name__ == "__main__":
    main()