---
template: item
published : true
title: Photos des tableaux de TS
---
##### 10.09.2019 
_proposition corigée de John pour Gobelets04/ Photo 04 (intégrant l'utilisation de variables)_
![tablo1](20190910_1.jpg?cropResize=1200,1200)

##### 09.09.2019 
à venir, cours remplacé par MS

##### 04.09.2019 
![tablo1](20190904_1.jpg?cropResize=1200,1200)

##### 03.09.2019
![tablo1](20190903_1.jpg?cropResize=1200,1200)
![tablo2](20190903_2.jpg?cropResize=1200,1200)
![tablo3](20190903_3.jpg?cropResize=1200,1200)
![tablo4](20190903_4.jpg?cropResize=1200,1200)
![tablo5](20190903_5.jpg?cropResize=1200,1200)
![tablo6](20190903_6.jpg?cropResize=1200,1200)
![tablo7](20190903_7.jpg?cropResize=1200,1200)

##### 02.09.2019
![tablo1](20190902_1.jpg?cropResize=1200,1200)
![tablo2](20190902_2.jpg?cropResize=1200,1200)
![tablo3](20190902_3.jpg?cropResize=1200,1200)

##### 28.08.2019
![tablo1](20190828_1.jpg?cropResize=1200,1200)
![tablo2](20190828_2.jpg?cropResize=1200,1200)
