# Série 11, Exercice 3
# Clément Vogt

# Procédures et fonctions
def ajoute_au_milieu(s1,s2):
    longueur_s1 = len(s1)
    milieu = longueur_s1//2
    if longueur_s1 % 2 != 0:
        milieu += 1  
    
    nouvelle_chaine = ""
    for i in range(0,milieu):
       nouvelle_chaine+=s1[i]
    nouvelle_chaine+=s2
    for i in range(milieu,len(s1)):
       nouvelle_chaine+=s1[i]    
    
    return nouvelle_chaine

def afficher_resultats(nouvelle_chaine):
    print(nouvelle_chaine)

# Procédure main()
def main():
    afficher_resultats(ajoute_au_milieu("python","COUCOU"))
    afficher_resultats(ajoute_au_milieu("sandbox","COUCOU"))
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()