# Série 9, Exercice 5
# Clément Vogt

# Constantes
RELEVES = [11.8, 14.4, 18.6, 16.5, 11.5, 12.3, 9.1]
LIM_TEMP = 15

# Procédures et fonctions
def moyenne(liste):
    somme = 0
    for element in liste:
        somme = somme + element
        
    return somme / len(liste)

def nombre_inferieurs_a_limite(liste):
    cpt = 0
    for element in liste:
        if element < LIM_TEMP:
            cpt = cpt + 1
            
    return cpt

def afficher(m,nb_neg):
    print("La moyenne est: ",m,sep='')
    print("Le nombre de températures inférieures à 15 degrés est: ",nb_neg,sep='')

# Afficher toutes les températures jusqu'au jour (non compris) où la température est inférieure à 11.8
# Boucle while, car on ne sait pas combien de fois on va tourner dans la boucle
# Attention, il est nécessaire de s'assurer qu'on ne désigne pas de composantes de temp inexistantes
#    sinon, on aura une erreur à l'exécution sur la condition de la boucle while. De plus, ce test doit
#    être fait préalablement; ainsi, gràce au "short cut" la deuxième partie de la condition, qui est erronée
#    lorsque la première a la valeur False, ne sera pas exécutée dans ce cas.
def afficher_jusqu_a_negative(liste):
    print("Le segment initial tant que c'est des températures >= à 11.8: ", end='')
    ind = 0
    while (ind < len(liste) and liste[ind] >= 11.8):
        print(liste[ind],end=' ')
        ind += 1

# Procédure main()
def main():
    moy = moyenne(RELEVES)
    nb_inf = nombre_inferieurs_a_limite(RELEVES)
    afficher(moy,nb_inf)
    afficher_jusqu_a_negative(RELEVES)

# Appel de la procédure main()
if __name__ == "__main__":
    main()