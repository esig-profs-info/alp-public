# Série 2, Exercice 1
# Clément Vogt

# ---------------------------------------------------------------------------
# V1: Version très moche, car à chaque exécution on fait les 3 tests.
# ---------------------------------------------------------------------------
def main():
    nombre = int(input("Entrez un nombre entier:"))

    print("Le nombre ", nombre , " est ",end='',sep='')
    
    if nombre == 0:
        print("nul")
    if nombre < 0:
        print("négatif")
    if nombre > 0:
        print("positif")

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# V2: Version meilleure, car dès que le résultat est connu, on ne fait pas les test suivants (donc moins de travail).
# ---------------------------------------------------------------------------
def main():
    nombre = int(input("Entrez un nombre entier:"))

    print("Le nombre ", nombre , " est ",end='',sep='')
    
    if nombre == 0:
        print("nul")
    else:
        if nombre < 0:
            print("négatif")
        else:
            if nombre > 0:
                print("positif")

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# V3: Le dernier test de la V2 est inutile, car si un nombre n'est ni nul, ni négatif, il ne peut être que positif.
# ---------------------------------------------------------------------------
def main():
    nombre = int(input("Entrez un nombre entier:"))

    print("Le nombre ", nombre , " est ",end='',sep='')
    
    if nombre == 0:
        print("nul")
    else:
        if nombre < 0:
            print("négatif")
        else:
            print("positif") # Le 3e test est inutile
        
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# V4: Simplification d'écriture, l'effet est identique à la V3; tout le monde est indenté au même endroit 
# ---------------------------------------------------------------------------
def main():
    nombre = int(input("Entrez un nombre entier:"))

    print("Le nombre ", nombre , " est ",end='',sep='')
    
    if nombre == 0:
        print("nul")
    elif nombre < 0:
        print("négatif")
    else:
        print("positif") # Le 3e test est inutile

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# V5: Suivant la nature du problème, on peut optimiser l'ordre des tests:
# Si par exemple, on sait que la probabilité de la saisie d'un nombre positif est plus grande que les deux autres cas,
# il faut faire ce test en premier; on fera ainsi une seul test dans la plupart des cas alors que c'était 2 dans la V4.
# Exemple: si on saisit un salaire - c'est toujours positif, sauf en cas d'erreur de saisie.
# ---------------------------------------------------------------------------
def main():
    nombre = int(input("Entrez un nombre entier:"))

    print("Le nombre ", nombre , " est ",end='',sep='')
    
    if nombre > 0:
        print("positif")
    elif nombre < 0:
        print("négatif")
    else:
        print("nul") # Le 3e test est inutile
        
if __name__ == "__main__":
    main()
