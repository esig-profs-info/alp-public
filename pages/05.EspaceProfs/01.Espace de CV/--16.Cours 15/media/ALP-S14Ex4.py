# Série 14, Exercice 4
# Clément Vogt

# Imports
import numpy as np

# Constantes
NOTE_POUR_SUFFISANT = 4

# Procédures et fonctions
def moyenne_une_colonne(num_col,tab2D):
    somme = 0
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        somme += tab2D[num_lig,num_col]
    return somme / nb_lignes

def moyenne_par_colonne(tab2D):
    liste = []
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        moyenne = moyenne_une_colonne(num_col,tab2D)
        liste.append(moyenne)
    return liste

def position_maximum(liste):
    ind_meilleur = 0
    for k in range(1, len(liste)):
        if liste[k] > liste[ind_meilleur]:
            ind_meilleur = k
    return ind_meilleur

def maximum_moyenne_par_colonne(tab2D):
    ind_max = 0
    nb_colonnes = tab2D.shape[1]
    for num_col in range(1,nb_colonnes):
        moyenne_max = moyenne_une_colonne(ind_max,tab2D)
        moyenne_courante = moyenne_une_colonne(num_col,tab2D)
        if moyenne_courante > moyenne_max:
            ind_max = num_col
    return ind_max

def minimum_moyenne_par_colonne(tab2D):
    ind_min = 0
    nb_colonnes = tab2D.shape[1]
    for num_col in range(1,nb_colonnes):
        moyenne_min = moyenne_une_colonne(ind_min, tab2D)
        moyenne_courante = moyenne_une_colonne(num_col,tab2D)
        if moyenne_courante < moyenne_min:
            ind_min = num_col
    return ind_min

def decompte_une_colonne(num_col,tab2D):
    nb_lignes = tab2D.shape[0]
    nb_suffisants = 0
    for num_lig in range(nb_lignes):
        if tab2D[num_lig,num_col] >= NOTE_POUR_SUFFISANT:
            nb_suffisants+=1
    return nb_suffisants

def decompte_par_colonne(tab2D):
    nb_colonnes = tab2D.shape[1]
    decompte = {}
    for num_col in range(nb_colonnes):
        decompte[num_col] = decompte_une_colonne(num_col,tab2D)
    return decompte
    

def afficher_resultats(num_epr_moyenne_max,num_epr_moyenne_min,decompte, tab2D_notes):
    print("La classe a eu la meilleure moyenne à l'épreuve n°",num_epr_moyenne_max," (",moyenne_une_colonne(num_epr_moyenne_max,tab2D_notes),").",sep="")
    print("La classe a eu la moins bonne moyenne à l'épreuve n°",num_epr_moyenne_min," (",moyenne_une_colonne(num_epr_moyenne_min,tab2D_notes),").",sep="")
    print("Statistiques par épreuve:")
    for k in decompte:
        print("Epreuve n°",k," : ",decompte[k]," suffisants et ",len(tab2D_notes)-decompte[k]," insuffisants",sep="")
        
# Procédure main()
def main():
    tab2D_notes = np.array([[5,4,3],[6,4.5,5.5],[6,6,6],[2.5,3,2],[4,4.5,4.5]])
    num_epr_moyenne_max = maximum_moyenne_par_colonne(tab2D_notes) # ou position_maximum(moyenne_par_colonne(tab2D_notes))
    num_epr_moyenne_min = minimum_moyenne_par_colonne(tab2D_notes)
    decompte_suffisant_insuffisant_par_epr = decompte_par_colonne(tab2D_notes)
    afficher_resultats(num_epr_moyenne_max,num_epr_moyenne_min,decompte_suffisant_insuffisant_par_epr, tab2D_notes)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()