# Procédures avec paramètres

## Question 1

Soit les procédures suivantes:

![](code1.png)

1.a Qu'est-ce qui s'affiche après l'exécution de `a(5)`?

1.b Qu'est-ce qui s'affiche après l'exécution de `b(2,3)`?

1.c Qu'est-ce qui s'affiche après l'exécution de `c(2,2)`?

## Question 2

Soit les procédures suivantes:

![](code2.png)

2.a Qu'est-ce qui s'affiche après l'exécution de `c(10)`?

2.b Qu'est-ce qui s'affiche après l'exécution de `b(10, 20)`?

2.c Qu'est-ce qui s'affiche après l'exécution de `a(20)`?

*Conseil: pour comprendre ce qui se passe, exécuter le code dans votre tête en notant tous les appels de procédures avec leurs paramètres effectifs!*
