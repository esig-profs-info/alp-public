---
template: default
---

# Livre utilisé en 2019-2020

===

<!-- Utilisation du workaraound sur https://discourse.getgrav.org/t/links-to-pdf-page-media/1172
voir aussi https://github.com/getgrav/grav/issues/452 (mais cela devrait être réparé dans notre version de grav?)  

<a href="http://esig-sandbox.ch/alp/ressources/livre%20de%202019-2020/Livre%202019-2020-ConceptsProgrammationAvecTigerjython.pdf">Le livre utilisé en 2019-2020 - Attention : champ différent</a>

Ci-dessous fonctionne. Je remets la version markdown pour voir:
-->
[Le livre utilisé en 2019-2020 - Attention : champ différent](./Livre%202019-2020-ConceptsProgrammationAvecTigerjython.pdf)
