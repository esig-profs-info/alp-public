---
template: item
published: true
---

# Listes

Répondez aux questions suivantes et *notez vos réponses*.

Après avoir répondu à toutes les questions, testez les programmes. Ajustez vos réponses si nécessaire. Si les programmes produisent des erreurs, trouver une correction.

Chaque question est complètement indépendante des autres (c'est un nouveau programme à chaque fois).


## Question 1

Soit la liste suivante:

```python
couleurs = ['green', 'blue', 'red', 'yellow', 'purple']
    
```
Qu'affichent les instructions suivantes?

a)
```python
print(couleurs[0]) 
```

b) 
```python
c = couleurs[5]
print(c)
```

c) 
```python
c = couleurs[4] + couleurs[2]
print(c)
```

d) 
```python
couleurs[1] += couleurs[0]
print(couleurs)
```

e) 
```python
print(couleurs[-1])
```

f) 
```python
print(len(couleurs))
print(couleurs[len(couleurs) - 1])
```


## Question 2

Soient les listes suivantes:

```python
couleurs = ['green', 'blue', 'red', 'yellow', 'purple']
codes = [4, 0, 0, 1]
```
Qu'affichent les instructions suivantes?

a)
```python
print(couleurs[4] * codes[4])
```

b)
```python
print(couleurs[-1] * codes[-2])
```

c)
```python
a = 3 - 1
c = couleurs[codes[a]]
print(c)
```

d)
```python
c = couleurs[codes[codes[2]]]
print(c)
```




