# Série 14, Exercice 2, Version 2
# Clément Vogt

# Imports
import numpy as np

# Procédures et fonctions
def moyenne_une_ligne_v1(num_lig,tab2D):
    somme = 0
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        somme += tab2D[num_lig,num_col]
    return somme / nb_colonnes

def moyenne_par_ligne_v1(tab2D):
    liste = [] 
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        moyenne = moyenne_une_ligne_v1(num_lig,tab2D)
        liste.append(moyenne)
    return liste

def moyenne_une_ligne_v2(ligne):
    somme = 0
    for element in ligne:
        somme += element
    return somme / len(ligne)

def moyenne_par_ligne_v2(tab2D):
    liste = [] 
    for ligne in tab2D:      
        moyenne = moyenne_une_ligne_v2(ligne)
        liste.append(moyenne)
    return liste

def moyenne_une_colonne(num_col,tab2D):
    somme = 0
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        somme += tab2D[num_lig,num_col]
    return somme / nb_lignes

def moyenne_par_colonne(tab2D):
    liste = [] 
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        moyenne = moyenne_une_colonne(num_col,tab2D)
        liste.append(moyenne)
    return liste    
        
# Procédure main()
def main():
    tab2D_notes = np.array([[5,4,3],[6,4.5,5.5],[6,6,6],[2.5,3,2],[4,4.5,4.5]])  
    moyenne_par_etudiant = moyenne_par_ligne_v2(tab2D_notes)
    moyenne_par_epreuve = moyenne_par_colonne(tab2D_notes) 
    print(moyenne_par_etudiant)
    print(moyenne_par_epreuve)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()