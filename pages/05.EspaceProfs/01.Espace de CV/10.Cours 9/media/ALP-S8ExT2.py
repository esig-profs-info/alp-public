# Série 8, Exercice T2
# Clément Vogt

# Imports
from turtle import *

# Constantes
MIN_LONGUEUR_COTES = 3

# Procédures et fonctions
def initialisation():
    speed(-1)
    hideturtle()

def dessiner_triangle(longueur_cotes):
   for i in range(0,3):
        forward(longueur_cotes)
        left(360/3) 

def aller_vers_position_initiale(longueur_cotes):
    penup() # pas obligatoire
    left(60)
    forward(longueur_cotes)
    pendown() # pas obligatoire

# Procédure main()
def main():
    longueur_cotes = int(input("Entrez une longueur pour les côtés du triangle"))
    
    initialisation()

    while longueur_cotes >= MIN_LONGUEUR_COTES:
        dessiner_triangle(longueur_cotes)
        longueur_cotes /= 2
        aller_vers_position_initiale(longueur_cotes)
      
# Appel de la procédure main()
if __name__ == "__main__":
    main()