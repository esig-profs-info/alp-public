# Série 11, Exercice 2, Version 2
# Clément Vogt
# Version 2: Utilisation de l'opérateur in pour savoir si un caractère est présent dans une chaîne de caractères.

# Constantes
MINUSCULES = "abcdefghijklmnopqrstuvwxyz"
MAJUSCULES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
NUM = "0123456789"

# Procédures et fonctionsa
def compte_et_affiche_types_de_car(s):
    cpt_min = 0
    cpt_maj = 0
    cpt_chiffre = 0
    cpt_autre = 0
    for element in s:
        if element in MINUSCULES:
            cpt_min += 1
        elif element in MAJUSCULES:
            cpt_maj += 1
        elif element in NUM:
            cpt_chiffre += 1
        else:
            cpt_autre+= 1        
    afficher_resultats(cpt_min,cpt_maj,cpt_chiffre,cpt_autre)
    
def afficher_resultats(nb_min,nb_maj,nb_chiffre,nb_autre):
    pluriel_min = ""
    pluriel_maj = ""
    pluriel_chi = ""
    pluriel_aut = ""
    if nb_min > 1:
        pluriel_min = "s"
    if nb_maj > 1:
        pluriel_maj = "s"
    if nb_chiffre > 1:
        pluriel_chi = "s"
    if nb_autre > 1:
        pluriel_aut = "s"
    print("La chaîne contient ", nb_min, " minuscule",pluriel_min,", ", nb_maj, " majuscule",pluriel_maj,", ", nb_chiffre, " chiffre",pluriel_chi, " et ", nb_autre , " autre",pluriel_aut," symbole",pluriel_aut,".",sep="")

# Procédure main()
def main():
    compte_et_affiche_types_de_car("P@#yn26at^&i5ve")
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()