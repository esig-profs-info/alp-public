---
template: item
published: true
---

# Cours 9

===

* **Boucles:** révision 
    * <u>Rappel:</u> on emploie une boucle lorsqu'on s'aperçoit qu'il y a un traitement à effectuer plusieurs fois. 
    * Lorsqu'**<u>on sait</u>** combien de fois on va devoir itérer et que le pas est constant: boucle **for**.
    * Lorsqu'**<u>on ne sait pas</u>** (avant de commencer la boucle) combien de fois on va devoir boucler: boucle **while**.
    * <u>Exercice:</u> implanter les procédures du module [ALP-RevisionBoucle](./media/ALP-RevisionBoucle.py) (30') [<u>corrigé:</u> [ALP-RevisionBoucleOk](./media/ALP-RevisionBoucleOk.py)].
* **Théorème de Böhm-Jacopini** (1966) 
    * Le groupe de formants constitué de la séquence, de l'alternative et de la boucle **while** est **complet**, c'est-à-dire qu'avec ces trois formants, il est possible de décrire l'algorithme de résolution de tout problème qui peut être résolu par un ordinateur. 
* **Boucle while:** 
    * Rappel des étapes de la construction.
    * Choix de la condition de continuation de la boucle et problème des boucles infinies.
* **Correction:** Série 8.
    * ExT1: voir [ALP-S8ExT1](./media/ALP-S8ExT1.py)
    * ExT2: voir [ALP-S8ExT2](./media/ALP-S8ExT2.py)
    * Ex3: voir [ALP-S8Ex3v1](./media/ALP-S8Ex3v1.py) et [ALP-S8Ex3v2](./media/ALP-S8Ex3v2.py) - voir la stratégie de développement des procédures d'affichage!
* **Listes**
    * <u>Introduction:</u> voir [ALP-ListeIntro](./media/ALP-ListeIntro.py)
    * Collection de variables de n’importe quel type dont les composants individuels sont désignés par un numéro d'ordre - l'indice de la composante. C’est une suite ordonnée d'éléments (séquence) qui est modifiable.
    * L’indice de la première composante est 0. L’indice de la dernière composante est donc la longueur de la liste - 1. 
    * Schéma illustrant la situation (Une liste possédant 5 composantes) :
    
        ![](./media/Image1.png)
        
    * Fonction [len(lst)](https://docs.python.org/fr/3/library/functions.html#len): retourne le nombre de composants de la liste **lst** (sa longueur)
    * <u>Illustration:</u> voir [ALP-ListeIntro2](./media/ALP-ListeIntro2.py)
* **Arrondir un nombre**
    * La fonction [round()](https://docs.python.org/fr/3/library/functions.html#round) permet d’arrondir un nombre
    * Syntaxe : **round(number, ndigits)**
    * Paramètres :
        * **number** (obligatoire) : Le nombre à arrondir
        * **ndigits** (optionnel) : Le nombre de décimales à considérer lors de l'arrondi. La valeur par défaut est 0.
* [Illustration des notions](./media/illustration-c09.py)

**<u>Exercices :</u>** [Série 9](/exercices/code/serie%209)