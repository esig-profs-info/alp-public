# Série 12, Exercice 3
# Clément Vogt

# Procédures et fonctions
def donnees_entree():
    return input("Quel mot voulez-vous tester?")

# Version 1 : avec le découpage en tranche (string slicing)
# Inverse la chaîne et vérifie si la chaîne inversée est égale au mot de départ.
def est_palindrome_v1(mot):
    return mot == mot[::-1]

# Version 2 : avec le découpage en tranche (string slicing)
# Compare la première moitié du mot avec l'inverse de la deuxième moitié.
def est_palindrome_v2(mot):
    milieu = len(mot)//2
    return mot[:milieu] == mot[:-milieu-1:-1]

# Version 3 : avec fonction reversed()
# Inverse la chaîne et vérifie si la chaîne inversée est égale au mot de départ.
def est_palindrome_v3(mot):
    if list(mot) == list(reversed(mot)):
        return True
    return False

def afficher_resultats(mot,palindrome):
    print("-->",mot,end=" ")
    if palindrome:
        print("est",end=" ")
    else:
        print("n'est pas",end=" ")
    print("un palindrome")

# Procédure main()
def main():
    mot = donnees_entree()
    palindrome = est_palindrome_v2(mot)
    afficher_resultats(mot,palindrome)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()