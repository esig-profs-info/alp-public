---
template: item
published: false
---

## SRP: Exemple pratique

##### Version du document: 0.0

### Disclaimer
Ce document contient le matériel nécessaire pour envisager un exemple pratique de mise en oeuvre de la stratégie SRP proposée <a href="srp.md" target="_blank">ici</a>.
Il n'est pas nécessaire de coder l'exercice. Au contraire, les questions qu'il contient sortent du champ du cours d'ALP. Le but, ici, est simplement d'illustrer la stratégie SRP.

### Enoncé
L'énoncé est le suivant: