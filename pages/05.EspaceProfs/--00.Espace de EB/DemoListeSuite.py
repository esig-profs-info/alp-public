from __future__ import print_function #accepté par Thonny mais inutile


def compterImpairs(liste):
    cpt = 0
    for nombre in liste:
        if nombre % 2 == 1: #si le reste de la division par 2 vaut 1 nb impair
            cpt += 1
    return cpt

#en exercice : faire la somme des nombres impairs
# en exercice : compter les 11

def testerImpair(liste):#teste la présence d'un nombre imapir
    for nombre in liste:
        if nombre % 2 == 1:
            return True
    return False # SEULEMENT UNE FOIS LA BOUCLE FINIE

def afficherPremierImpair(liste):#teste la présence d'un nombre imapir
    for nombre in liste:
        if nombre % 2 == 1:
            print(nombre)
            break # return serait possible aussi pour sortir aussi de la fonction
                  # break ne sort que de la boucle

def bonAfficherPremierImpair(liste):
    position = 0
    while position < len(liste) and liste[position] % 2 != 1:
    #tant que pas fini et pas trouvé
        position += 1 #on continue à chercher
        
    if position < len(liste):#on s'est arrêté avant la fin
        print(liste[position])
    else: #on a été jusqu'au bout sans trouver
        print("que des pairs")
        
def listerImpairs(liste):
    listeImpairs = []
    for nombre in liste:
        if nombre % 2 == 1:
            listeImpairs.append(nombre)
    return listeImpairs

# prog principal
listeNombres = [50, 27, 95, 60, 11, 11, 11, 92, 17, 98, 11]


print("nombre d'impairs dans", listeNombres," =", compterImpairs(listeNombres))
print(testerImpair(listeNombres))
print(testerImpair([78, 56, 48, 12]))
afficherPremierImpair(listeNombres)
bonAfficherPremierImpair(listeNombres)
bonAfficherPremierImpair([78, 56, 48, 12])
print(listerImpairs(listeNombres))
print(listerImpairs([78, 56, 48, 12]))
