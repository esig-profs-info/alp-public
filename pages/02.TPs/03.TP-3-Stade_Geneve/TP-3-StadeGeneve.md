---
template: item
published: true
---
===

![](logo-sg.png?cropResize=200,200)
## Contexte du TP
Le club d'athlétisme du Stade Genève vous mandate pour créer quelques programmes utiles à l'administration aux entraîneurs et aux membres du club.

Les exercices reprennent le programme d'ALP, dans l'ordre, afin de reprendre les bases.

## Fichiers pour travailler 
Vous pouvez récupérer le fichier main sur GitHub:
[https://github.com/esig-ge/alp-tp-stade-geneve.git](https://github.com/esig-ge/alp-tp-stade-geneve.git)

## Tâche no1 (fonction simple)
### En-tête
Créer une fonction en_tete() qui affiche l'en-tête du stade Genève dans la sortie comme suit:

```
**********STADE GENEVE**********
********************************
```

Il suffira donc d'appeler cette fonction pour écrire  l'en-tête.

## Tâche no2 (fonction avec paramètres)
### En-tête amélioré
Les utilisateurs de votre programme on remarqué que le titre et la taille (le nombre d'étoiles) de l'en-tête peuvent changer.
Créer une fonction en_tete_mieux() capable de créer un en-tête avec titre et nombre d'étoiles paramètrable

_NB1:_ len(chaine) donne le nombre de caractère d'une String "chaine"
_NB2:_ un caractère prend la même "place" qu'une étoile (si typo à caractère de largeur fixe comme courier)

## Tâche no3 (structures conditionnelles)
### Catégories
Le programme devra être capable d'afficher la catégorie à laquelle appartient un coureur. Pour simplifier, le programme ne gère que les catégories pour les coureurs de 100m. Lorsque l'utilisateur entre ses informations, le programme lui affiche sa catégorie.

Créer ainsi une fonction _affiche_categ()_ qui demande à l'utilisateur son nom, son genre (homme ou femme), son âge et son meilleur temps sur 100m puis qui affiche:

Appeler la fonction 3 fois depuis le main

voici les données permettant de trouver la bonne catégorie:
```
FEMME 1 -- pour les femmes de 18 ans et plus qui courent en 15.0 secondes et moins
FEMME 2 -- pour les femmes de 18 ans et plus qui courent en plus de 15.0 secondes
FEMME J --  pour toutes les femmes de moins de 18 ans (Junior)
HOMME 1 -- pour les hommes de 18 ans et plus qui courent en moins de 14.5 secondes
HOMME 2 -- pour les hommes de 18 ans et plus qui cournet en 16 secondes et moins (mais en plus de 14.5)
HOMME 3 -- pour les hommes de 18 ans et plus qui courent en plus de 16 secondes
HOMME J --  pour les hommes de moins de 18 ans (Junior)
```

## Tâche no4 (boucles)
### Répertoire des temps pour les femmes de 20 ans:
Créer une fonction _rep_temps()_ qui affiche un répertoire des temps allant de 10 à 16 secondes (tous les dixièmes de secondes) accompagné de la catégorie correspondant pour une femme de 20 ans.

```
10.0 -> FEMME 1
10.1 -> FEMME 1
10.2 -> FEMME 1
...
15.0 -> FEMME 1
15.1 -> FEMME 2
15.2 -> FEMME 2
...
```

## Tâche no5 (fonctions, paramètres, return)
### Amélioration du code
Votre responsable vient de commenter votre code, elle trouve qu'il n'est pas suffisamment _réutilisable_. En effet, votre fonction _rep_temps()_ ne marche que pour une catégorie et des temps entre 10 et 16 secondes. Il faudrait pouvoir _paramétrer_ celà. Elle vous demande donc de faire en sorte que la fonction rep_temps() puisse afficher ses résultats pour n'importe quels genre, âge, temps mininmum et temps maximum.
Elle vous conseille aussi de trouver un moyen (ajouter une fonction?) pour que votre programme soit capable de trouver et retourner (non pas afficher) une catégorie sur la base des informations nécessaires.

Copier-coller les fonction _affiche_categ()_ et _rep_temps()_ si vous souhaitez les ré-utiliser. Modifier et ajouter le code nécessaire et afficher les résultats pour les données suivantes:

* femmes de 18 ans pour des temps entre 11 et 16 secondes
* femmes de 16 ans pour des tempes entre 13 et 17 secondes
* hommes de 30 ans pour des temps entre 12 et 18 secondes
* hommes de 15 ans pour des temps entre 12 et 13 secondes

### question subsidiaire (facultative mais importante):
Lorsqu'un programme demande des valeurs à un utilistaur, il doit se prémunir contre des données entrées de manière érronées. Comment pouvez-vous fair celà?

## Tâche no6 (utilisation des listes)
### Affichage des athlètes
En utilisant la variable _names_ proposée dans le main, afficher les noms de toute l'équipe comme suit:
```
Bob
Jane
Jack
Chloe
etc.
```

## Tâche no7 (parcours de plusieurs listes)
### Affichage des athlètes plus complet
En utilisant les variables proposées dans le main, afficher les noms de toute l'équipe comme suit:
```
Bob - homme - 20 ans
Jane - femme - 22 ans
Jack - garçon - 23 ans
Chloe - femme - 20 ans
etc.
```
**NB:** Vous êtes invité-e à créer des fonctions annexes pour cette tâche

### Question subsidiaire: 
Savez-vous utiliser les 3 façons de parcourir une liste (for each, for in range et while?) et savez-vous dans quel cas les utiliser?


## Tâche no8 (filtrage de liste)
### Créer une sous-liste puis l'afficher
Afficher les noms des athlètes de moins de 30 ans (les enregistrer dans une nouvelle liste puis les afficher)

**NB:** Vous êtes invité-e à créer des fonctions annexes pour cette tâche

## Tâche no9 (filtrage de liste)
### Créer une sous-liste puis l'afficher
Afficher les noms des athlètes qualifiés lors de la manche 1 (qualification: chrono min homme = 13.5, chrono min femme = 14.0) (les enregistrer dans une nouvelle liste puis les afficher)

**NB:** Vous êtes invité-e à créer des fonctions annexes pour cette tâche

## Tâche no10 ("soustraire" deux listes)
### Créer une sous-liste puis l'afficher
Afficher les noms des athlètes disqualifiés après la manche 1 (en utilisant la liste des qualifiés)

## Tâche no11 (Booléen, return et paramètres)
### fonction utile
Créer une fonction qui retourne True si un-e athlète a effectué un chrono en dessous d'un temps passé en paramètre

## Tâche no12 (algo moyenne)
### Moyennes
Afficher les moyennes d'age de l'équipe (pour les hommes, pour les femmes et en général)

## Tâche no13 (algo)
### fonction utile
Afficher, pour une manche, le vainqueur de la course (pour les hommes, pour les femmes et en général)

## Tâche no14 (création de liste, return, algo)
### liste des rapides
Afficher, pour une manche, la liste des athlètes qui ont couru plus vite que la moyenne (hommes et femmes confondus)

## Tâche no15 (algo)
Afficher, pour chaque athlète, son meilleur chrono (entre les 3 manches)

## Tâche no16 (hors champs mais intéressant et faisable)
Affichez de manière harmonieuse tous les résultats sur la base de la variable _resultats_ (qui est une liste contenant des listes) dans le main.