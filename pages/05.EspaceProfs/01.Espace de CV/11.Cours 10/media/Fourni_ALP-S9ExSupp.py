# Série 9, Exercice supplémentaire
# Nom et prénom: ?

# Procédures et fonctions
# Procédure permettant d'afficher les valeurs contenues dans la liste reçue en paramètre. (Les éléments doivent être affichés les uns à côté des autres avec un espace entre chacun)
def afficher_liste_int(''' A COMPLETER '''):
    ''' A COMPLETER '''
    
# Fonction à résulat entier permettant de calculer et retourner la somme des valeurs int contenues dans la liste reçue en paramètre. 
def somme_liste_int(''' A COMPLETER '''):
    ''' A COMPLETER '''
    
# Fonction à résultat booléean retournant True si et seulement si la liste reçue en paramètre contient au moins une valeur négative, False sinon.
def existe_valeur_negative(''' A COMPLETER '''):
    ''' A COMPLETER '''

# Procédure main()
# Test des procédures à développer. Ne doit (en principe) pas être modifiée.
def main():
    lst1 = [1, 2, 3, 4, 5, 6, 7]
    lst2 = [1, 2, 7, -1, -2, -7, 0]
    
    print("Liste lst1: ",end='')
    afficher_liste_int(lst1)
    print()
    print("Liste lst2: ",end='')
    afficher_liste_int(lst2)
    print()
    print("Somme des valeurs de lst1: ", somme_liste_int(lst1),sep='')
    print("Somme des valeurs de lst2: ", somme_liste_int(lst2),sep='')
    if existe_valeur_negative(lst1):
        print("Il y a au moins une valeur négative dans lst1")
    if existe_valeur_negative(lst2):
        print("Il y a au moins une valeur négative dans lst2")

# Appel de la procédure main()
if __name__ == "__main__":
    main()
