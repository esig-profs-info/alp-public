---
template: item
published: true
---

# Feuille de route pour les semaines à venir

===

* Série 5 : sur la boucle for : faite
* Série 6 : sur les tests de validité : on y reviendra  
* Série 7 : sur boucle for avancée : exercices Tortue facultatifs, autres exercices faits (normalement)
* Série 8 : à ignorer (on reviendra sur un point, les "pyramides" de l'exercice 3)
* Série 9 : sur les listes : à faire (sauf T1, facultatif)
* Série 10 : sur les listes : à faire
* Retour sur le while
* Retour sur les boucles imbriquiés