from __future__ import print_function

def formaterReel(nombreReel):
    return '{:.2f}'.format(nombreReel)

def calculePoidsApparent(poidsPlongeuse, poidsCombinaison, volume, nbPlombs500g, nbPlombs1kg):
    return round((poidsPlongeuse + poidsCombinaison/1000 + nbPlombs500g*0.5 + nbPlombs1kg) - volume, 3)

def afficheSiCoule(poidsApparent):
    if poidsApparent < 0:
        print('Il faut rajouter des poids car la plongeuse flottera et ne pourra pas plonger')
    elif poidsApparent > 0:
        print('Parfait! La plongeuse va pouvoir profiter de sa plongée')
    else: # == 0
        print('Il faut rajouter des poids car la plongeuse sera en équilibre et ne pourra pas plonger')

def couleTool():
    print('''
----------------------------------------------------
Association Lémanique de Plongée
Bienvenue au CouleTool !
----------------------------------------------------
''')
    poids = float(input('Poids de la plongeuse [kg]: '))
    poidsCombinais = float(input('Poids de sa combinaison [g] : '))
    volume = float(input('Volume de la plongeuse [l]: '))
    nbPoids500 = int(input('Nombre de poids 500g : '))
    nbPoids1kg = int(input('Nombre de poids 1kg : '))
    
    pa = calculePoidsApparent(poids, poidsCombinais, volume, nbPoids500, nbPoids1kg)
    print('Poids apparent',formaterReel(pa),'kg')
    afficheSiCoule(pa)

def poidsApparentGroupe(nomsPlongeuses, poidsPlongeuses, poidsCombinaisons, volumesTotaux, nbPoids500g, nbPoids1kg):
    resultat = []
    for i in range(len(nomsPlongeuses)):
        resultat.append(calculePoidsApparent(poidsPlongeuses[i], poidsCombinaisons[i], volumesTotaux[i], nbPoids500g[i], nbPoids1kg[i]))      
    return resultat


def groupeCoule(poidsApparents):
    # Solution pythonesque: return all(pa > 0 for pa in poidsApparents)
    # Solution manuelle
    for pa in poidsApparents:
        if pa <= 0:
            return False
    return True

def aEquilibre(poidsApparents):
    return 0 in poidsApparents # retourne True si la valeur 0 est dans la liste poidsApparents

def resumeGroupe(noms, poidsApparents):
    for i in range(len(noms)): # pythonesque: for n, p in zip(noms, poidsApparents):
        pa = poidsApparents[i]
        print(noms[i], '\ta comme poids apparent', formaterReel(pa), ': ok' if pa > 0 else ': ATTENTION!')

def groupeCouleTool(noms, poids, poidsCombinaisons, volumes, nbP500, nbP1):
    poidsApparents = poidsApparentGroupe(noms, poids, poidsCombinaisons, volumes, nbP500, nbP1)
    resumeGroupe(noms, poidsApparents)

    if groupeCoule(poidsApparents):
        print("C'est go pour la plongée!")
    else:
        print("Ay! Faut vérifier les poids!")
    
    if aEquilibre(poidsApparents):
        print("Il y a une plongeuse en parfait équilibre!")    

def exercice1():
    couleTool()

def exercice2(jeuDonnees):# en param: indique avec quel jeu de données l'on veut tester.    
    if jeuDonnees == 1: 
        # Données de test 1        
        noms = ['Anne', 'Carole', 'Mike']
        poids = [72.5, 72.5, 72.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [1, 1, 1]
        nbPoids1kg = [0, 1, 2]
    elif jeuDonnees == 2:
        # Données de test 2
        noms = ['Magda', 'John', 'Céline']
        poids = [72.5, 72.5, 73.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [2, 0, 2]
        nbPoids1kg = [3, 1, 1]

    elif jeuDonnees == 3: 
        # Données de test 3        
        noms = ['Anne', 'Carole', 'Mike']
        poids = [72.5, 72.5, 72.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [1, 1, 2]
        nbPoids1kg = [3, 1, 2]
    else:
        print('Jeu de données inexistant: ', jeuDonnees)
    
    print('#### Jeu de données no.', jeuDonnees, '####')
    groupeCouleTool(noms,poids,poidsCombi,volume,nbPoids500g,nbPoids1kg)

if __name__ == '__main__':        
    exercice = 2 # Indique quel exercice vous souhaitez tester.
    jeuxDonnees = 1 # Indique le jeu de données à utiliser (pour l'exercice 2)
    if 1 == exercice:
        exercice1()
    elif 2 == exercice:
        exercice2(jeuxDonnees)
    else:
        print('Exercice inexistant:', exercice)
    


 
