```python
# ex. 1
# solution minimaliste
for i in range(0,11,2):
    print(i)

# solution avec test si pair
for i in range(11):
    if i % 2 == 0:
        print(i)
        
# ex. 2
print("ex. 2")

s = input("n: ")
n = int(s)

for i in range(0,n+1,2):
    print(i)

for i in range(n+1):
    if i % 2 == 0:
        print(i)

        
# ex. 3
print('ex. 3')
m = int(input("borne inférieure: "))
n = int(input("borne supérieure: "))

if m > n:
    print("Erreur: m doit être inférieur ou égal à n")
    exit

print('version 1:')
for i in range(m,n+1):
    if i % 2 == 0:
        print(i)
        
print('version 2:')
if m % 2 == 1:
    m = m + 1  
for i in range(m,n+1,2):
    print(i)

print('ex. 4')
m = int(input("borne inférieure: "))
n = int(input("borne supérieure: "))

print('version 1:')
pas = 1
if m > n:
    pas = -1    
for i in range(m,n+pas,pas):
    if i % 2 == 0:
        print(i)
 ```       




        
        