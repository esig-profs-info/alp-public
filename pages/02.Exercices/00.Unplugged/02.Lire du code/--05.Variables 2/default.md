---
template: item
published: false
---

# Variables 2

Répondez aux questions suivantes et *notez vos réponses par écrit*.

Après avoir répondu à toutes les questions, testez les programmes. Ajustez vos réponses si nécessaire. Si les programmes produisent des erreurs, trouvez une correction.

Chaque question est complètement indépendante des autres (c'est un nouveau programme à chaque fois).

## Question 1

Soit le programme suivant:

```python
def hi(n):
    print n

n = "Théodore"
hi()
```
Qu'affichera ce programme? Pourquoi?

## Question 2
Soit le programme suivant:
```python
def hi(n):
    n = 2
    print n

hi("Théodore")
```
Qu'affichera ce programme? Pourquoi?

## Question 3
Soit le programme suivant:
```python
def a(a):
    print a*a
a(3)

```
Qu'affichera ce programme? Pourquoi?

## Question 4
Soit le programme suivant:
```python
def a(a):
    b(a*a)

def b(a):
    print a+b

a(2)
```
Qu'affichera ce programme? Pourquoi?

## Question 4
Soit le programme suivant:
```python
def a(a):
    b(a*a)

def b(a):
    print a-1

a(2)
```
Qu'affichera ce programme? Pourquoi?

## Question 5
Soit le programme suivant:
```python
a = 3
def b(a):
    a += 2
    print a
b(a)
print a
```
Qu'affichera ce programme? Pourquoi?

## Question 6
Soit le programme suivant:
```python
def a(a):
    a += 2
    print a
a = 3
a(a)
print a
```
Qu'affichera ce programme? Pourquoi?

## Question 7
Soit le programme suivant:
```python
def b(c):
    c += 2

def a(c):
    b(c)
    print c
    
a(5)
```
Qu'affichera ce programme? Pourquoi?



