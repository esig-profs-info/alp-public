# Série 14, Exercice 4
# Clément Vogt

# Imports
import numpy as np

# Constantes
NOTE_POUR_SUFFISANT = 4

# Procédures et fonctions
def moyenne_une_colonne(num_col,tab2D):
    somme = 0
    nb_lignes = tab2D.shape[0]
    for num_lig in range(nb_lignes):
        somme += tab2D[num_lig,num_col]
    return somme / nb_lignes

def maximum_moyenne_par_colonne(tab2D):
    moyenne_max = moyenne_une_colonne(0,tab2D)
    ind_max = 0
    nb_colonnes = tab2D.shape[1]
    for num_col in range(1,nb_colonnes):
        moyenne_courante = moyenne_une_colonne(num_col,tab2D)
        if moyenne_courante > moyenne_max:
            moyenne_max = moyenne_courante
            ind_max = num_col
    return ind_max,moyenne_max

def minimum_moyenne_par_colonne(tab2D):
    ind_min = 0
    moyenne_min = moyenne_une_colonne(ind_min,tab2D)
    nb_colonnes = tab2D.shape[1]
    for num_col in range(1,nb_colonnes):
        moyenne_courante = moyenne_une_colonne(num_col,tab2D)
        if moyenne_courante < moyenne_min:
            moyenne_min = moyenne_courante
            ind_min = num_col
    return ind_min, moyenne_min

def decompte_une_colonne(num_col,tab2D):
    nb_lignes = tab2D.shape[0]
    nb_suffisants = 0
    for num_lig in range(nb_lignes):
        if tab2D[num_lig,num_col] >= NOTE_POUR_SUFFISANT:
            nb_suffisants+=1
    return nb_suffisants, nb_lignes-nb_suffisants      

def decompte_par_colonne(tab2D):
    nb_colonnes = tab2D.shape[1]
    decompte = {}
    for num_col in range(nb_colonnes):
        decompte[num_col] = decompte_une_colonne(num_col,tab2D)
    return decompte

def afficher_resultats(epr_moyenne_max,epr_moyenne_min,decompte):
    print("La classe a eu la meilleure moyenne à l'épreuve n°",epr_moyenne_max[0]," (",epr_moyenne_max[1],").",sep="")
    print("La classe a eu la moins bonne moyenne à l'épreuve n°",epr_moyenne_min[0]," (",epr_moyenne_min[1],").",sep="")
    print("Statistiques par épreuve:")
    for k in decompte:
        print("Epreuve n°",k," : ",decompte[k][0]," suffisants et ",decompte[k][1]," insuffisants",sep="")
        
# Procédure main()
def main():
    tab2D_notes = np.array([[5,4,3],[6,4.5,5.5],[6,6,6],[2.5,3,2],[4,4.5,4.5]])   
    epr_moyenne_max = maximum_moyenne_par_colonne(tab2D_notes)
    epr_moyenne_min = minimum_moyenne_par_colonne(tab2D_notes)
    decompte_suffisant_insuffisant_par_epr = decompte_par_colonne(tab2D_notes)
    afficher_resultats(epr_moyenne_max,epr_moyenne_min,decompte_suffisant_insuffisant_par_epr)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()