---
template: item
published: true
---

# Série 6
## Objectifs
Approfondir le concept d'**alternative**.

**Décomposer** la solution d'un problème en **tâches**.

Mettre en pratique les concepts de **procédures**, de **fonction** et de **paramètre**.

**Valider** des données d'entrée

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S6Ex1** à **ALP-S6Ex2** et se trouveront dans un dossier nommé **ALP-S6**.

## Énoncé

Ajouter la validation des données pour les exercices 1 et 2 de la série 4. En effet,  assurez-vous que les données fournies par l'utilisateur soient cohérentes. Dans le cas contraire, affichez un message d'erreur pertinent.

### Exercice 1
La type de piano ne peut pas être différent de 1 ou 2. Le nombre de km doit être forcément positif et ne peut pas dépasser 300. Le nombre d'étages (montés et descendus) ne peut pas être négatif. Le nombre d'étages montés ne peut pas être supérieur au triple du nombre d'étages descendus.

Le programme doit afficher un seul message d'erreur général dès qu'il y a au moins une erreur.

### Exercice 2
La type de places ne peut pas être différent de 1 ou 2. Le nombre de places doit être forcément positif. Le nombre de places de peut pas excéder 70.

Le programme doit afficher un message pour chaque erreur rencontrée.