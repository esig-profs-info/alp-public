# Série 1, Exercice 6
# Clément Vogt
# Version 2: L'exercice est réalisé en appliquant la démarche de résolution de problèmes

# Imports
# Il n'y pas besoin de réaliser d'imports dans cet exercice.

# Constantes
ANNEE_COURANTE = 2020
NOMBRE_ANNEES_POUR_PRIME = 5
TAUX_PRIME = 0.03

# Procédures et fonctions
def donnees_entree():
    salaire_base = float(input("Entrez un salaire de base"))
    annee_engagement = int(input("Entrez une année d'engagement"))
    return salaire_base, annee_engagement

def montant_de_la_prime(salaire_base,nombre_annees):
    if nombre_annees < NOMBRE_ANNEES_POUR_PRIME:
        prime = 0
    else:
        prime = TAUX_PRIME * salaire_base
        
    return prime

def afficher_resultats(salaire_base,nombre_annees,prime,salaire_brut):
    print("Salaire de base:",salaire_base)
    print("Nombre d'années:",nombre_annees,"ans")
    print("Prime:",prime)
    print("Salaire brut:",salaire_brut)
    
# Procédure main()
def main():
    # Tâche 1: Récupérer les données d'entrée (informations que doit fournir l’utilisateur)
    salaire_base, annee_engagement = donnees_entree()
    
    # Tâche 2: Calculer le nombre d'années d'engagement
    nombre_annees = ANNEE_COURANTE - annee_engagement
    
    # Tâche 3: Calculer le montant de la prime
    prime = montant_de_la_prime(salaire_base,nombre_annees)
    
    # Tâche 4: Calculer le salaire brut
    salaire_brut = salaire_base + prime
    
    # Tâche 5: Afficher les résultats
    afficher_resultats(salaire_base,nombre_annees,prime,salaire_brut)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()