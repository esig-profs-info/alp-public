---
template: item
published: true
---

# Pour accéder au tutorial sur les tableaux NumPy

###### *au besoin - même chose que pour le slicing*
* Fichier .pdf  : [Instructions à suivre pour l'installation](../00.Slicing/media/Comment%20exécuter%20un%20notebook%20Jupyter.pdf) 
* Fichier à décompacter selon les instructions : [EduPython version 3.0, lien sur le Drive eduge.ch](https://drive.google.com/file/d/11v2ExZy0WKVz772SxjSYR0JoERj0KI1i/view?usp=sharing) <br> ou
    [site de téléchargement au besoin](https://edupython.tuxfamily.org/)
<br>
###### *la nouveauté*
* Fichier notebook lui-même : [notebook sur les *tableaux NumPy*](./media/NumPy_premiers_pas.ipynb)