---
template: item
published : true
---

Activité numéro 6 avec des gobelets...et si nous "rangions" des gobelets?

===

##Exercices - Gobelets
###Activité n°6 - Rangement
#### Contexte:
* Activité individuelle
* Le bras est en position "zéro" au départ

Soit la disposition de gobelets suivante:

![position initiale des gobelets](gobelets6-positionInitiale.jpg?cropResize=350,350)


##### Exercice 1
Déterminez l'algorithme le plus optimisé possible pour obtenir le rangement des gobelets suivant (l'ordre des gobelets n'est pas important):<br/>
![rangement de l'exercice 1](gobelets6-exercice1.jpg?cropResize=350,350)

##### Exercice 2
Déterminez l'algorithme le plus optimisé possible pour obtenir le rangement des gobelets suivant:<br/>
![rangement de l'exercice 1](gobelets6-exercice2.jpg?cropResize=350,350)

##### Exercice 3
Déterminez l'algorithme le plus optimisé possible pour obtenir le rangement des gobelets suivant:<br/>
![rangement de l'exercice 1](gobelets6-exercice3.jpg?cropResize=350,350)