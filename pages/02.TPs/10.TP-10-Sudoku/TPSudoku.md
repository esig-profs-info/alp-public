---
template: item
published: true
---


TP Sudoku

===

![logo sudoku](sudoku.png?cropResize=200,200)

## Partie A

Une grille de Sudoku se représente naturellement dans un programme sous la forme d'un tableau à deux dimensions de 9 lignes et de 9 colonnes.

Votre programme doit définir la (ou les) procédure(s) ou fonction(s) demandée(s) dans chaque exercice ainsi que la _procédure de test_ correspondante. Reportez-vous à [ExempleOrganisation.py](./ExempleOrganisation.py)

Plus précisément chaque exercice de cet énoncé donnera lieu à une procédure, dite procédure de test dans le module **TPSudoku** (fichier **TPSudoku.py** ) **: exerciceA1, exerciceA2, ...**

Les procédures de test ne prennent pas de paramètre et ce sont les seules que votre programme peut appeler directement dans la procédure main().

Ces procédures de test vont, quant à elles, appeler les procédures et fonctions que vous définirez pour les tester (justement) avec les données fournies, ou éventuellement demandées à l'utilisateur. Elles afficheront aussi les résultats. Cf. le fichier [SortiesProduitesSudokuA.txt](SortiesProduitesSudokuA.txt).

Les données fournies sont des grilles obtenues par lecture de fichiers texte qui devront accompagner votre programme et ne _pas_ être modifiés. Si vous voulez tester d'autres grilles, introduisez vos propres fichiers. (Les grilles fournies sont reprises à la fin du document.)



### Exercice A1 - validation des données

Ecrire une fonction qui renvoie le nombre d'éléments d'une grille qui ne sont pas compris entre 1 et 9.

**Remarque**  : Il est recommandé de définir une fonction qui teste l'appartenance d'un nombre à l'intervalle des valeurs 1..9. Et il est recommandé que cette fonction reçoive aussi en paramètre les valeurs limites de l'intervalle. Cela permettra de la réutiliser.



### Exercice A2 – validation étendue et affichage

Modifier la fonction de l'exercice précédent afin d'autoriser l'utilisation du nombre 0 pour représenter une case vide (intervalle des valeurs entre 0 et 9) des grilles partiellement remplies.

Si la grille est valide, on pourra l'afficher. Dans ce cas, on affichera une suite point-espace-espace à la place du 0.

**Remarque**  : Il est **fortement recommandé** de définir une procédure d'affichage à part. Cela permettra de la réutiliser.



### Exercice A3 - transposition

Ecrire une fonction qui crée une grille vide et qui range dedans les données d'une autre grille en recopiant les lignes de cette grille dans les colonnes de la grille vide, ce qu'on appelle la transposée. Puis afficher les deux grilles, si possible côte à côte (pour cela il faudra une procédure d'affichage spécifique).

**Remarque**  : Pour créer une grille vide, vous pouvez utiliser la méthode np.empty((nb_lignes,nb_colonnes),dtype) qui créera un tableau comportant nb_lignes, nb_colonnes. Le paramètre dtype spécifie quant à lui le type des données qui sont contenues dans le tableau, dans notre cas c'est des **int**.



### Exercice A4 - gestion des cases vides

Ecrire une fonction qui renvoie le nombre de cases vides d'une grille.

A l'aide de cette fonction, en écrire une autre qui indique si une grille est complètement remplie ou pas en renvoyant un booléen.



### Exercice A5 – nombre d'occurrences (de préférence avec un dictionnaire)

Ecrire une fonction qui renvoie un dictionnaire contenant le nombre d'occurrences de chaque nombre de 0 à 9 dans une grille.



## Données de test

_Grille remplie de l'énoncé ([Grille1.txt](./Grille1.txt))_

        8 4 7 9 6 3 5 2 1
        1 2 3 8 4 5 6 7 9
        5 9 6 1 2 7 4 8 3
        9 7 8 3 5 4 1 6 2
        4 5 1 2 9 6 7 3 8
        6 3 2 7 1 8 9 5 4
        7 1 9 6 3 2 8 4 5
        3 8 4 5 7 9 2 1 6
        2 6 5 4 8 1 3 9 7

_Grille ayant des nombres non valides ([Grille2a.txt](./Grille2a.txt))_

        8 4 7 9 6 3 5 2 1
        1 2 3 8 4 5 6 7 9
        5 9 6 1 2 10 4 8 3
        9 7 8 3 5 4 1 6 2
        4 5 1 2 9 6 7 3 8
        6 3 2 7 1 8 9 5 4
        7 1 9 6 3 2 8 4 5
        3 8 4 5 7 9 2 1 6
        2 6 5 4 8 1 3 9 7

_Grille ayant des nombres non valides ([Grille2b.txt](./Grille2b.txt))_

        8 4 7 9 6 3 5 2 1
        1 2 3 8 4 5 6 7 9
        5 9 6 1 2 7 4 8 3
        9 7 -8 3 5 4 1 6 2
        4 5 1 2 9 6 7 3 8
        6 3 2 7 1 8 9 5 4
        7 1 9 6 3 2 8 4 5
        3 8 4 5 7 9 2 1 6
        2 6 5 4 8 1 3 9 7

_Grilles à remplir de l'énoncé ([Grille3a.txt](./Grille3a.txt))_

        0 0 7 9 0 0 0 0 1
        0 2 3 8 0 0 6 7 0
        0 0 6 0 2 7 0 0 0
        0 7 8 0 5 0 0 0 0
        0 5 0 2 0 6 0 3 0
        0 0 0 0 1 0 9 5 0
        0 0 0 6 3 0 8 0 0
        0 8 4 0 0 9 2 1 0
        2 0 0 0 0 1 3 0 0

_Grille à remplir de l'énoncé ([Grille3b.txt](./Grille3b.txt))_

        8 4 7 9 6 3 5 0 1
        1 2 3 8 4 5 6 7 9
        5 9 6 1 2 7 4 8 3
        9 7 8 3 5 4 1 6 2
        4 5 1 2 9 6 7 3 8
        6 3 2 7 1 8 9 5 4
        7 1 9 6 3 2 8 4 5
        3 8 4 5 7 9 2 1 6
        2 6 5 4 8 1 3 9 7

_Grille à remplir de l'énoncé, donnée par colonne ([GrilleSup.txt](./GrilleSup.txt))_

        0 0 0 0 0 0 0 0 2
        0 2 0 7 5 0 0 8 0
        7 3 6 8 0 0 0 4 0
        9 8 0 0 2 0 6 0 0
        0 0 2 5 0 1 3 0 0
        0 0 7 0 6 0 0 9 1
        0 6 0 0 0 9 8 2 3
        0 7 0 0 3 5 0 1 0
        1 0 0 0 0 0 0 0 0