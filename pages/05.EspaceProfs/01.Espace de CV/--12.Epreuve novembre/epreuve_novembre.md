---
template: item
published: true
---

# Épreuve de novembre

===

* **Rappels:**
    * L’épreuve a lieu le lundi 30.10.2023 à 08h30.
    * La durée du test est de ~240 minutes.
    * Tous les documents personnels (y compris électroniques) sont autorisés
    * Tout accès au réseau est formellement interdit.
    * Le contrôle est obligatoirement réalisé sur le matériel mis à disposition par l'école (les ordinateurs personnels sont interdits).
* **Révision**
    * Alternatives: Tests, test imbriqués et formants de luxe (elif)
    * Procédures et fonctions
    * Boucles
    * Listes
    * Validation en cascade et en séquence
    * ... c'est à dire, cours 1 à 10 avec les séries d'exercices correspondantes.