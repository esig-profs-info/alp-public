# Exercice de révision sur les boucles
# Clément Vogt


# Procédures et fonctions
''' Afficher les 15 premiers multiples de 6 :  6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90 '''

# On applique exactement les spécifications de l'énoncé: on compte jusqu'à 15 et on effectue
# la multiplication à chaque tour de la boucle. 
def afficher_mult_6a():
    for cpt in range (1,15+1):
        print(cpt * 6,end=' ')
    print()

# Comme précédemment, on compte jusqu'à 15. Les multiples sont obtenus en observant que la
# liste des multiples successifs peut être obtenue par additions successives de la valeur 6.
# Cette methode est plus performante que afficher_mult_6a() car on effectue des additions au lieu de
# multiplications. Le nombre d'opérations effectuées est le même, mais la multiplication coûte plus
# cher en temps processeur.
def afficher_mult_6a2():
    multiple = 6
    for cpt in range (1,15+1):
        print(multiple,end=' ')
        multiple += 6 # Identique à multiple = multiple + 6
    print()
    
# Ici, on fait directement prendre les valeurs successives de la liste des multiples à la variable de
# contrôle de la boucle. 
# Cette méthode a exactement la même performance que afficher_mult_6a2, mais on économise
# une variable.
def afficher_mult_6a3():
    for multiple in range (6,15*6+1,6):
        print(multiple,end=' ')
    print()    


''' Afficher les multiples de 6, compris entre 6 et 50 :  6, 12, 18, 24, 30, 36, 42, 48 '''

# On passe en revue les nombres compris entre 6 et 50 et on affiche ceux qui sont des
# multiples de 6.     
def afficher_mult_6b():
    for nb in range(6,50+1):
        if nb % 6 == 0:
            print(nb,end=' ')
    print()

# Les multiples peuvent être obtenus directement par additions successives de la valeur 6.
# Plus performant que afficher_mult_6b(), car on fait moins de tours de la boucle et surtout on ne
# fait pas 45 opérations de division. 
def afficher_mult_6b2():
    for multiple in range (6,50+1,6):
        print(multiple,end=' ')
    print()   


''' Afficher les multiples de 6, tant que ce n'est pas un multiple de 14 :  6, 12, 18, 24, 30, 36 '''

# Ici, on ne sait pas le nombre de fois qu'on va devoir itérer: donc while.
def afficher_mult_6c():
    multiple = 6
    while multiple % 14 != 0:
        print(multiple,end=' ')
        multiple += 6
    print()
    
# Procédure main()    
def main():
    afficher_mult_6a()
    afficher_mult_6a2()
    afficher_mult_6a3()
    afficher_mult_6b()
    afficher_mult_6b2()
    afficher_mult_6c()
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()