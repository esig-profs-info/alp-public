# Série 8, Exercice 3, Version 1
# Clément Vogt

# Procédures et fonctions
def donnees_valides(type_triangle,nb_lignes):
    if type_triangle >= 1 and type_triangle <= 4 and nb_lignes > 0:
        return True
    print("Vos données sont invalides.")
    return False

def afficher_une_ligne_etoiles(nb_etoiles):
    for cpt in range(1,nb_etoiles+1):
        print("*",end='')
    print()

def afficher_espaces(nb_espaces):
    for cpt in range(1,nb_espaces+1):
        print(" ",end='')
        
def triangle1(nb):
    for num_lig in range(1,nb+1):
        afficher_une_ligne_etoiles(num_lig)
    
def triangle2(nb):
    for num_lig in range(1,nb+1):
        afficher_une_ligne_etoiles(nb - num_lig + 1)
    
def triangle3(nb):    
    for num_lig in range(1,nb+1):
        afficher_espaces(nb - num_lig)
        afficher_une_ligne_etoiles(num_lig)
    
def triangle4(nb):    
    for num_lig in range(1,nb+1):
        afficher_espaces(num_lig - 1)
        afficher_une_ligne_etoiles(nb - num_lig + 1)

# Procédure main()
def main():
    type_triangle = int(input("Entrez un type de triangle"))
    nb = int(input("Entrez un nombre de lignes"))
    if donnees_valides(type_triangle,nb):
        if type_triangle == 1:
            triangle1(nb)
        elif type_triangle == 2:
            triangle2(nb)
        elif type_triangle == 3:
            triangle3(nb)
        else:
            triangle4(nb)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()