---
template: item
published: true
---

# Série 11
## Objectifs
Travailler avec des **chaînes de caractères**

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

[Les chaînes de caractères](/ressources/chaines%20de%20caracteres)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S11Ex1** à **ALP-S11Ex5** et se trouveront dans un dossier nommé **ALP-S11**.

## Énoncé
## Exercice 1
Ecrire un programme permettant d'afficher le nombre d'*occurences* (= apparitions) d'un caractère dans une chaîne. Votre programme doit contenir au minimum une fonction. <br/>
Par exemple, le caractère `a` apparaît `5` fois dans la chaîne `"abracadabra"`.
Faire saisir par l'utilisateur la chaîne et le caractère à compter puis afficher le résultat comme suit:

```
la lettre 'a' se trouve 5 fois dans 'abracadabra'
```

## Exercice 2
Ecrire une fonction `compte_et_affiche_types_de_car(s)` qui compte et affiche le nombre de : minuscules, majuscules, chiffres et autres symboles d'une chaîne `s` passée en paramètre.<br/>

Par exemple, cet appel :
```
compte_et_affiche_types_de_car("P@#yn26at^&i5ve")
```
produit l'affichage suivant (notez la gestion des pluriels et singuliers) :
```
La chaîne contient 7 minuscules, 1 majuscule, 3 chiffres et 4 autres symboles.
```

**Informations:**<br/>
Vous pouvez déclarer les constantes suivantes:
```
MINUSCULES = "abcdefghijklmnopqrstuvwxyz"
MAJUSCULES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
NUM = "0123456789"
```
et voir si le caractère que vous testez s'y trouve (sinon c'est un autre symbole).

Une autre approche est possible avec les fonctions `islower()` ou `isupper()` qui s'utilisent comme ceci ```c.islower()```... A vous de choisir !

## Exercice 3
Ecrire une fonction `ajoute_au_milieu(s1,s2)` qui retourne une chaîne de caractères. La chaine de caractère retournée doit contenir la chaîne `s2` au milieu de la chaîne `s1`. Si la longueur de `s1` est impaire, garder la plus grande partie au début du mot.<br/>

Par exemple:
```
ajoute_au_milieu("python", "COUCOU") → "pytCOUCOUhon"
ajoute_au_milieu("sandbox", "COUCOU") → "sandCOUCOUbox"
```

## Exercice 4
Ecrire un programme qui demande un mot à l'utilisateur, puis qui lui indique si ce mot est un palindrome (un mot que se lit dans les deux sens). Votre programme doit contenir au minimum une fonction à résultat booléeen. Votre solution ne doit, si possible, pas inverser la chaîne.

```
Quel mot voulez-vous tester? sugus
--> sugus est un palindrome

Quel mot voulez-vous tester? python
--> python n'est pas un palindrome
```
Trouver une autre manière de résoudre le même problème (par exemple en inversant la chaîne cette fois). A écrire dans une (ou plusieurs) autre fonction.

## Exercice 5
Ecrire une procédure permettant d'afficher "en escalier", descendant (paramètre à 0) ou montant (paramètre à 1), une chaîne de caractères.

Par exemple: la chaîne `"abracadabra"`, en `descendant`, donne:
```
a
 b
  r
   a
    c
     a
      d
       a
        b
         r
          a
```
Et en `montant`...
```
          a
         b
        r
       a
      c
     a
    d
   a
  b
 r
a
```


<!--
## Exercice 3_old
Ecrire une fonction `troisCarDuMilieu(s)` qui retourne une chaîne composée des trois caractères du milieu d'une chaîne `s` passée en paramètre. La chaîne passée en paramètre doit être impaire et composée d'au moins 7 caractères, sinon ce n'est pas le cas, la fonction retourne une chaîne vide `""`.<br/>

**info:** le slicing peut être utile (mais pas indispensable)<br/>

Par exemple:
```
troisCarDuMilieu("JhonDipPeta") → "Dip"
troisCarDuMilieu("Jasonay") → "son"
```

## Exercice 5_old
Ecrire une fonction `rangeMajDabord(s)` qui retourne une chaîne de caractère qui aura déplacé les caractères en majuscule de la chaine `s` passée en paramètre au début (de la chaine retournée).<br/>

**info:** Pour savoir si un caractère `char` est une minuscule on peut utiliser la fonction `char.isLower()`. Par ailleurs, la méthode conseillée pour cet exercice consiste à créer une chaîne `maj` qui contient les majuscules dans l'ordre, puis une chaine `min` qui contient les minuscules puis de les concaténer l'une à l'autre.<br/>

Par exemple:
```
rangeMajDabord("PyThOn") → "PTOyhn"
```

## Exercice 6_old
Ecrire une fonction permettant d'afficher la chaîne de caractères résultant de l'insertion d'un caractère à une position donnée dans chaîne. (vérifiez que la position est comprise entre 1 et la taille de la chaîne)

Par exemple, l'insertion du caractère `'x'` à la cinquième position de la chaîne `"abracadabra"` donnera la chaîne `"abraxcadabra"`.

Faites entrer la chaîne, le `caractère à insérer` ainsi que la `position` par l'utilisateur puis affichez le résultat comme suit:

```
insertion du caractère 'x' dans la chaîne 'abracadabra' à la position 5:
=> abraxcadabra
```
ou
```
insertion du caractère 'a' dans la chaîne 'potatoes' à la position 1:
=> apotatoes
```
ou
```
insertion du caractère 'a' dans la chaîne 'potatoes' à la position 0:
=> impossible d'insérer un caractère en position 0
```
ou
```
insertion du caractère 'a' dans la chaîne 'potatoes' à la position 9:
=> impossible d'insérer un caractère en position 9
```

## Exercice 9
Ecrire un programme qui demande à l'utilisateur un mot puis qui lui indique s'il s'agit d'une adresse email valide. Sachant que pour être valide (ici, pour simplifier...): Une adresse email doit commencer par x caractères (x >=1) qui précédent l'unique caractère `@`, puis y caractères (y>=1) qui suivent le `@` et qui précédent le `.`, puis z caractères (z>=1) après l'unique caractère `.`. Les x,y et z caractères, doivent être des caractères acceptés, vous avez de la chance, nous vous fournissons une liste (non-exhaustive) des caractères interdits: `caracteresIntedits`:<br/>

NB: on considère ici qu'une adresse qui a plusieurs `.` est invalide (ce qui n'est pas le cas dans la réalité...)<br/>

Une méthode pourrait être de trouver à quelles positions (index) se trouvent le `@` et le `.` puis de voir si le nombre de caractères qui se trouvent entre ces positions est suffisant. En outre, il faudra tester que chaque caractère de l'adresse n'est pas interdit.

```
caracteresInterdits = ['!','#','$','%','&','*','+',',','-','/','=','?']
```
Le programme affichera:
```
Entrez une adresse: bob@gmail.com
bob@gmail.com est une adresse valide

Entrez une adresse: bob#gmail.com
bob#gmail.com n'est pas valide

Entrez une adresse: bob@gmailcom
bob@gmailcom n'est pas valide

```

-->
