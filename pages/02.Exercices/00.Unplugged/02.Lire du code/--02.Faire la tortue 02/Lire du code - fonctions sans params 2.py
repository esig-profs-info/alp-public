from gturtle import *

def setup():
    makeTurtle()
    speed(-1)

def finalize():
    hideTurtle()
    
def b():
   forward(10)

def c():
    right(45) 

def a():
    repeat 2:
        b()
        c()
    b()

setup()
a()
finalize()