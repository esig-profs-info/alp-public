---
content:
    items: '@self.children'
    order:
        by: folder
        dir: desc
---

# Espace du groupe 3 (ms)

<h1>DEMO</h1>

1. [Opérateur in et algorithme "décompte" (jeu de devinette de carte de Jass)](./operateur-in-et-algo-decompte.py)
2. [Solution de l'exercice 1 du TP-2 (Association Lémanique de Plongée)](./TPAssociationPlongee.py)
3. [Solution des exercices 1 et 2 du TP-2 (Association Lémanique de Plongée)](./TPAssociationPlongee_corrige_thomas_fusion.py)


