---
template: item
title: Fonctions pour les listes (pp. 109-110 du livre)
--- 

# Définir des fonctions pour les listes
Si vous utilisez TigerJython (python 2.x) pour faire les exercices ci-dessous, ajoutez les instructions suivantes au début de chaque programme:

```python
from __future__ import print_function
```

## Exemple 8
```python
def intervalle(debut, fin):
    liste = []
    if debut <= fin:
        x = debut
        while x <= fin:
            liste.append(x)
            x += 1
    return liste

print(intervalle(3, 5))
```
### 20
Définissez une fonction `intervalle_pairs(debut, fin)` qui ne génère que les nombres paires entre `debut` et `fin` et les retourne sous forme de liste. Affichez la valeur retournée par la fonction depuis le programme principal.

### 21
Définissez une fonction `moyenne(liste)` qui calcule et retourne la moyenne arithmétique de tous les nombres d'une liste. Affichez la valeur retournée par la fonction depuis le programme principal.

### 22
Définissez une fonction `somme_liste(liste)` qui calcule et retourne la somme de tous les nombres d'une liste. Affichez la valeur retournée par la fonction depuis le programme principal.

### 23
Définissez une fonction `compte_valeurs(liste, valeurs)` qui compte le nombre d'occurrences du nombre `valeur` dans la liste `liste`. Depuis le programme principal, affichez le résultat, par exemple "La valeur 4 apparaît 7 fois dans la liste".

### 24
À l'exercice 10, tortue dessine un rectangle à l'aide de la boucle `for`. Définissez une fonction `rectangle(a, b)` utilisant cette technique avec une liste et une boucle `for` pour dessiner un rectangle quelconque dont les côtés mesures `a` et `b`.

### 25
Déterminez la liste affichée par la fonction suivante. Commencez par répondre à la question sans utiliser l'ordinatezur avant de vérifier votre solution en exécutant le programme sur l'ordinateur.
```python
def abc():
    liste = []
    x = 1
    while x < 20:
        liste.append(2*x)
        x += 1
    return liste
print(abc())
```