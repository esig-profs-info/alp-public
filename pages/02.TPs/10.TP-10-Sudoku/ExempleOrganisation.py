# -*- coding: utf-8 -*-

# Imports
import numpy as np

#exemple d'une procédure de test pour une fonction
def exercice000():
    g = np.loadtxt("Grille1.txt", dtype=int) # pour lire une grille à partir du fichier Grille1.txt
    résultat = a_faire(g)
    print("résultat = ", résultat)

#exemple d'une procédure de test pour une procédure d'affichage sans calcul
def exercice007():
    afficher_JB()

#exemple d'une fonction demandée
def a_faire(g):
    return g[1, 1] # par exemple. Ça sera plus compliqué en général...

#exemple d'une fonction demandée
def afficher_JB():
    print("James Bond") # par exemple. Ça sera plus compliqué en général...

# Procédure main()
def main():
    #exemple d'appels des procédures de test et seulement celles-ci au premier niveau
    exercice000()
    exercice007()
    
# Appel de la procédure main()    
if __name__ == "__main__":
    main()