# Série 7, Exercice 4
# Clément Vogt

# ---------------------------------------------------------------------------
# Méthode1: On se donne un accumulateur (ici la variable somme) dans laquelle on "accumule" les valeurs successives de nb.
# ---------------------------------------------------------------------------
def main():
    n = int(input("Entrez un nombre entier"))
    
    print("La somme des entiers compris entre 0 et ",n,sep='',end='')
    somme = 0
    for nb in range(0,n+1):
        somme = somme + nb
    print(" vaut ",somme,sep='')
    
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Méthode 2: Il y a une formule de calcul qui donne le résultat directement.
# ---------------------------------------------------------------------------
def main():
    n = int(input("Entrez un nombre entier"))

    print("La somme des entiers compris entre 0 et ",n,sep='',end='')
    print(" vaut ",(n * (n + 1)) // 2,sep='')

if __name__ == "__main__":
    main()