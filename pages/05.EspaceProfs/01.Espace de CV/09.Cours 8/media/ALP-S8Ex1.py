# Série 8, Exercice 1
# Clément Vogt

# Procédures et fonctions
def donnees_valides(n):
    if n >= 0:
        return True
    print("Vos données sont invalides.")
    return False

def afficher(numero,valeur):
    print("F(",numero,") = ",valeur,sep='')

# Procédure main()
def main():
    n = int(input("Entrez une valeur pour n"))
    
    if donnees_valides(n):
        print("Voici les termes de la suite de Fibonacci jusqu'à l'ordre ",n,":",sep='')
        
        f0 = 0
        f1 = 1
        
        if n>=0:
            afficher(0,f0)
        if n>=1:
            afficher(1,f1)
            
        for nb in range (2,n+1):
            fn = f0+f1
            afficher(nb,fn)
            f0 = f1 # Attention à l'ordre dans lequel on fait cette assignation et la suivante
            f1 = fn
        
        ''' Version équivalente    
        fn = f0+f1  
        for nb in range (2,n+1):
            afficher(nb,fn)
            f0 = f1 # Attention à l'ordre dans lequel on fait cette assignation et la suivante
            f1 = fn
            fn = f0+f1
        '''
# Appel de la procédure main()
if __name__ == "__main__":
    main()