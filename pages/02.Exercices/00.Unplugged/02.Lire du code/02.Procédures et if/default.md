---
template: item
published: true
---

# Prodédures et instruction if

Répondez aux questions suivantes et *notez vos réponses*.

Après avoir répondu à toutes les questions, testez les programmes. Ajustez vos réponses si nécessaire. Si les programmes produisent des erreurs, trouver une correction.

Chaque question est complètement indépendante des autres (c'est un nouveau programme à chaque fois).


## Question 1

Soit le programme suivant:

```python
def a():
    print('a')
    
def z():
    print('z')
    a()
    a()
    b()
    
def b():
    a()
    a()
 

z()
```
Qu'affichera ce programme? Pourquoi?

## Question 2
Soit le programme suivant:
```python
def p1():
    print(1)

def p2():
    print(2)
    p1()
    p3()
    p1()
    p1()

def p3():
    print(3)
    p1()
    p1()

def p4():
    print(4)
    p2()
    p1()
    
  
p2()
```
Qu'affichera ce programme? Pourquoi?

## Question 3
Soit le programme suivant:
```python
def iff():
    print('f1')
    if 2//1 == 2:
        ffi()
    fif()
    
def ffi():
    print('f2')
    
def fif():
    print('f3')
    ffi()


iff()
```
Qu'affichera ce programme? Pourquoi?

## Question 4
Soit le programme suivant:
```python
def d():
    print('d')

def b():
    print('b.1')
    c()
    print('b.2')

def c():
    print('c')


a = 2
if a < 3:
    d()
    if a < 5:
        b()
    elif a > 3:
        c()
    else:
        print('else 1')
    print(2)   
d()
```
Qu'affichera ce programme? Pourquoi?

## Question 5
Soit le programme suivant:
```python
def zz():
    yy()
    
def yy():
    zz()
    
yy()
```
Qu'affichera ce programme? Pourquoi?
