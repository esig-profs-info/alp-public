---
template: item
---
# TP - Pendu
===

![](pendu.jpg?cropResize=200,200)

En terme de programmation, ce TP couvre:
* parcours de chaîne avec indices
* construction de chaîne
* mise en forme de chaînes de caractères avec la méthode `format` de la classe `str`
* l'utilisation de la fonction `range` en conjonction avec `for` notamment

En terme de résolution de problème ce TP couvre:
* la construction d'une architecture pour résoudre le problème (sur papier):
	* Définir la liste des fonctions utiles
	* Formuler en français ce que fait chaque fonction
	* Définir comment les fonctions seront utilisées (appelées depuis où, avec quels paramètres, etc)

## But du programme

Le programme doit être un jeu de pendu en mode texte.
Le joueur propose des lettres et le programme compte les essais de lettres non contenues dans le mot à trouver, il affiche aussi l'état du mot en remplaçant le affichant les lettres trouvées.
Si le nombre d'essai de lettres fausses dépasse 10, le joueur a perdu
Si le jouer trouve toutes les lettres, alors il a gagné.

Exemple avec le mot à trouver "bananes":

```
Veuillez proposer une lettre: f
et non... plus que 9 lettres fausses possibles
_ _ _ _ _ _ _ 
Veuillez proposer une lettre: b
oui bravo!
b_ _ _ _ _ _ 
Veuillez proposer une lettre: n
oui bravo!
b_ n_ n_ _ 
Veuillez proposer une lettre: u
et non... plus que 8 lettres fausses possibles
b_ n_ n_ _ 
Veuillez proposer une lettre: e
oui bravo!
b_ n_ ne_ 
Veuillez proposer une lettre: a
oui bravo!
banane_ 
Veuillez proposer une lettre: s
oui bravo!
bananes
Bravo vous avez trouvé le mot bananes
```

## Votre travail

### Etape 1

* **Sur papier : ** dessiner les fonctions / procedures que vous allez utiliser (un rectangle par fonction), et écrivez, en français (pas en python) ce que fait chaque fonction. N'oubliez pas qu'en principe, une fonction fait une seule chose. Evitez des fonctione du type compte_compare_teste_et_affiche().
* **Sur papier : ** ajouter à vos fonctions / procédures les paramètres que vous allez utiliser.
* **Sur papier : ** depuis le main, définissez comment les fonctions que vous avez décidé vont être appelées, comment des valeurs vont "transiter" entre le main et les fonctions. 


### Etape 2

* Codez en python les fonctions choisies (après validation de votre professeur idéalement)
* Debuggez et testez votre programme

### Etape 3

* Lorsque votre jeu fonctionne, améliorez-le:

* vérifiez que l'utilisateur n'entre pas n'importe quoi
* essayez de faire en sorte que votre programme choisisse un mot au hasard parmi une liste de mots (dans un fichier à part si vous vous sentez en confiance)
