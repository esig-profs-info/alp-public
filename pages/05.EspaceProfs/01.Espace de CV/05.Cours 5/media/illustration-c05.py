# Cours 5 - Illustration
# Clément Vogt
print("\n---------------------------\nCours 5 - Illustration\n---------------------------")


NB_DE_FOIS = 20
DEBUT = -9
FIN = -1

# ---------------------------------------------------------------------------
# Point 0
# ---------------------------------------------------------------------------
print("\nPoint 0")

def main():
    sequence = range(1,10) # La fonction range() renvoie une séquence de nombre. Dans notre exemple de 1 à 9 (9 et non 10 car la dernière valeur d’un range est toujours la valeur qui précède celle spécifiée par le paramètre stop).
    for element in sequence: # La boucle for permet de parcourir cette séquence. La variable element prend successivement les valeurs contenus dans la séquence soit 1,2,3,4,5,6,7,8,9.
        print(element) # Il est possible de traiter un à un les éléments contenus dans la séquence.
    
    # ou directement...
    for element in range(1,10):
        print(element)
        
    # Une boucle for permet de répéter un certain nombre de fois (dans notre cas 9) l'exécution d'une tâche (dans notre cas print(element)). Cette tâche peut être une instruction élémentaire ou alors une tâche plus complexe.

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 1: Ce  qu'il ne faut pas faire
# ---------------------------------------------------------------------------
print("\nPoint 1: Ce  qu'il ne faut pas faire")

# On n'écrit JAMAIS plusieurs fois la même instruction. On fait TOUJOURS une boucle.
# Une boucle permet de répéter un certain nombre de fois l'exécution d'une tâche. Cette tâche peut être une instruction élémentaire ou alors une tâche plus complexe.
def main():
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    print("Hello !")
    
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 2
# ---------------------------------------------------------------------------
print("\nPoint 2")

def main():
    # La variable nb permet de numéroter les tours effectués par la boucle (le nombre de fois que l'instruction est exécutée)
    for nb in range(1,NB_DE_FOIS+1): # NB_DE_FOIS+1 et non NB_DE_FOIS car la dernière valeur d’un range est toujours la valeur qui précède celle spécifiée par le paramètre stop.
        print("Hello !")
    # La variable nb prend successivement les valeurs 1, 2, 3, ..., NB_DE_FOIS et l'instruction est répétée autant de fois.
    # L'exécution continue après la boucle lorsque la variable nb dépasse la limite fixée, ici NB_DE_FOIS.

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 3
# ---------------------------------------------------------------------------
print("\nPoint 3")

# La variable nb a une valeur qu'on peut utiliser dans l'instruction contrôlée par la boucle (calculs, affichage).
def main():
    for nb in range(1,NB_DE_FOIS+1):
        print(nb,end='')
        print(". Hello !")        

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 4
# ---------------------------------------------------------------------------
print("\nPoint 4")

# La variable nb peut prendre des valeurs successives à distance CONSTANTE, ici: 3
# Les valeurs de nb seront donc: 1, 4, 7, 10, 13, 16, 19.  Pas 22, car 22 > NB_DE_FOIS.
def main():
    for nb in range(1,NB_DE_FOIS+1,3):
        print(nb,end='')
        print(". Hello !")
        
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 5
# ---------------------------------------------------------------------------
print("\nPoint 5")

# S'il y a plusieurs instructions à exécuter sous contrôle de la boucle, on en fait une procédure (comme d'hab.).
# Comme c'est "un truc à répéter", c'est forcément une tâche => procédure ou fonction.
# En fait, en toute généralité, la plupart du temps, la tâche à répéter est implantée sous la forme d'une procédure ou fonction recevant la variable de contrôle de la boucle comme paramètre. 
def traiter(numero):
    print(numero,end='')
    print(". Hello !")     

def main():
    for nb in range(1,NB_DE_FOIS+1,3):
        traiter(nb)

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 6
# ---------------------------------------------------------------------------
print("\nPoint 6")

# Comme d'habitude, si la tâche devait être effectuée pour une occurrence donnée, on pourrait la réutiliser.
traiter(37)

# ---------------------------------------------------------------------------
# Point 7
# ---------------------------------------------------------------------------
print("\nPoint 7")

# La variable nb peut prendre ses valeurs dans le sens descendant.
def main():
    for nb in range(NB_DE_FOIS,1-1,-1): # 1-1 (ou 0) et non 1 car la dernière valeur d’un range est toujours la valeur qui précède celle spécifiée par le paramètre stop
        traiter(nb)

if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 8
# ---------------------------------------------------------------------------
print("\nPoint 8")

# La variable nb peut prendre ses valeurs dans le sens descendant, également avec un pas CONSTANT, ici -3
# On aura donc les valeurs 20, 17, 14, 11, 8, 5, 2 pour nb (pas -1, car la limite est 1)
def main():
    for nb in range(NB_DE_FOIS,1-1,-3):
        traiter(nb)
        
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Point 9
# ---------------------------------------------------------------------------
print("\nPoint 9")

# La variable nb peut prendre des valeurs négatives.  Ici: DEBUT = -9 et FIN = -1. Les valeurs de nb seront donc -9, -8, ..., -2, -1
def main():
    for nb in range(DEBUT,FIN+1):
        traiter(nb)
        
if __name__ == "__main__":
    main()


# Attention:
#    -   Lorsque la boucle débute, les bornes (valeurs de début et de fin de la variable de contrôle) doivent être connues.
#    -   Elles peuvent être variables, càd être obtenues par le calcul, mais, une fois que la boucle a démarré, elles ne peuvent
#        plus changer. En particulier, il est interdit de modifier la valeur de la variable fixant la fin de la boucle à l'intérieur des
#        instructions exécutées sous le contrôle de la boucle.