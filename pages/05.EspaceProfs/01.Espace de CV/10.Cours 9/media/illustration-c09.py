# Cours 9 - Illustration
# Clément Vogt
print("\n---------------------------\nCours 9 - Illustration\n---------------------------")


# ---------------------------------------------------------------------------
# Déclaration d'une variable de type liste et ajout de composants à l'intérieur
# ---------------------------------------------------------------------------
print("\nDéclaration d'une variable de type liste et ajout de composants à l'intérieur")

def main():
    # Déclaration d'une variable de type liste
    liste = [] # Liste vide

    # Déclaration d'une variable de type liste et ajout de composants à l'intérieur
    liste = ['p',4,'o',2.5,'e'] # Liste possédant 5 composantes de différents types

    # Liste définie et remplie au moyen d'un input (saisie de données par l’utilisateur)
    # Pour se simplifier la vie, on part du principe qu'une liste remplie à l'aide d'un input DOIT UNIQUEMENT contenir des composants de même type.
    # Lors de la saisie des composants de la liste par l'utilisateur, ceux-ci doivent être séparés par un espace: Exemple: 1 2 3 4 5 6
    liste = input("Entrez une liste de valeurs entières").split() # Si la liste contient uniquement des chaines de caractères (str)
    liste = list(map(int,input("Entrez une liste de valeurs entières").split())) # Sinon => Ici, la liste contient des entiers (int). On spécifie le type dans le premier paramètre de la fonction map()
    
    ########## CETTE PARTIE EST DESTINEE AUX CURIEUX, SA COMPREHENSION N'EST PAS DEMANDEE ##########
    # La méthode split(): Divise une chaîne ne caractère (str) en fonction d'un séparateur (un espace par défaut) et retourne une liste contenant chaque composante du str.
    # La fonction map(fonction,itérable): Applique une fonction donnée à chaque composant d'un itérable (un objet dont on peut parcourir les valeurs, par exemple une liste) et renvoie une map (qui est aussi un itérable).
    # La fonction int() convertit un nombre ou un string en un nombre entier.
    # La fonction list(itérable) permet de créer une liste et ajoute les composants contenus dans l'itérable à celle-ci.
    # Dans notre cas:
    #     1. On utilise la méthode split() afin de diviser le str saisi par l'utilisateur et ajouter chacune de ses composantes dans une liste.
    #     2. On utilise la fonction map() pour convenir les composants de la liste qui sont des str (je vous rappelle les input() sont toujours considérées par Python comme étant des chaînes de caractères) en un autre type, ici int (à l'aide la fonction int())
    #     3. On utilise la fonction list() pour convertir notre map en list.
    #     Résultat: On a converti une chaîne de caractère en une liste d'un certain type.
    ################################################################################################
    
if __name__ == "__main__":
    main()


# ---------------------------------------------------------------------------
# Accéder à une composante d'une liste
# ---------------------------------------------------------------------------
print("\nAccéder à une composante d'une liste")

def main():
    liste = ['p',4,'o',2.5,'e']

    # Pour accéder à un composant d'une liste on doit spécifier son indice => nom_liste[indice]
    print(liste[0]) # On récupère le composant à l'indice 0 (donc la lettre p) et on l'affiche. Attention, l’indice de la première composante d'une liste est 0 et non 1.

    print("Avant =",liste[3])
    liste[3] = 8 # On modifie la valeur du composant à l'indice 3 => 2.5 devient 8
    print("Après =",liste[3])

if __name__ == "__main__":
    main()


# ---------------------------------------------------------------------------
# Parcourir une liste: Afficher les composantes d'une liste
# ---------------------------------------------------------------------------
print("\nParcourir une liste: Afficher les composantes d'une liste")

def main():
    liste = [1,2,3,4,5,6]
    
    # On veut afficher les 6 données: boucle qui parcourt la liste
    print("Valeurs de la liste:")
    for composant in liste:
        print(composant)
        
    # Si, on souhaite le numéro d'ordre (indice de la composante), on peut parcourir la liste de cette manière:
    print("Valeurs de la liste:")
    for k in range(0,len(liste)): # La fonction len() retourne le nombre de composants de la liste qui lui est passée en paramètre. Attention, l’indice de la première composante d'une liste est 0 et non 1, l’indice de la dernière composante est donc la longueur de la liste - 1  (décalage de 1 comme on part de 0). Ceci explique pourquoi nous n'avons pas besoin de faire len(liste) + 1 pour inclure la dernière composante de la liste.
        print("En ",k," : ",liste[k],sep='')
        
    # Il est  également possible d'utiliser une boucle while. Cependant, on préfère l'utilisation d'une boucle for car ici on sait combien de fois on va devoir itérer et que le pas est constant. La boucle for en Python a spécialement été conçue pour parcourir des séquences comme les listes.
    print("Valeurs de la liste:")
    k = 0 
    while (k < len(liste)): # Attention, l’indice de la première composante d'une liste est 0 et non 1. Le dernier indice valide est donc la longueur de la liste - 1
        print("En ",k," : ",liste[k],sep='')
        k = k + 1

if __name__ == "__main__":
    main()


# ---------------------------------------------------------------------------
# Parcourir une liste: Modifier la valeur des composantes d'une liste
# --------------------------------------------------------------------------- 
print("\nParcourir une liste: Modifier la valeur des composantes d'une liste")

# On a besoin d'une procédure qui affiche les données d'une liste afin de visualiser si les modifications que l'on va effectuer sont prises en compte.
def afficher_liste(liste):
    print("Valeurs de la liste:")
    for composant in liste:
        print(composant)   

# Ici, on souhaite remplacer la valeur de chaque composante de la liste par son double 
def main():
    liste = [1,2,3,4,5,6]
    
    # On pourrait penser que le code suivant est fonctionnel, cependant il ne l'est pas. Ceci, car cette méthode travaille uniquement sur une COPIE des composants et non sur les composants eux-même.
    for composant in liste:
        composant = 2 * composant
        
    afficher_liste(liste)
    
    # Pour modifier la valeur des composantes d'une liste, il faut utiliser une des méthodes avec les numéros d'ordre (indices).
    for k in range(0,len(liste)):
        liste[k] = 2 * liste[k]
        
    afficher_liste(liste)
    
if __name__ == "__main__":
    main()


# ---------------------------------------------------------------------------
# Arrondir un nombre: round()
# ---------------------------------------------------------------------------
print("\nArrondir un nombre: round()")

print(round(5.256)) # Arrondi le nombre à l'entier le plus proche
print(round(5.256, 1)) # Arrondi le nombre à une décimale
print(round(5.256, 2)) # Arrondi le nombre à deux décimales