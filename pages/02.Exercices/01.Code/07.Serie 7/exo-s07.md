---
template: item
published: true
---

# Série 7
## Objectifs
Comprendre les concepts liés à la **boucle for** et les mettre en œuvre.

Identifier les différentes étapes importantes dans la conception d'une boucle.

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S7Ex1** à **ALP-S7Ex5** et se trouveront dans un dossier nommé **ALP-S7**.

## Énoncé

### Exercice 1
Afficher les nombres entiers pairs compris entre 0 et n dans l'ordre **croissant**.

### Exercice 2
Afficher les nombres entiers pairs compris entre n et 0 dans l'ordre **décroissant**.

### Exercice 3
A partir de la donnée d'un capital de départ, d'un taux d'intérêt et d'un nombre de périodes, calculer et afficher le capital obtenu en appliquant le principe des intérêts composés au capital pendant le nombre de périodes considéré.

Vous procéderez itérativement en ajoutant les intérêts au capital à l'issue de chaque période (la formule directe des intérêts composés ne nous intéresse pas dans ce contexte).

<u>Exemples d’exécution :</u>

![](./media/image1.png)
![](./media/image2.png)
![](./media/image3.png)

### Exercice 4
Calculez et affichez la **somme** des nombres entiers compris entre **0** et **n**.

### Exercice 5
Calculez et affichez le **produit** des nombres entiers compris entre **1** et **n**.
