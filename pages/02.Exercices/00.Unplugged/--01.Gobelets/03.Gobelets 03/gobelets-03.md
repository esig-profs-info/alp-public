---
template: item
---

Activité numéro 3 avec des gobelets. Il s'agit ici d'écrire du code pour obtenir un résultat donné

===

##Exercices - Gobelets
###Activité n°3 - Ecriture du code
#### Contexte:
* Activité individuelle

#### Ce que l'on retient de l'activité précédente:
* *Qu'est-ce qu'un algorithme? A quoi ça sert?*
* *Qu'est-ce qu'un langage de programmation? A quoi ça sert?*
* *Qu'est-ce que du code? Quelle différence avec un algorithme?*
* *Qu'est-ce qu'une instruction?*
* *Qu'est-ce que déboguer pas-à-pas? (et qu'est-ce qu'un bug?)*


#### Concernant la correction de l'activité 2:
Pas de correction fournie, vous devez trouver un consensus dans la classe (avec l'approbation de votre ensiegnant).

#### Votre travail:
Vous trouverez ci-dessous des photos représentant des configuration de gobelets. Ecrivez les algorithmes correspondant à chaque photo.
*NB: on admet qu'une pile de gobelets se trouve collée à gauche de chaque configuration proposée...*

#####Photo n°1:
![photo gobelet 1](pyramide_gobelets_1.jpg)
#####Photo n°2:
![photo gobelet 2](pyramide_gobelets_2.jpg)
#####Photo n°3:
![photo gobelet 3](pyramide_gobelets_3.jpg)
#####Photo n°4:
![photo gobelet 4](pyramide_gobelets_4.jpg)
#####Photo n°5:
![photo gobelet 5](pyramide_gobelets_5.jpg?cropResize=350,350)