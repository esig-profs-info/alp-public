---
template: item
published: true
---

# Opérateurs logiques

Répondez aux questions suivantes et *notez vos réponses*.

Après avoir répondu à toutes les questions, testez les programmes. Ajustez vos réponses si nécessaire. Si les programmes produisent des erreurs, trouver une correction.

Chaque question est complètement indépendante des autres (c'est un nouveau programme à chaque fois).


## Question 1

Soit la fonction:

```python
def meteo1(humidite, temperature):
    if humidite > 50 and temperature < 20:
        return 'a'
    elif humidite > 60:
        return 'c'
    return 'b'
    
```
Que retournent les appels suivants?

a) meteo1(50, 15)

b) meteo1(80, 25)

c) meteo1(60.1, 19)

## Question 2

Soit la fonction:

```python
def meteo2(humidite, temperature):
    if temperature < -5 or temperature > 40:
        return 'a'
    elif humidite > 80:
        return 'b'
    else:
        return 'c'

    
```
Que retournent les appels suivants?

a) meteo2(80, 10)

b) meteo2(81, 41)

c) meteo2(81, 40)

## Question 3

Soit la fonction:

```python
def meteo3(option, humidite, temperature):
    if option and (temperature > 10 or humidite < 80):
        return 'a'
    elif not option:
        return 'b'
    elif False:
        return 'c'
    return 'd'
    
```
Que retournent les appels suivants?

a) meteo3(True, 10, 80)

b) meteo3(False, 15, 80)

c) meteo3(True, 13, 80)

d) meteo3(True, -15, 100)