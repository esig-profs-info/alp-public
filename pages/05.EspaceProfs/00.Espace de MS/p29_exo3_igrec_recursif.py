from gturtle import *

def igrec_recursif(longueur):
    if longueur < 10:
        fd(10)
        bk(10)
    else:
        fd(longueur)
        lt(30)
        igrec_recursif(longueur/2)
        rt(60)
        igrec_recursif(longueur/2)
        lt(30)
        bk(longueur)
         
makeTurtle()
hideTurtle()
penUp()
bk(200)
penDown()
igrec_recursif(160)


    