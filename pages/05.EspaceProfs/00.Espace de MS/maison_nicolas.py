from gturtle import *
from math import *

makeTurtle()

setPenWidth(3)
setPenColor("red")

fd(100)
rt(90)
fd(100)
rt(180-45)
fd(sqrt(2*(100**2)))
lt(180-45)
fd(100)
lt(90)
fd(100)
lt(90-60)
fd(100)
lt(180-60)
fd(100)
lt(90-60 + 45)
fd(sqrt(2*(100**2)))

