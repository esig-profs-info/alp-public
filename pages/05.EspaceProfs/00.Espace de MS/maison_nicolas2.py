from gturtle import *
from math import *

makeTurtle()


setPenColor("red")
setPenWidth(3)

fd(80)
rt(90)
fd(50)
# on utilise la formule expliquée ici: 
# https://www.assistancescolaire.com/eleve/3e/maths/reviser-une-notion/calculer-la-mesure-d-un-angle-dans-un-triangle-rectangle-3mtr07
rt(90 + degrees(atan(5/8)))
fd(sqrt(50**2 + 80**2))
lt(90 + degrees(atan(5/8)))
fd(50)
lt(90)
fd(80)
lt(90-60)
fd(50)
lt(180-60)
fd(50)
lt(90-60 + degrees(atan(5/8)))
fd(sqrt(50**2 + 80**2))
hideTurtle()

