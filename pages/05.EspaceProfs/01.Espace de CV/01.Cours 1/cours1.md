---
template: item
published: true
---

# Cours 1

===

* <u>Rappel:</u> Exécuter (<span style="color:red">F5</span>) / Déboguer (<span style="color:red">CTRL+F5</span>)
* Commentaires : **#** avant le texte du commentaire
* Complétion automatique du code: <span style="color:red">TAB</span>.
* Mots réservés
* Variable
    * zone mémoire,
    * nommée,
    * « typée ».
* Une variable est désignée par un nom et possède un contenu d'un certain type
* Convention de nommage des variables :
    * Nom signifiant, exprimant la valeur contenue par la variable.
    * Le nom est entièrement écrit en minuscules et les mots sont séparés par des signes soulignés (_). <u>Exemple :</u> mon_resultat, prix_final
* Types simples de base :
    * int
    * float
    * str
    * bool
* Déclaration de variables
    * Données entrées
    * Résultats intermédiaires et finaux
* Les guillemets (ou apostrophes) permettent de spécifier une chaîne de caractère (str). Les utiliser lorsque vous souhaitez du texte, ne les utilisez pas si vous souhaitez calculer un résultat numérique. 
* Opérations de base:
    * L'assignation **=**
    * Expressions arithmétiques
    * les 7 opérations (+, -, *, /, //, % et **)
    * priorité des opérations,
    * parenthèses.
* Procédure d'affichage : [print()](https://docs.python.org/fr/3/library/functions.html#print)
    * print(objet(s), sep=séparateur, end=fin, file=sortie) 
        * objet(s) : Spécifie les éléments que vous souhaitez afficher. La procédure print() admet des éléments qui peuvent être de différents types et que l’on sépare par des virgules.
        * sep (optionnel): Spécifie ce qui sépare les éléments affichés, s'il y en a plusieurs. La valeur par défaut est un espace ("  ")
        * end (optionnel): Spécifie quoi imprimer à la fin. La valeur par défaut est un saut de ligne (\n)
        * file (optionnel) : Spécifie où l’affichage doit s’effectuer. La valeur par défaut est la console (sys.stdout)
* Saisie de données par l’utilisateur : [input()](https://docs.python.org/fr/3/library/functions.html#input)
    * Permet de récupérer les données d'entrées saisies par l'utilisateur
    * input(prompt)
        * prompt (optionnel): Spécifie une chaîne de caractère, représentant un message par défaut avant l'entrée de l’utilisateur et permettant généralement d’indiquer à ce dernier que l’on attend une entrée de sa part.
    * Les saisies de l’utilisateurs sont toujours considérées par Python comme étant des chaînes de caractères (str). Attention, lorsque celles-ci ne sont pas des chaînes de caractères il faut utiliser des fonctions de conversion.
* Conversion des types de données (**cast**)
    * [int()](https://docs.python.org/fr/3/library/functions.html#int) : permet de convertir un élément en entier (nombre entier)
    * [float()](https://docs.python.org/fr/3/library/functions.html#float) : permet de convertir un élément en flottant (nombre réel)
    * [str()](https://docs.python.org/fr/3/library/functions.html#func-str) : permet de convertir un élément en string (chaîne de caractères)
* <u>Illustration:</u> Exercice 1 de la série 1 (voir [ALP-S1Ex1](./media/ALP-S1Ex1.py))
* L'alternative (**if ... : ... else: ...**).
    * Expressions logiques,
    * comparaison,
    * les 6 opérations de comparaison (<, <=, >, >=, ==, !=)
    * les 3 opérateurs logiques (not, and, or)
    *
        ```python
        if <expression_booléenne> :
            <instruction1>
        else:
            <instruction2>
        ```

* <u>Illustration:</u> Exercice 4 de la série 1 (voir [ALP-S1Ex4](./media/ALP-S1Ex4.py))
* Indentation: En Python, il est <u>nécessaire</u> d’indenter votre code afin d’indiquer à quel bloc de code appartient une instruction, ce n’est pas que de la cosmétique comme pour d’autres langages.
    * Utilisez 4 espaces par niveau d'indentation.
* Tortue:
    * La tortue graphique est une manière bien connue et intuitive pour initier les étudiants au monde de la programmation.
    * La tortue permet de dessiner des formes complexes en utilisant un programme qui répète des actions élémentaires.
    * 
        ```python
        from turtle import *

        <action1>
        <action2>
        <actionN>

        exitonclick()
        ```
    * Quelques actions:    
        * [forward(distance)](https://docs.python.org/fr/3/library/turtle.html#turtle.forward) : Avancer de la distance indiquée
        * [back(distance)](https://docs.python.org/fr/3/library/turtle.html#turtle.back) : Reculer de la distance indiquée
        * [left(angle)](https://docs.python.org/fr/3/library/turtle.html#turtle.left) : Tourner sur place à gauche de angle degrés
        * [right(angle)](https://docs.python.org/fr/3/library/turtle.html#turtle.right) : Tourner sur place à droite de angle degrés
    * [Liste des actions](https://docs.python.org/fr/3/library/turtle.html#turtle-methods)
* Le [mémento](/ressources/quick%20reference/MementoPython_v1.pdf) Python    
* [Illustration des notions](./media/illustration-c01.py)

**<u>Exercices :</u>** [Série 1](/exercices/code/serie%201)