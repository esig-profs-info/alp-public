# Cours 8 - Illustration
# Clément Vogt
print("\n---------------------------\nCours 8 - Illustration\n---------------------------")

# ---------------------------------------------------------------------------
# Boucle while: Méthodologie de construction d'une boucle
# --------------------------------------------------------------------------
print("\nBoucle while: Méthodologie de construction d'une boucle")

def main():
    nb = 1 # 2. Définir la valeur initiale de la variable de pilotage (opération Init dans le schéma)
    while nb <= 10: # 3. Définir la condition de la boucle en fonction de la variable de pilotage (Cond dans le schéma). C'est la condition de continuation.
        print("Instructions...") # 5. Définir la séquence itérée - celle qu'on répète (opération Séquence dans le schéma)
        nb = nb + 1 # 4. Définir l'actualisation de la variable de pilotage - la valeur qu'elle devra avoir au prochain tour de la boucle (opération Actualisation dans le schéma)
        
if __name__ == "__main__":
    main()

# ---------------------------------------------------------------------------
# Boucle while: Caractéristiques
# ---------------------------------------------------------------------------
print("\nBoucle while: Caractéristiques")

def main():
    nb = 11
    while nb <= 10: # La séquence sous contrôle de la boucle peut n'être jamais exécutée (si l'expression booléenne qui contrôle la boucle est fausse lorsqu'on arrive dessus pour la première fois). Ici la variable de pilotage de la boucle nb est == à 11, elle est donc > à 10 => Conséquence: On n'entre pas dans la boucle.
        print("Instructions...")  # La séquence sous contrôle de la boucle
        nb += 1 # Identique à nb = nb + 1
    
    nb = 1
    while nb <= 10:
        print("Instructions...") # Il est fondamental que la séquence itérée modifie une des variables qui fait partie de l'expression booléenne qui contrôle la boucle, sinon on se trouve en présence d'une boucle infinie (si aucune variable de l'expression booléenne n'est modifiée, celle-ci ne peut pas changer de valeur de vérité et on ne ressortira donc plus de la boucle). Ici la variable de pilotage (nb) n'est pas actualisée, elle n'atteindra donc jamais 10 => Conséquence: On ne sortira jamais de la boucle.
        # ICI, Il manque l'actualisation de la variable de pilotage
        
if __name__ == "__main__":
    main()