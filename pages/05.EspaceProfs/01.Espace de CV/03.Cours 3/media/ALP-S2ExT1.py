# Série 2, Exercice T1
# Clément Vogt

# Imports
from turtle import *

# Procédures et fonctions
def ligne():
    forward(10)
    back(10)

def i_grec():
    forward(10)
    left(30)
    ligne()
    right(60)
    ligne()
    left(30)
    back(10)
    
def double_i_grec():
    forward(20)
    left(30)
    i_grec()
    right(60)
    i_grec()
    left(30)
    back(20)
    
def quadruple_i_grec():
    forward(40)
    left(30)
    double_i_grec()
    right(60)
    double_i_grec()
    left(30)
    back(40)

# Procédure main()
def main():
    left(90)
    quadruple_i_grec()

# Appel de la procédure main()
main()