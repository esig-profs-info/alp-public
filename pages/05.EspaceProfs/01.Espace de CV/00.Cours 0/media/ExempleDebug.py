# ExempleDebug
# Clément Vogt

print("Avant")

if (1 < 2):
    print("1 est supérieur ou égal à 2") # Il y a une erreur, il faut déboguer !
else:
    print("1 est inférieur à 2")

print("Après")