---
template: item
published: true
---

Exercices traitant les boucles en python

===

<!--todo: lister les pré-requis-->

### Attention: 
pour coder dans TigerJython, ajouter la ligne suivante en haut de votre code:
```
from __future__ import print_function
```

## Exercice 1
Afficher les nombres entiers pairs compris entre 0 et 10.

## Exercice 2
Afficher les nombres entiers pairs compris entre 0 et N (N entré par l'utilisateur).

*Données de test:	3   20   23   0  -1*

## Exercice 3
Afficher les nombres entiers pairs compris entre M et N (M et N entrés par l'utilisateur, avec M <= N).

**NB:** Si M>N afficher le message d'erreur de votre choix et ne pas effectuer le calcul

## Exercice 4
Afficher les nombres entiers pairs compris entre M et N (M et N entrés par l'utilisateur, avec M pouvant être plus grand que N).

## Exercice 5
Afficher le livret de 13 (jusqu'à "fois 12")
le résultat doit s'afficher sous la forme suivante:<br/>
*1 x 13 = 13<br/>
2 x 13  = 26<br/>
...<br/>
12 x 13 = 156*

## Exercice 6
Afficher le livret de N, de la même manière que pour l'exercice précédent mais cette fois avec N entré par l'utilisateur

## Exercice 7
A partir de la donnée d'un capital de départ c, d'un taux d'intérêt t et d'un nombre de périodes p, calculer et afficher le capital obtenu en appliquant le principe des intérêts composés à c pendant p périodes.

On supposera que la monnaie utilisée est le francs suisse, CHF en notation comptable.

Structurellement, votre programme devra comporter au moins une fonction de calcul qui ne fait ni lecture, ni affichage.

**Défi en plus (optionnel):** Arrondir aux 5 centimes l'affichage du capital obtenu. 

#### Données de test pour l'ex7:
<!--todo: transformer en tableau avec ajout colonne résultat-->
    (C   t   p) 
    (1000.0   1.0   1)   (1234.50   5.5   11)
    (1000.0   0.0   5)   (1000.0   1.0   -1)
    (500.0   -1.0   1)   (-500.0   1.0   1)
    (-500.0   -1.0   1)   (-500.0   -1.0   -1)

## Exercice 8
Calculer et afficher la somme des nombres entiers compris entre 0 et N (N entré par l'utilisateur).
	
#### Données de test pour l'ex8:	
<!--todo: transformer en tableau avec ajout colonne résultat-->
	3   10   20   123   997  -1   0

## Exercice 9
<!--todo: transformer en tableau avec ajout colonne résultat-->
Calculer et afficher le produit des nombres entiers compris entre 0 et N (N entré par l'utilisateur).
	
#### Données de test pour l'ex9:
	3   10   20   123   997  -1   0

## Exercice 10
Afficher un triangle isocèle formé d'étoiles. La hauteur du triangle (c'est-à-dire le nombre de lignes) sera fourni par l'utilisateur, comme dans l'exemple ci-dessous. On s'arrangera pour que la dernière ligne du triangle s'affiche sur le bord gauche de l'écran.
```
combien de lignes? 5
    *   
   ***
  *****
 *******
*********
```