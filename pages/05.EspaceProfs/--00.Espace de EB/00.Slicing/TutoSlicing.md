---
template: item
published: true
---

# Pour accéder au tutorial sur le slicing

===

* Fichier .pdf : [Instructions à suivre pour l'installation](./media/Comment%20exécuter%20un%20notebook%20Jupyter.pdf) 
* Fichier à décompacter selon les instructions : [EduPython version 3.0, lien sur le Drive eduge.ch](https://drive.google.com/file/d/11v2ExZy0WKVz772SxjSYR0JoERj0KI1i/view?usp=sharing) <br> ou
    [site de téléchargement au besoin](https://edupython.tuxfamily.org/)
* Fichier notebook lui-même : [notebook sur le *slicing*](./media/Slicing.ipynb)