# Série 11, Exercice 4
# Clément Vogt

# Procédures et fonctions
def donnees_entree():
    return input("Quel mot voulez-vous tester?")

# Version 1
def est_palindrome_v1(mot):
    debut = 0
    fin = len(mot)-1
    while debut < fin and mot[debut] == mot[fin]:
        debut+=1
        fin-=1    
    if debut >= fin:
        return True
    else:
        return False
    
# Version 2
def est_palindrome_v2(mot):
    moitie = len(mot) // 2
    for i in range(moitie):
        if mot[i] != mot[-i-1]:
            return False
    return True

def afficher_resultats(mot,palindrome):
    print("-->",mot,end=" ")
    if palindrome:
        print("est",end=" ")
    else:
        print("n'est pas",end=" ")
    print("un palindrome")

# Procédure main()
def main():
    mot = donnees_entree()
    palindrome = est_palindrome_v2(mot)
    afficher_resultats(mot,palindrome)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()