---
template: item
published: true
---

Troisième série d'exercices sur les listes traitant du slicing.

===

## Exercice 1
Soit la liste nums = [35, 26, 19, 23, 6, 8, 18, 1, 34]. Quelle est la valeur de chacune de ces expressions:

1. nums[3]
1. nums[-1]
1. nums[:4]
1. nums[6:]
1. nums[2:5]
1. nums[3:7]
1. nums[1::2]
1. nums[::-1]

## Exercice 2
Soit la liste nums = [32, 18, 25, 34, 23, 24, 40, 37, 4, 31]. Quelle est la valeur de chacune de ces expressions:
1. nums[2]
1. nums[-1]
1. nums[:-1]
1. nums[3:]
1. nums[1:4]
1. nums[5:8]
1. nums[::2]
1. nums[::-1]

## Exercice 3
Supposons que items est une liste non vide. Comment avoir en Python:
1. Le premier élément à partir du début
1. Le premier élément à partir de la fin (autrement dit le dernier)

## Exercice 4
Supposons que items est une listeayant au moins 3 éléments. Comment avoir en Python:
1. Le troisième élément à partir du début
1. L'avant-dernier élément

## Exercice 5
Soit la liste items = [7, 4, 27, 26, 2, 38, 19, 10, 34]. Ecrivez des expressionsen Python qui utilisentla liste items pour produire chacune de ces valeurs:
1. 38
1. [7, 4]
1. [2, 38, 19]
1. [4, 26, 38, 10]

## Exercice 6
Soit la liste items = [11, 29, 22, 36, 40, 25, 14, 7, 16]. Ecrivez des expressions en Python qui utilisent la liste items pour produire chacune de ces valeurs:
1. [29]
1. [14, 7, 16]
1. [36, 40, 25, 14]
1. [16, 7, 14, 25, 40]

## Illustration 
Représentation de la liste [10, 3, -1, 8, 24]:

![](listes-indices_neg.png)
