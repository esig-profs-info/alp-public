# Série 3, Exercice 1
# Clément Vogt
# Variante: on a en entrée 2 rayons de cercle et on doit, pour chacun, faire les calculs d'aire et de circonférence

# Imports
import math

# Procédures et fonctions
def donnees_entree():
    rayon1 = float(input("Entrez un premier rayon"))
    rayon2 = float(input("Entrez un deuxième rayon"))
    return rayon1, rayon2

def calcul_aire(r):
    return math.pi * r**2

def calcul_circonference(r):
    return math.pi * r * 2

def afficher_resultats(r,a,c):
    print("Un cercle de rayon ",r," a une aire de ",a," et une circonférence de ",c,sep="")

# Procédure main()
def main():
    rayon1, rayon2 = donnees_entree()
    aire1 = calcul_aire(rayon1)
    circonference1 = calcul_circonference(rayon1)
    afficher_resultats(rayon1,aire1,circonference1)
    aire2 = calcul_aire(rayon2)
    circonference2 = calcul_circonference(rayon2)
    afficher_resultats(rayon2,aire2,circonference2)    

# Appel de la procédure main()
if __name__ == "__main__":
    main()