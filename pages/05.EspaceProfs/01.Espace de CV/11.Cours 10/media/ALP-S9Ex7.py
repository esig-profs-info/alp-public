# Série 9, Exercice 7
# Clément Vogt

# Constantes
# passe juste
LISTE_1 = [3, 3, 6, 6, 4, 9, 5, 6, 4, 7, 6, 4, 3, 3, 1]
JAUGE_1 = 70

# passe juste
LISTE_2 = [4, 9, 1, 7, 10, 5, 6, 10, 4, 6, 8, 9, 3, 7, 3, 8]
JAUGE_2 = 100

# passe ou pas selon l'algo
LISTE_3 = [8, 2, 6, 2, 4, 6, 7, 6, 10, 1, 2, 2, 1, 4]
JAUGE_3 = 60

# passe ou pas selon l'algo
LISTE_4 = [6, 3, 1, 7, 7, 6, 7, 4, 3, 4, 9, 8, 9, 5, 2, 4]
JAUGE_4 = 80

# Procédures et fonctions
def controle_escalade(liste, jauge):
    print("Liste =", liste, "avec une jauge de", jauge)
    somme = 0
    pos = 0
    while somme < jauge: # dans ce cas on accepte le dernier groupe même s'il dépasse
        somme += liste[pos]
        print("groupe n°", pos, "de", liste[pos], "personnes accepté")
        delta = jauge-somme
        if delta > 0:
            print("encore", delta, "peuvent entrer")
        pos += 1
    if pos == len(liste):
        print("tous les groupes ont été acceptés")
    else:
        print("les groupes n°s", pos, "à", len(liste)-1, "ont été refoulés")
    print()

def controle_escalade_strict_v1(liste, jauge):
    print("Liste =", liste, "avec une jauge de", jauge)
    somme = 0
    pos = 0
    while pos < len(liste) and somme + liste[pos] <= jauge: # dans ce cas on accepte le dernier groupe que s'il ne dépasse pas
        somme += liste[pos]
        print("groupe n°", pos, "de", liste[pos], "personnes accepté")
        delta = jauge-somme
        if delta > 0:
            print("encore", delta, "peuvent entrer")
        pos += 1
    if pos == len(liste):
        print("tous les groupes ont été acceptés")
    else:
        print("les groupes n°s", pos, "à", len(liste)-1, "ont été refoulés") 
    print()

def controle_escalade_strict_v2(liste, jauge):
    print("Liste =", liste, "avec une jauge de", jauge)
    restant = jauge
    pos = 0
    while pos < len(liste) and liste[pos] <= restant: # dans ce cas on accepte le dernier groupe que s'il ne dépasse pas
        restant -= liste[pos]
        print("groupe n°", pos, "de", liste[pos], "personnes accepté")
        if restant > 0:
            print("encore", restant, "peuvent entrer")
        pos += 1
    if pos < len(liste):
        print("les groupes n°s", pos, "à", len(liste)-1, "ont été refoulés")
    else:
        print("tous les groupes ont été acceptés")  
    print()
    
# Procédure main()
def main():
    controle_escalade(LISTE_1, JAUGE_1)
    controle_escalade_strict_v2(LISTE_1, JAUGE_1)
    
    controle_escalade(LISTE_2, JAUGE_2)
    controle_escalade_strict_v2(LISTE_2, JAUGE_2)
    
    controle_escalade(LISTE_3, JAUGE_3)
    controle_escalade_strict_v2(LISTE_3, JAUGE_3)
    
    controle_escalade(LISTE_4, JAUGE_4)
    controle_escalade_strict_v2(LISTE_4, JAUGE_4)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()
