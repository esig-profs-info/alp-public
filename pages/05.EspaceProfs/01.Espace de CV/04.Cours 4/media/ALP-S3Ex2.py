# Série 3, Exercice 2
# Clément Vogt

# Constantes
RABAIS = 50 / 100
NB_JOURS_POUR_RETOUR = 7

# Procédures et fonctions
def donnees_entree():
    no_client = int(input("Entrez un numéro de client"))
    prix1 = int(input("Entrez le prix du premier article"))
    prix2 = int(input("Entrez le prix du deuxième article"))
    return no_client, prix1, prix2

def afficher_entete(no_client):
   print("Magasin PTIPRI")
   print("Achats effectués par le client n°", no_client,sep="")
   
def prix_a_payer(prix1,prix2):
    if prix1 > prix2:
        return prix1 + prix2 * RABAIS
    else:
        return prix1 * RABAIS + prix2
    
def afficher_achat(prix1,prix2,prix):
    print("Prix des 2 articles achetés : ", prix1, " et ", prix2, " ==> Prix à payer: ",prix,sep="")

def afficher_conditions():
    print("Merci pour votre achat.")
    print("En cas de problème, vous pouvez nous les retourner dans les ",NB_JOURS_POUR_RETOUR," jours.",sep="")

# Procédure main()
def main():
    no_client, prix1, prix2 = donnees_entree() 
    afficher_entete(no_client)
    prix_a_paye = prix_a_payer(prix1,prix2)
    afficher_achat(prix1,prix2,prix_a_paye)
    afficher_conditions()

# Appel de la procédure main()
if __name__ == "__main__":
    main()