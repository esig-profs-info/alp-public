---
template: item
---

Suite avec la console python

===

===

# Exploration 02

[Formulaire d'évaluation formative](https://forms.gle/pUphbvzR7LWdXUTC9)

Que sera l'output de python pour chacune des commandes ci-dessous? Expliquez et argumentez.

Gardez à l'esprit que l'important n'est pas de savoir exactement que sera affiché, mais de *comprendre pourquoi*.

```python
>>> hello
```

```python
>>> "hello"
```

```python
>>> 'hello'
```

```python
>>> "hello
```

```python
>>> "hello" + ' world'
```

```python
>>> "hello'
```

```python
>>> 3 + 4
```

```python
>>> '3 + 4'
```

```python
>>> '3' + 4
```
