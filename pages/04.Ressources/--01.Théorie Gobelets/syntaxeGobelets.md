---
template: item
title: Syntaxe officielle pour les gobelets
body_classes: syntaxeGobelets
published: true
---

La syntaxe officielle du langage "gobelets"

===

## Syntaxe de base
| Instruction | Description |
| :---------- | ----------- |
| ↑ | Prendre un gobelet et remonter assez haut |
| ↓ | Déposer le gobelet tenu puis remonter assez haut |
| → | Déplacer le bras d'un pas (largeur d'un demi gobelet) vers la droite |
| ← | Déplacer le bras d'un pas (largeur d'un demi gobelet) vers la gauche | 
| ↺ | Retourner le gobelet |
<br/><br/>
NB: Ci-dessous, les mots en _bleu et italique_ représentent des éléments syntaxiques qui peuvent changer
##Structure conditionnelle (si)

SI _couleur_ ALORS <br/> 
 &emsp;_instructions_<br/>
SINON <br/> 
 &emsp;_instructions_<br/>
FSI 


##Structure répétitive (boucle)

REPETER _N_ x<br/> 
 &emsp;_instuctions_<br/>
FREP

#### ou
![boucle fléchée](boucle.png?cropResize=200,200)

#### ou 
(version qui répète des instructions pour tous les gobelets de la pile de départ)<br/>

TANTQUE non vide<br/> 
 &emsp;_instuctions_<br/>
FTANTQUE

##Fonctions sans paramètre

DEF _nomDeLafonction_( )<br/>
 &emsp;_instuctions_<br/>
FDEF


##Fonctions avec paramètre(s)

DEF _nomDeLafonction_(_nomParam1_, _nomParam2_, _..._)<br/>
 &emsp;_instuctions utilisant les paramètres nomParam1, nomParam2, ..._<br/>
FDEF

