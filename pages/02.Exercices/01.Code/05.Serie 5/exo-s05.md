---
template: item
published: true
---
<style>
img
{
    display:block;
    float:none;
    margin-left:auto;
    margin-right:auto;
    width:40%;
}
</style>

# Série 5
## Objectifs
Comprendre les concepts liés à la **boucle `for`** et les mettre en œuvre.

Identifier les différentes étapes importantes dans la conception d'une boucle.

===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S5Ex1** à **ALP-S5Ex4** et se trouveront dans un dossier nommé **ALP-S5**.

## Énoncé

### Exercice T1
Dessiner un carré de taille fixe en évitant les duplications de code à l'aide de la boucle `for`.

### Exercice T2
Reprendre l'exercice T2 de la série 2 (dessin de triangle, carré ou pentagone) en minimisant les lignes de code dupliquées à l'aide de la boucle `for`.

### Exercice T3
Reproduisez la figure ci-dessous à l'aide de la boucle `for`.
![Croix bleue](media/CroixBleue.jpg)
>### <i class="fa fa-info-circle"></i> Indications techniques
>Pour dessiner en couleur, utilisez la fonction (procédure) `color()` qui prend en paramètre le **nom** de la couleur en anglais (ici `'blue'`).  
> Pour dessiner un point, utilisez la fonction (procédure) `dot()` qui prend en paramètre le diamètre du point (ici `20` et `10` pour le point central).

### Exercice T4
Après avoir demandé un (petit) nombre entier à l'utilisateur, dessinez une figure comme celle ci-dessous à l'aide de la boucle `for`. La figure correspond à la saisie du nombre 4.
![Zigzag avec nombre de traits pair](media/ZigZagPair.jpg)
>### <i class="fa fa-info-circle"></i> Indications techniques
>Pour que la tortue démarre plus à gauche (ce qui évitera de redimensionner la fenêtre du graphique), utilisez le code qui suit:
>```python
>hideturtle() #pour éviter de voir la tortue bouger
>penup() #pour éviter un trait non demandé
>setpos(-400, 0) #pour démarrer plus à gauche
>pendown() #pour réactiver le dessin de la tortue
>showturtle() #pour afficher la tortue elle-même

Vous pouvez faire évoluer votre programme en demandant à l'utilisateur le nombre de **traits** plutôt que de "pics" (double trait). Voici ce que donnerait la saisie de 7.
![Zigzag avec nombre de traits impair](media/ZigZagImpair.jpg)


### Exercice 1
Afficher tous les nombres entiers compris entre 0 et 10

### Exercice 2
Afficher tous les nombres entiers compris entre N et M.

### Exercice 3
Afficher le *livret*, ou table de multiplication, de 13 (jusqu'à "fois 12"). Le résultat doit s'afficher sous la forme suivante:<br/>

    Livret de 13
    1 x 13 = 13
    2 x 13 = 26
    ...
    12 x 13 = 156

### Exercice 4
Afficher les k premiers termes du livret de n sous la forme suivante (k=12, n=7) :<br/>

    Livret de 7
    1 x 7 = 7
    2 x 7 = 14
    3 x 7 = 21
    4 x 7 = 28
    5 x 7 = 35
    6 x 7 = 42
    7 x 7 = 49
    8 x 7 = 56
    9 x 7 = 63
    10 x 7 = 70
    11 x 7 = 77
    12 x 7 = 84
    
[//]: # (### Exercice T5)


[//]: # (Reprendre l'exercice précédent en y ajoutant la possibilité de répéter la figure dessinée un certain nombre de fois \(saisi par l'utilisateur\). )
