---
title: Valeurs de retour
---

# Les valeurs de retour

Pour l'instant, tous les fonctions que nous avons écrit ont communiqué le fruit de leur travail au monde extérieur (à la fonction) soit en faisant bouger la tortue sur un canevas, soit en affichant du texte à la console avec `print`.

Par exemple, la fonction suivante indique si un nombre est premier en écrivant un message à la console:

```python
def est_premier(nombre):
    nombre_test = 2
    compteur = 0
    while nombre_test < nombre:
        if isInteger(nombre / nombre_test):
            compteur += 1
        nombre_test += 1
    if compteur == 0:
        print(nombre, "est premier")
    else:
        print(nombre, "n'est pas premier")
```
Un humain peut donc lire à la console si le nombre est premier ou pas, ce qui est très bien. Par contre, comment utiliser le résultat de cette fonction depuis le reste de notre programme?

Imaginons par exemple que nous voulons faire un programme qui multiplie par lui-même un nombre saisi par l'utilisateur, mais uniquement si ce dernier est premier. Il serait pratique de pouvoir écrire notre petit programme comme suit:

```python
n = input("Entrez un nombre premier à multiplier par lui-même")
if est_premier(n):
    print(n*n)
else:
    print("Désolé, ce nombre n'est pas premier")
```

Avec la version ci-dessus de la fonction `est_premier` ceci n'est pas malheureusement pas possible.

Pour qu'une telle chose, fort pratique, devienne possible, nous devons modifier notre fonction comme suit:

```python
def est_premier(nombre):
    nombre_test = 2
    compteur = 0
    while nombre_test < nombre:
        if isInteger(nombre / nombre_test):
            compteur += 1
        nombre_test += 1
    if compteur == 0:
        return True
    else:
        return False
```

---
**À retenir!**

Le mot réservé `return` indique que la fonction doit arrêter son traitement _avec effet immédiat_ et retourner la valeur qui suit.

---

Dans l'exemple, les valeurs sont `True` ou `False`, mais il est bien entendu possible d'utiliser d'autre types de valeurs (`return 1` par exemple, ou `return [1, 2, 3]`).

## Une analogie

Nous pouvons considérer chaque fonction comme une machine avec des entrées (les paramètres) et une sortie (la valeur de retour).

![Illustration machine input-output](raspberry_pi_io_machine.png)

Par exemple pour la fonction `sqrt` qui calcule la racine carrée d'un nombre, l'entrée est un nombre quelconque et la sortie est la racine carrée de ce nombre.

Il est possible de réutiliser la valeur de retour d'une fonction comme entrée pour une autre fonction:

![Illustration machine input-output](raspberry_pi_machines_connected.png)

Si nous écrivons par exemple `print(sqrt(2))`, la valeur de retour de l'appel de la fonction `sqrt` est utilisée comme paramètre effectif pour l'appel de la fonction `print`. Sans utiliser des valeurs de retour, ceci ne serait pas possible.

## Comment récupérer la valeur retournée?

Nous avons vu qu'il est possible de réutiliser directement la valeur retournée comme paramètre pour une autre fonction. Nous pouvons également stocker la valeur dans une variable. Par exemple:

```python
racine = sqrt(4)
a = racine*2
...
```

## Sources des images
https://learn.adafruit.com/assets/23712

https://learn.adafruit.com/assets/23238