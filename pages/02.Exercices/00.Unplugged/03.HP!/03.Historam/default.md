# Lire du code HP!

Créez l'historam pour le code hp! ci-dessous. La valeur lue en entrée est 1.

Sur la dernière ligne de l'historam vous devez indiquer l'état final de toutes les zones de mémoire utilisées et de l'accumulateur.

[Historam vide](media/bit.ly_histo-ram-a4.pdf)

![Code HP!](media/code_hp.jpg)


