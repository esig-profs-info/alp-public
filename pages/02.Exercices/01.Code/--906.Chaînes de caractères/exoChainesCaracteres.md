---
template: item
published: true
---

Série d'exercices sur les chaînes de caractères

===

<!--todo: lister les pré-requis-->
## Exercice 1
Ecrire une fonction permettant d'afficher le nombre d'occurences d'un caractère dans une chaîne.<br/>
Par exemple, le caractère `a` apparaît `5` fois dans la chaîne `"abracadabra"`.
Faites entrer la chaîne et le caractère à compter par l'utilisateur puis affichez le résultat comme suit:

```
=> la lettre 'a' se trouve 5 fois dans 'abracadabra'
```

## Exercice 2
Ecrire une fonction `compteEtAfficheTypesDeCar(s)` qui compte et affiche le nombre de : minuscules, majuscules, chiffres et symboles d'une chaîne `s` passée en paramètre.<br/>

Par exemple:
```
compteEtAfficheTypesDeCar("P@#yn26at^&i5ve") → La chaîne contient 7 minuscules, 1 majuscule, 3 chiffres et 4 symboles.
```

**Inforations:**<br/>
Vous pouvez déclarer les chaines suivantes:
```
minuscules = "abcdefghijklmnopqrstuvwxyz"
majsucules = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
num = "0123456789"
```
et voir si le charactère que vous testez s'y trouve.

Sinon, les fonctions islower() ou isupper() qui s'utilisent sous la forme ```"c".islower()``` peuvent être utiles... à vous de choisir!


## Exercice 3
Ecrire une fonction `troisCarDuMilieu(s)` qui retourne une chaîne composée des trois caractères du milieu d'une chaîne `s` passée en paramètre. La chaîne passée en paramètre doit être impaire et composée d'au moins 7 caractères, sinon ce n'est pas le cas, la fonction retourne une chaîne vide `""`.<br/>

**info:** le slicing peut être utile (mais pas indispensable)<br/>

Par exemple:
```
troisCarDuMilieu("JhonDipPeta") → "Dip"
troisCarDuMilieu("Jasonay") → "son"
```

## Exercice 4
Ecrire une fonction `ajouteAuMilieu(s1,s2)` qui retroune une chaîne qui ajoute `s2` au milieu de `s1`. Si la chaîne `s1` est impaire, garder la plus grande partie au début du mot.<br/>

Par exemple:
```
ajouteAuMilieu("python", "COUCOU") → "pytCOUCOUhon"
ajouteAuMilieu("sandbox", "COUCOU") → "sandCOUCOUbox"
```


## Exercice 5
Ecrire une fonction `rangeMajDabord(s)` qui retourne une chaîne de caractère qui aura déplacé les caractères en majuscule de la chaine `s` passée en paramètre au début (de la chaine retournée).<br/>

**info:** Pour savoir si un caractère `char` est une minuscule on peut utiliser la fonction `char.isLower()`. Par ailleurs, la méthode conseillée pour cet exercice consiste à créer une chaîne `maj` qui contient les majuscules dans l'ordre, puis une chaine `min` qui contient les minuscules puis de les concaténer l'une à l'autre.<br/>

Par exemple:
```
rangeMajDabord("PyThOn") → "PTOyhn"
```

## Exercice 6
Ecrire une fonction permettant d'afficher la chaîne de caractères résultant de l'insertion d'un caractère à une position donnée dans chaîne. (vérifiez que la position est comprise entre 1 et la taille de la chaîne)

Par exemple, l'insertion du caractère `'x'` à la cinquième position de la chaîne `"abracadabra"` donnera la chaîne `"abraxcadabra"`.

Faites entrer la chaîne, le `caractère à insérer` ainsi que la `position` par l'utilisateur puis affichez le résultat comme suit:

```
insertion du caractère 'x' dans la chaîne 'abracadabra' à la position 5:
=> abraxcadabra
```
ou
```
insertion du caractère 'a' dans la chaîne 'potatoes' à la position 1:
=> apotatoes
```
ou
```
insertion du caractère 'a' dans la chaîne 'potatoes' à la position 0:
=> impossible d'insérer un caractère en position 0
```
ou
```
insertion du caractère 'a' dans la chaîne 'potatoes' à la position 9:
=> impossible d'insérer un caractère en position 9
```

## Exercice 7
Ecrire un programme qui demande un mot à l'utilisateur, puis qui lui indique si ce mot est un palindrome (un mot que se lit dans les deux sens). Utilisez une fonction booléenne.

```
Quel mot voulez-vous tester? sugus
--> sugus est un palindrome

Quel mot voulez-vous tester? python
--> python n'est pas un palindrome
```

## Exercice 8
Exercice 3
Ecrire une fonction permettant d'afficher "en escalier", descendant (paramètre à 0) ou montant (paramètre à 1), une chaîne de caractères.

Par exemple: la chaîne `"abracadabra"`, en `descendant`, donne:
```
a
 b
  r
   a
    c
     a
      d
       a
        b
         r
          a
```
Et en `montant`...
```
          a
         b
        r
       a
      c
     a
    d
   a
  b
 r
a
```

## Exercice 9
Ecrire un programme qui demande à l'utilisateur un mot puis qui lui indique s'il s'agit d'une adresse email valide. Sachant que pour être valide (ici, pour simplifier...): Une adresse email doit commencer par x caractères (x >=1) qui précédent l'unique caractère `@`, puis y caractères (y>=1) qui suivent le `@` et qui précédent le `.`, puis z caractères (z>=1) après l'unique caractère `.`. Les x,y et z caractères, doivent être des caractères acceptés, vous avez de la chance, nous vous fournissons une liste (non-exhaustive) des caractères interdits: `caracteresIntedits`:<br/>

NB: on considère ici qu'une adresse qui a plusieurs `.` est invalide (ce qui n'est pas le cas dans la réalité...)<br/>

Une méthode pourrait être de trouver à quelles positions (index) se trouvent le `@` et le `.` puis de voir si le nombre de caractères qui se trouvent entre ces positions est suffisant. En outre, il faudra tester que chaque caractère de l'adresse n'est pas interdit.

```
caracteresInterdits = ['!','#','$','%','&','*','+',',','-','/','=','?']
```
Le programme affichera:
```
Entrez une adresse: bob@gmail.com
bob@gmail.com est une adresse valide

Entrez une adresse: bob#gmail.com
bob#gmail.com n'est pas valide

Entrez une adresse: bob@gmailcom
bob@gmailcom n'est pas valide

```