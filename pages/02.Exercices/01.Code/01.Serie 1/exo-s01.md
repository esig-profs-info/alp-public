---
template: item
published: true
---

# Série 1
## Objectifs
Comprendre les concepts d'**expression**, de **séquence** et d'**alternative**. Les mettre en œuvre dans un programme **Python**.

À partir d'un énoncé de problème à résoudre, identifier les **données**, les **résultats** ainsi que la **relation** qui les unit.


===

## Notions pour la série
[Tortue: Liste des actions](https://docs.python.org/fr/3/library/turtle.html#turtle-methods)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S1Ex1** à **ALP-S1Ex6** et se trouveront dans un dossier nommé **ALP-S1**.

## Énoncé

### Exercice T1
Dessiner un rectangle de taille 100x150.

### Exercice T2
Dessiner un losange selon le croquis ci-dessous:

![](./media/losange.png)

### Exercice 1
À partir de la donnée d'un **nombre entier**, calculer et afficher son
**double** et sa **moitié**.

<u>Exemples d’exécution :</u>

![](./media/image1.png)

### Exercice T3
Dessiner un rectangle dont la longueur est saisie par l'utilisateur. La largeur est la moitié de la longueur.

### Exercice 2
À partir de la donnée de la **base** et de la **hauteur** d'un
triangle, calculer et afficher son **aire**.

<u>Exemples d’exécution :</u>

![](./media/image2.png)

### Exercice 3
À partir de la donnée de la **largeur**, de la **longueur** et de la
**hauteur** (en mètres) d'une pièce d'habitation parallélépipédique rectangle 
(un rectangle en 3D, comme une pièce standard),
calculer et afficher son **volume**, sa **surface au sol** et la
**surface totale (aire)**.

<u>Exemples d’exécution :</u>

![](./media/image3.png)

### Exercice 4
À partir de la donnée d'un **nombre entier**, afficher l'un des deux
textes ci‑dessous en fonction de sa valeur :

> * "Le nombre est négatif"
> * "Le nombre est positif ou nul"

<u>Exemples d’exécution :</u>

![](./media/image4.png)
![](./media/image5.png)

### Exercice T4
Écrire un programme qui laisse l'utilisateur choisir une forme à dessiner. 

Soit un carré, soit une simple ligne droite.

La longueur des côtés, respectivement de la ligne, est également saisi par l'utilisateur.

### Exercice 5
Une entreprise de vente de pièces détachées désire automatiser le calcul
des factures de ses clients. La somme à payer varie en fonction du
nombre de pièces commandées de la manière suivante :

> - Une commande de 100 pièces ou moins ne bénéficie d'aucun rabais.
>
> - Une commande de plus de 100 pièces bénéficie d'un rabais de 10%.

Le programme que vous devez écrire calcule le prix total à payer en
fonction du **prix unitaire** d'une pièce et du **nombre de pièces**
commandées. Il affiche le **prix unitaire** d'une pièce, le **nombre de
pièces** commandées, le **prix avant rabais**, le **montant du rabais**
ainsi que le **prix à payer**.

<u>Exemples d’exécution :</u>

![](./media/image6.png)
![](./media/image7.png)

### Exercice 6
L'entreprise dans laquelle vous travaillez vous demande de réaliser un
programme qui calcule le salaire brut d'un employé à partir de son
**salaire de base** et de l'**année d'engagement** de l'employé. Les
employés bénéficient d'une prime qui se rajoute au salaire de base en
fonction de l'ancienneté (nombre d'années pendant lesquelles
l'employé a travaillé dans l'entreprise). Cette prime est calculée de
la manière suivante :

> - Les employés engagés depuis moins de 5 ans ne reçoivent aucune
> prime.
>
> - Les employés engagés depuis 5 ans et plus reçoivent une prime
> correspondant à 3% de leur salaire de base.

Le programme que vous devez écrire calcule la prime et affiche le
**salaire de base**, le **nombre d'années** pendant lesquelles
l'employé a travaillé dans l'entreprise, le **montant de la prime**
ainsi que le **salaire brut**.

<u>Exemples d’exécution (avec 2020 comme année courante):</u>

![](./media/image8.png)
![](./media/image9.png)