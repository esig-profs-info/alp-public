---
template: item
published: false
---

Exercices unplugged pour comprendre la syntaxe de base de python

===

###Exercice 1
#### Matériel nécessaire:
Une feuille de papier, un crayon... puis, une fois terminé, la console python pour corriger
#### Enoncé
Soient les déclarations suivantes:
```python
n=5
p=9
s="3.0"
```
Donnez les valeurs affectées et le type des différentes variables concernées par chacune des instructions suivantes:
```python
q = n > p                   #1
q = n == p                  #2
q = n == p or p > 8.999     #3
x = p / n                   #4
x = p // n                  #5
x = (p + 0.5) / n           #6
y = int(p + 0.5) // n       #7
y = int(s)                  #8
y = p % n + int(p > n)      #9
y = float(s)                #10
y = int(float(s))           #11
```

###Exercice 2
#### Matériel nécessaire:
Une feuille de papier, un crayon... puis, *une fois terminé!*, Thonny pour corriger
#### Enoncé
Quels résultat fournit le programme suivant:
```python
i=0
n=i + 1
print("A: i = " + str(i) + " n = " + str(n) + "\n")

i=10
n += i
print("B: i = " + str(i) + " n = " + str(n) + "\n")

i = 20
j = 5
m = 10
m += j * i
n *= j * i

print("C: m = " + str(m) + " n = " + str(n) + "\n")
```

###Exercice 3
#### Matériel nécessaire:
Une feuille de papier, un crayon... puis, *une fois terminé!*, Thonny pour corriger
#### Enoncé
Soit le programme suivant:
```python
n = int(input("entrez un nombre: "))
if n == 0:
    print("Nul")
elif n > 1 and n <= 5:
    print("Petit")
elif n < 10:
    print("Moyen")
else:
    print("Grand")
```
Quel sera le résultat pour les valeurs saisies suivantes:
1.  10
2.  0
3.  1000
4.  4
5.  -4
6.  -1000

==> Corrigez le programme dans Thonny pour qu'il gère correctement les nombres négatifs