from __future__ import print_function

def formaterReel(nombreReel):
    return '{:.2f}'.format(nombreReel)

def poidsApparent(poids, poidsCombi, volume, nbPoids500, nbPoidsKg):
    poidsReel = poids + poidsCombi + nbPoids500*0.5 + nbPoidsKg
    #pousseeArchimede = volume
    return round(poidsReel - volume, 3)

def afficheSiCoule(poidsApparent):
    if poidsApparent < 0:
        print('Il faut rajouter des poids car la plongeuse flottera et ne pourra pas plonger')
    elif poidsApparent > 0:
        print('Parfait! La plongeuse va pouvoir profiter de sa plongée')
    else: # == 0
        print('Il faut rajouter des poids car la plongeuse sera en équilibre et ne pourra pas plonger')

def couleTool():
    print('''
----------------------------------------------------
Association Lémanique de Plongée
Bienvenue au CouleTool !
----------------------------------------------------
''')
    poids = float(input('Poids de la plongeuse [kg]: '))
    poidsCombinais = float(input('Poids de sa combinaison [g] : '))
    volume = float(input('Volume de la plongeuse [l]: '))
    nbPoids500 = int(input('Nombre de poids 500g : '))
    nbPoids1kg = int(input('Nombre de poids 1kg : '))
    
    pa = poidsApparent(poids, poidsCombinais, volume, nbPoids500, nbPoids1kg)
    print('Poids apparent',pa)
    afficheSiCoule(pa)


def exercice1():
    # écrire ici le code de l'exercice 1
    couleTool()
    
def exercice2(jeuDonnees):
    
    if jeuDonnees == 1: 
        # Données de test 1        
        noms = ['Anne', 'Carole', 'Mike']
        poids = [72.5, 72.5, 72.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [1, 1, 1]
        nbPoids1kg = [0, 1, 2]
    elif jeuDonnees == 2:
        # Données de test 2
        noms = ['Magda', 'John', 'Céline', 'Oleko']
        poids = [72.5, 72.5, 73.5, 60]
        poidsCombi = [150, 100, 100, 200]
        volume = [76.4, 73.4, 75.1, 62]
        nbPoids500g = [2, 0, 2, 1]
        nbPoids1kg = [3, 1, 1, 2]
    elif jeuDonnees == 3: 
        # Données de test 3        
        noms = ['Anne', 'Carole', 'Mike']
        poids = [72.5, 72.5, 72.5]
        poidsCombi = [150, 100, 100]
        volume = [76.4, 73.4, 75.1]
        nbPoids500g = [1, 1, 2]
        nbPoids1kg = [3, 1, 2]
    else:
        print('Jeu de données inexistant: ', jeuDonnees)
    
    print('#### Jeu de données no.', jeuDonnees, '####')
    
    # écrire ici le code de l'exercice 2

if __name__ == '__main__':
    
    
    exercice = 1 # Indique quel exercice vous souhaitez tester.
    if 1 == exercice:
        exercice1()
    elif 2 == exercice:
        exercice2(1)
    else:
        print('Exercice inexistant:', exercice)

 
