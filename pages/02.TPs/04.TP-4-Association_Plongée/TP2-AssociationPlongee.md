---
template: item
---

# TP2 - Association Lémanique de Plongée

===

[Énoncé](./TP_Association_Plongee.doc)

[Fichier à compléter - Partie A](./TPAssociationPlongee-A.py)

[Fichier à compléter - Partie B](./TPAssociationPlongee-B.py)