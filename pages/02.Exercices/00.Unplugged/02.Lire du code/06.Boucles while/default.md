---
template: item
published: true
---

Evaluez le résultat de chacun des 6 extraits de code suivants:

![Enoncé au tableau](evaluationCode_while.JPG?cropResize=900,900)

Une fois terminé, saisissez les dans Thonny et observez l'exécution de ces codes avec le debugger.
