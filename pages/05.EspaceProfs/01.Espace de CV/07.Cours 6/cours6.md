---
template: item
published: true
---

# Cours 6

===

* **Problème de la validation des données**
    * Si des données erronées sont saisies, les résultats en seront faussés (jusqu'à présent, on supposait toujours que les données fournies par l'utilisateur étaient valides).
    * PBCK (**P**roblem **B**etween **C**hair and **K**eyboard).
    * Il faut donc refuser l'exécution si les données sont invalides et, éventuellement, produire un/des messages d'erreur circonstanciés.
    * <u>Solution:</u> une fonction à valeur booléenne (bool)  qui:
        * Vérifie la validité des données saisies
        * Affiche le/les messages d'erreur
    * <u>Principalement deux méthodes:</u>
        * Validation en cascade
        * Validation en séquence
        * Voir [ALP-S3Ex3-Val](./media/ALP-S3Ex3-Val.py) développé en classe qui illustre les différentes variantes, leurs avantages et leurs inconvénients.

**<u>Exercices :</u>** [Série 6](/exercices/code/serie%206)