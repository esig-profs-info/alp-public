Résoudre à l'aide du système HP! puis écrivez le code correspondant.

### Ex01: Photocopieur
Recopie deux valeurs de l’entrée sur la sortie:

|In |Out|
|---|---|
|1,2,3|1,2|

### Ex02:
Recopie deux valeurs sur la sortie, mais en changeant
l’ordre.

|In |Out|
|---|---|
|1,2,3|2,1|

### Ex03:
Lire deux valeurs. Calcule leur somme et affiche le
résultat sur la sortie

|In |Out|
|---|---|
|3,4|7|

### Ex04 - Doubleur:
Multiplie par 2 une valeur lu sur l’entrée et affiche le
résultat sur la sortie.

|In |Out|
|---|---|
|10|20|

### Ex05 - Tripleur:
Multiplie par 3 une valeur lu sur l’entrée et affiche le
résultat sur la sortie.

|In |Out|
|---|---|
|15|45|

### Ex06 - Quadripleur:
Multiplie par 4 une valeur lu sur l’entrée et affiche le
résultat sur la sortie.

|In |Out|
|---|---|
|3|12|
