# Introduction au concept de liste
# Clément Vogt

# Procédure main()
def main():
    num = [1,2,3,4,5]
    
    # On veut afficher les 5 données: boucle qui parcourt la liste
    for element in num:
        print(element,end=' ')

# Appel de la procédure main()
if __name__ == "__main__":
    main()