# Série 13, Exercice 4
# Clément Vogt

# Constantes
MAJUSCULES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Procédures et fonctions
def init_dico():
    dico_maj = {}
    for element in MAJUSCULES:
        dico_maj[element] = 0  
    return dico_maj

def preparer_donnees(chaine):
    dico_maj = init_dico()
    for element in chaine:
        if element in dico_maj:
            dico_maj[element]+=1
    return dico_maj

def afficher_donnees(dico_maj):
     for k in dico_maj:
        espace = ""
        if dico_maj[k] > 0:
            espace = " "
        print(k,": ",dico_maj[k]*"*",espace,"(",dico_maj[k],")",sep="")

def afficher_histogramme(chaine):
    print("Histogramme en étoiles des lettres majuscules:")
    dico_maj = preparer_donnees(chaine)
    afficher_donnees(dico_maj)
    
# Procédure main()
def main():
    chaine = input("Saisissez une chaîne de caractères:")
    afficher_histogramme(chaine)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()