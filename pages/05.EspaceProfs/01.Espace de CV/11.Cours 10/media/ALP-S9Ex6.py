# Série 9, Exercice 6
# Clément Vogt

# Constantes
NOTES = [3, 6, 5.5, 4.5, 2.5, 4, 5, 4, 3, 4, 2.5, 4.5, 5, 5, 4, 3]
NOTE_MIN = 1
NOTE_MAX = 6
NOTE_MOYENNE = 4
NB_DECIMALE = 1

# Procédures et fonctions
def notes_valides(liste):
    for element in liste:
        if element < NOTE_MIN or element > NOTE_MAX:
            return False
    return True

# Cette version est fonctionnelle et correcte, mais moins performante car on fait toujours la boucle en entier
# alors qu'on pourrait s'arrêter dès que le résultat est connu. Or, on connaît le résultat final à partir du moment
# où on a rencontré une valeur invalide. Donc, ici, on continue quand même la boucle alors que ce n'est pas
# nécessaire.
def notes_valides_moins_bien(liste):
    resultat = True
    for element in liste:
        if element < NOTE_MIN or element > NOTE_MAX:
            resultat = False
    return resultat

# Dans les deux versions précédentes, on doit mettre la condition d'invalidité, ici, c'est la condition de validité.
def notes_valides_while(liste):
    k = 0
    while k < len(liste) and (NOTE_MIN <= liste[k] and liste[k] <= NOTE_MAX):
        k += 1
    # Ici: la boucle s'est arrêtée soit parce qu'on est arrivé à la fin du tableau, soit parce qu'il y a eu une note invalide.
    # Il faut refaire l'un des deux tests pour savoir lequel a arrêté la boucle. On doit obligatoirement refaire le 1er,
    # car si le premier avait la valeur False, le deuxième poserait un problème de dépassement (si k = len(liste), 
    # liste[k] est invalide, car k désigne un indice inexistant). *)     
    return k == len(liste)

def moyenne(liste):
    somme = 0
    for element in liste:
        somme = somme + element
    return round(somme / len(liste),NB_DECIMALE)

def nb_insuffisants(liste):
    nb_insuf = 0
    for element in liste:
        if element < NOTE_MOYENNE:
            nb_insuf = nb_insuf + 1
            
    return nb_insuf

def indice_meilleure_note(liste):
    ind_meilleur = 0                          # On part du principe que le meilleur est à l'indice 0
    for k in range(1,len(liste)):             # On peut donc commencer à comparer à partir du suivant (1)
        if liste[k] > liste[ind_meilleur]:    # dès qu'on trouve une meilleure note
            ind_meilleur = k                  # on conserve son indice dans ind_meilleur 
    return ind_meilleur                       # ind_meilleur contiendra donc bien l'indice de la meilleure note

# Procédure main()
def main():                
    if notes_valides(NOTES):
        print("Moyenne des notes : ",moyenne(NOTES),sep='')
        print(nb_insuffisants(NOTES)," note(s) en dessous de ",NOTE_MOYENNE,sep='')
        ind_meilleur = indice_meilleure_note(NOTES)
        print("Le meilleur élève se trouve en position ",ind_meilleur,sep='')
        print("Sa note est : ",NOTES[ind_meilleur],sep='')
    else:
        print("Les notes ne sont pas toutes valides !")

# Appel de la procédure main()
if __name__ == "__main__":
    main()