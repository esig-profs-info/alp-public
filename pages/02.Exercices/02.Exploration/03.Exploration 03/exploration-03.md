---
template: item
---

3 exercices pour continuer (individuellement maintenant?)

===

# Exploration 03

## Exercice 1
Répertoriez et traduisez un maximum d'erreurs python possible. Si vous n'en comprenez pas, listez-les sans les traduire, mais gardez le code qui a généré l'erreur: Consignez votre travail dans le document suivant (attention c'est un document partagé pour tout le monde, merci de ne pas effacer des lignes saisies par d'autres):
[lien vers le document partagé pour les erreurs python](https://drive.google.com/open?id=1iIyYLJImV2HUf3DfaJuc6WatgOe6cp_OuZRpbpisCz8&noprocess)

## Exercice 2
Basiquement, le rôle d'un programme est de recevoir des données en *entrée* pour les *traiter*, puis de *sortir* d'une façon ou d'une autre le résultat attendu par le programmeur. Trouvez 2 exemples illustrant l'input, le traitement et l'output. Faîtes un schéma.

## Exercice 3
Que sera l'output de python pour chacune des commandes ci-dessous? Expliquez et argumentez.
Gardez à l'esprit que l'important n'est pas de savoir exactement que sera affiché, mais de *comprendre pourquoi*.

```python
>>> 2 + 3 * 4
```

```python
>>> 2 + (3 * 4)
```

```python
>>> (2 + 3) * 4
```

```python
>>> (2 + 3 * 4
```

```python
>>> () 2 + 3 * 4
```

```python
>>> () + 2 + 3 * 4
```

```python
>>> "hello" - "lo"
```

```python
>>> 10 / 3
```

```python
>>> 10 // 3
```

```python
>>> 10 % 3
```

```python
>>> 10 / 3 + 2
```

```python
>>> 14 / 4 + 6 / 2
```

```python
>>> 14 / (4 + 6 / 2)
```

```python
>>> 10 / 5 % 3
```