# Cours 3 - Illustration
# Clément Vogt
print("\n---------------------------\nCours 3 - Illustration\n---------------------------")

# ---------------------------------------------------------------------------
# Alternative: Le formant de luxe - elif
# ---------------------------------------------------------------------------
print("\nLe formant de luxe if ... : ... elif ... : ... else : ...")

print("DEBUT")
mon_chiffre = 2
if mon_chiffre == 2:
    print("La valeur de mon_chiffre est égal à 2")
elif mon_chiffre >= 10:
    print("La valeur de mon_chiffre est supérieure ou égale à 10")
else:
    print("La valeur de mon_chiffre est inférieure à 10 et est différente de 2")
print("FIN")

# ---------------------------------------------------------------------------
# Importation d’un module
# ---------------------------------------------------------------------------
print("\nImportation d’un module")

import math

def main():
    print("Pi =",math.pi)
    
if __name__ == "__main__":
    main()