# Série 4, Exercice 1
# Clément Vogt

# Constantes
TYPE_PIANO_A_QUEUE = 2
NB_ETAGES_MAX = 10
PRIX_PAR_ETAGE_PIANO_DROIT = 60
PRIX_PAR_ETAGE_PIANO_A_QUEUE = 90
PRIX_PAR_KM = 6
PRIX_TOTAL_MIN = 200
PRIX_TOTAL_MAX = 2500

# Procédures et fonctions
def donnees_entree():
    type_piano = int(input("Entrez le type de piano"))
    nb_etages_descendus = int(input("Entrez le nombre d'étages à descendre"))
    nb_etages_montes = int(input("Entrez le nombre d'étages à remonter"))
    nb_km = int(input("Entrez le nombre de km à parcourir"))
    return type_piano, nb_etages_descendus, nb_etages_montes, nb_km

def prix_de_base(type_piano,nb_etages):
    if nb_etages > NB_ETAGES_MAX:
        nb_etages = NB_ETAGES_MAX
    
    if type_piano == TYPE_PIANO_A_QUEUE:
        prix = nb_etages * PRIX_PAR_ETAGE_PIANO_A_QUEUE
    else:
        prix = nb_etages * PRIX_PAR_ETAGE_PIANO_DROIT
    return prix

def prix_a_payer(prix_base,prix_transport):
    prix = prix_base + prix_transport
    
    if prix < PRIX_TOTAL_MIN:
        prix = PRIX_TOTAL_MIN
    elif prix > PRIX_TOTAL_MAX:
        prix = PRIX_TOTAL_MAX 
    return prix

def afficher_resultats(type_piano,nb_etages_descendus,nb_etages_montes,nb_km,prix_total,prix_transport):
    print("Prix total à payer pour un piano ",end='')
    if type_piano == TYPE_PIANO_A_QUEUE:
        print("à queue ",end='')
    print(": ",prix_total,".- (dont ",prix_transport,".- de transport pour les ",nb_km," km)",sep='')

# Procédure main()
def main():
    type_piano, nb_etages_descendus, nb_etages_montes, nb_km = donnees_entree()
    nb_total_etages = nb_etages_descendus + nb_etages_montes
    prix_base = prix_de_base(type_piano,nb_total_etages)
    prix_transport = PRIX_PAR_KM*nb_km
    prix_total = prix_a_payer(prix_base,prix_transport)      
    afficher_resultats(type_piano,nb_etages_descendus,nb_etages_montes,nb_km,prix_total,prix_transport)

# Appel de la procédure main()
if __name__ == "__main__":
    main()