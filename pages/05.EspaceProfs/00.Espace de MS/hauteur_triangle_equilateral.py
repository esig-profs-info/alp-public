from gturtle import *
import math

# version avec variables
a = 144
h = math.sqrt( a**2 - (a/2)**2 )
print h

# version sans variables
print math.sqrt(144**2 - (144/2)**2)

makeTurtle()
speed(-1)

# version avec variables
repeat 3:
    fd(a)
    rt(120)
fd(a/2)
rt(90)
fd(h)

# version sans variables
repeat 3:
    fd(144)
    rt(120)
fd(144/2)
rt(90)
fd(math.sqrt(144**2 - (144/2)**2))

hideTurtle()