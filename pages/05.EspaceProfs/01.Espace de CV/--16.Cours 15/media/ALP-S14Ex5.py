# Série 14, Exercice 5
# Clément Vogt

# Imports
import numpy as np

# Procédures et fonctions
def afficher_table_pythagore_une_ligne(nb_colonnes,num_lig):
    for num_col in range(nb_colonnes):
        print(num_lig*num_col,end="\t")
    print()
    
def afficher_table_pythagore(nb_lignes,nb_colonnes):
    print("--- Table de Pythagore ---")
    for num_lig in range(nb_lignes):
        afficher_table_pythagore_une_ligne(nb_colonnes,num_lig)
    print()

def creer_table_multiplication_une_ligne(num_lig,tab2D):
    nb_colonnes = tab2D.shape[1]
    for num_col in range(nb_colonnes):
        tab2D[num_lig,num_col] = num_lig * num_col

def cree_table_multiplication(nb_lignes,nb_colonnes):
    tab2D = np.zeros((nb_lignes,nb_colonnes),int)
    for num_lig in range(nb_lignes):
        creer_table_multiplication_une_ligne(num_lig,tab2D)
    return tab2D

def afficher_en_tete_colonnes(tab2D):
    nb_colonnes = tab2D.shape[1]
    print("",end="\t")
    for cpt in range(nb_colonnes):
        print(cpt,end="\t")
    print()

def afficher_table_multiplication_une_ligne(num_lig,tab2D):
    nb_colonnes = tab2D.shape[1]
    print(num_lig, end="\t")
    for num_col in range(nb_colonnes):
        print(tab2D[num_lig,num_col],end="\t")
    print()
        
def afficher_table_multiplication(tab2D):
    print("--- Table de multiplication avec en-têtes ---")
    nb_lignes = tab2D.shape[0]
    afficher_en_tete_colonnes(tab2D)
    for num_lig in range(nb_lignes):
        afficher_table_multiplication_une_ligne(num_lig,tab2D)

# Procédure main()
def main():
    afficher_table_pythagore(13,13)
    afficher_table_multiplication(cree_table_multiplication(13,13))

# Appel de la procédure main()
if __name__ == "__main__":
    main()
