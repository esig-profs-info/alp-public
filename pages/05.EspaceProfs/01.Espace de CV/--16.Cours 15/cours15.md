---
template: item
published: true
---

# Cours 15

===

* **Correction de la série 14**
    * Série 14, exercice 1 ([solution](./media/ALP-S14Ex1.py))
    * Série 14, exercice 2 (solutions: [v1](./media/ALP-S14Ex2v1.py) - [v2](./media/ALP-S14Ex2v2.py))
    * Série 14, exercice 3 ([solution](./media/ALP-S14Ex3.py))
    * Série 14, exercice 4 (solutions: [v1](./media/ALP-S14Ex4.py) - [v2](./media/ALP-S14Ex4v2.py))
    * Série 14, exercice 5 ([solution](./media/ALP-S14Ex5.py))
    * Série 14, exercice 6 ([solution](./media/ALP-S14Ex6.py))
        
**<u>Exercices :</u>** [TP Rattrapage](/tps/tp-8-rattrapage)