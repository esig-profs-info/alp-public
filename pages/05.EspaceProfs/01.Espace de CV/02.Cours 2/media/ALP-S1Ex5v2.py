# Série 1, Exercice 5
# Clément Vogt
# Version 2:  avec les paramètres du problème (rabais et nombre de pièces y donnant droit) définis comme constantes

NB_PIECES_LIMITE = 100;
TAUX_RABAIS = 0.1

prix_unit = float(input("Entrez un prix unitaire"))
nb_pieces = int(input("Entrez un nombre de pièces"))

prix_avant_rabais = nb_pieces * prix_unit

if nb_pieces <= NB_PIECES_LIMITE:
    montant_rabais = 0
else:
    montant_rabais = TAUX_RABAIS * prix_avant_rabais
    
prix_a_payer = prix_avant_rabais - montant_rabais

print("Pour " , nb_pieces, " pièces à ", prix_unit, ":", sep='')
print("  Prix avant rabais: ", prix_avant_rabais, sep='')
print("  Montant du rabais: ", montant_rabais, sep='')
print("  Prix à payer: ", prix_a_payer, sep='')