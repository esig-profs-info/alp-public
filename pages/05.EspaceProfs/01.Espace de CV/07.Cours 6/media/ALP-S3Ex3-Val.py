# Clément Vogt
# Série 3, Exercice 3, avec validation des données saisies par l'utilisateur:
#    - le prix unitaire doit être supérieur à 0 et inférieur ou égal à une valeur plausible, par exemple 1000.
#    - le nombre de pièces doit être supérieur  0 et inférieur ou égal à une valeur plausible, par exemple 300;
# Ce sont les spécifications données par le client de l'application qui déterminent les valeurs maximales ci-dessus. 

# Constantes
NB_PIECES_LIMITE = 100;
TAUX_RABAIS = 0.1

PRIX_UNITAIRE_MAX = 1000
NB_PIECES_MAX = 300

# Procédures et fonctions
'''
Méthode 1: validation en cascade:
   - On teste la validité de chaque variable; si elle est valide, on teste la suivante.
   - Lorsqu'on a testé toutes les variables, et qu'elles sont toutes valides, on peut retourner True;
   - À la fin de la fonction, on doit retourner False, car si on arrive tout en bas, il y a au moins une valeur erronée;
   - Défaut: dès qu'il y a une erreur, on retourne False, sans tester les autres valeurs. On ne sait pas non plus quelle est l'erreur!
'''
def donnees_valides_0(prix_unit,nb_pieces):
    if prix_unit > 0:
        if prix_unit <= PRIX_UNITAIRE_MAX:
            if nb_pieces > 0:
                if nb_pieces <= NB_PIECES_MAX:
                    return True
    return False

# L'opérateur and (et logique) permet de simplifier le code écrit; l'effet est par contre absolument identique 
def donnees_valides_1(prix_unit,nb_pieces):
    if prix_unit > 0 and prix_unit <= PRIX_UNITAIRE_MAX and nb_pieces > 0 and nb_pieces <= NB_PIECES_MAX:
        return True
    return False

# Encore identique: on retourne directement la valeur de l'évaluation de la condition
def donnees_valides_2(prix_unit,nb_pieces):
    return prix_unit > 0 and prix_unit <= PRIX_UNITAIRE_MAX and nb_pieces > 0 and nb_pieces <= NB_PIECES_MAX

# Si maintenant on veut un message d'erreur:
# Inconvénient: seul le message de la première erreur rencontrée est affiché.
def donnees_valides_3(prix_unit,nb_pieces):
    if prix_unit > 0:
        if prix_unit <= PRIX_UNITAIRE_MAX:
            if nb_pieces > 0:
                if nb_pieces <= NB_PIECES_MAX:
                    return True
                else:
                    print("Le nombre de pièces est trop grand. Maximum: ",NB_PIECES_MAX,sep='')
            else:
                print("Le nombre de pièces doit être positif.")
        else:
            print("Le prix unitaire est trop grand. Maximum: ",PRIX_UNITAIRE_MAX,sep='')
    else:
        print("Le prix unitaire doit être positif.")    
    return False

# Si maintenant on veut un message d'erreur avec donnees_valides_1()
# Inconvénient: on ne sait pas de quelle données il s'agit.
def donnees_valides_4(prix_unit,nb_pieces):
    if prix_unit > 0 and prix_unit <= PRIX_UNITAIRE_MAX and nb_pieces > 0 and nb_pieces <= NB_PIECES_MAX:
        return True
    print("Vos données sont invalides.")
    return False

# On ne peut pas transformer donnees_valides_2() pour produire un message d'erreur, même général..

'''
Méthode 2: validation en séquence:
    - Objectif: produire un message pour chaque erreur rencontrée.
    - On a une variable est_valide qui contient le résultat de l'évaluation de la validité
    - Initialement, on considère que les données sont valides (on lui affecte la valeur True)
    - Si on rencontre une donnée invalide, on affiche le message correspondant et note ce fait en affectant la valeur False à est_valide
    - Finalement on retourne la valeur de est_valide
    Donc, s'il y a plusieurs erreurs de saisie, elles sont toutes signalées.
    Attention: ici on teste les conditions d'invalidité.
'''
def donnees_valides(prix_unit,nb_pieces):
    est_valide = True
    
    if prix_unit <= 0:
        print("Le prix unitaire doit être positif.")
        est_valide = False
        
    if prix_unit > PRIX_UNITAIRE_MAX:
        print("Le prix unitaire est trop grand. Maximum: ",PRIX_UNITAIRE_MAX,sep='')
        est_valide = False
        
    if nb_pieces <= 0:
        print("Le nombre de pièces doit être positif.")
        est_valide = False
    
    if nb_pieces > NB_PIECES_MAX:
        print("Le nombre de pièces est trop grand. Maximum: ",NB_PIECES_MAX,sep='')
        est_valide = False
    
    return est_valide
    
def calcul_rabais(prix_avant_rabais,nb_pieces):
    if nb_pieces <= NB_PIECES_LIMITE:
        montant_rabais = 0
    else:
        montant_rabais = TAUX_RABAIS * prix_avant_rabais  
    return montant_rabais     
    
def afficher_prix_a_payer(nb_pieces,prix_unit,prix_avant_rabais,montant_rabais,prix_a_payer):
    print("Pour " , nb_pieces, " pièces à ", prix_unit, ":", sep='')
    print("  Prix avant rabais: ", prix_avant_rabais, sep='')
    print("  Montant du rabais: ", montant_rabais, sep='')
    print("  Prix à payer: ", prix_a_payer, sep='')    

# Procédure main()
def main():
    prix_unit = float(input("Entrez un prix unitaire"))
    nb_pieces = int(input("Entrez un nombre de pièces"))
    
    if donnees_valides(prix_unit,nb_pieces): # Si les données sont valides, on effectue les traitements.
        prix_avant_rabais = nb_pieces * prix_unit
        montant_rabais = calcul_rabais(prix_avant_rabais,nb_pieces)
        prix_a_payer = prix_avant_rabais - montant_rabais
        afficher_prix_a_payer(nb_pieces,prix_unit,prix_avant_rabais,montant_rabais,prix_a_payer)

# Appel de la procédure main()
if __name__ == "__main__":
    main()