---
template: blog
content:
    items: '@self.children'
    order:
        by: folder
        dir: asc
---

Excercices de lecture et d'interprétation de code.