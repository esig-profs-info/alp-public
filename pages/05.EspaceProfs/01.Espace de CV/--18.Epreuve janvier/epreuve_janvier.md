---
template: item
published: true
---

# Épreuve de janvier

===

* **Rappels:**
    * L’épreuve a lieu le lundi 15.01.2024 à 08h30.
    * La durée du test est de 240 minutes.
    * Tous les documents personnels (y compris électroniques) sont autorisés
    * Tout accès au réseau est formellement interdit.
    * Le contrôle est obligatoirement réalisé sur le matériel mis à disposition par l'école (les ordinateurs personnels sont interdits).
* **Révision**
    * Alternatives
    * Procédures et fonctions
    * Boucles
    * Listes
    * Chaînes de caractères
    * Dictionnaires
    * Tableaux 2D (NumPy)
    * ... c'est à dire, cours 1 à 16 avec les séries d'exercices et travaux pratiques correspondants.