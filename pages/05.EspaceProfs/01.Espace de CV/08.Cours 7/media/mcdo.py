# Clément Vogt


# Constantes
BIG_MAC = 1
FRITES = 1
COCA = 1
KCAL_BIG_MAC = 509
KCAL_MC_NUGGETS = 170
KCAL_FRITES = 350
KCAL_SALADE_VERTE = 50
KCAL_COCA = 175
KCAL_COCA_ZERO = 1
ULTRA_CALORIQUE_POURCENTAGE_KCAL_ADDITIONNEL = 0.25


# Procédures et fonctions
def donnnes_entrees():
    print("Choisissez un plat principal :")
    print("1. Big Mac")
    print("2. Mc Nuggets")
    plat_principal = int(input("Entrez le numéro correspondant (1 ou 2):"))
    
    print()
    print("Choisissez un accompagnement :")
    print("1. Frites")
    print("2. Salade verte")
    accompagnement = int(input("Entrez le numéro correspondant (1 ou 2):"))
    
    print()
    print("Choisissez une boisson :")
    print("1. Coca Cola")
    print("2. Coca Cola Zero")
    boisson = int(input("Entrez le numéro correspondant (1 ou 2):"))
    
    return plat_principal, accompagnement, boisson

def calcul_calories_totales(plat_principal, accompagnement, boisson):
    total_kcal = 0
    
    if plat_principal == BIG_MAC:
        total_kcal += KCAL_BIG_MAC
    else:
        total_kcal += KCAL_MC_NUGGETS
        
    if accompagnement == FRITES:
        total_kcal += KCAL_FRITES
    else:
        total_kcal += KCAL_SALADE_VERTE
    
    if boisson == COCA:
        total_kcal += KCAL_COCA
    else:
        total_kcal += KCAL_COCA_ZERO
    
    if plat_principal == BIG_MAC and accompagnement == FRITES:
        total_kcal *= 1 + ULTRA_CALORIQUE_POURCENTAGE_KCAL_ADDITIONNEL
    
    return total_kcal

def afficher_resultats(total_kcal):
    print()
    print("Calories totales de votre menu :", total_kcal, "kcal")
    
def main():
    plat_principal, accompagnement, boisson = donnnes_entrees()
    total_kcal = calcul_calories_totales(plat_principal, accompagnement, boisson)
    afficher_resultats(total_kcal)
   
   
# Appel de la procédure main()
if __name__ == "__main__":
    main()
