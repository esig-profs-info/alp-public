---
template: item
published: true
---

Exercices traitant les listes en python

===

<!--todo: lister les pré-requis-->

### Attention: 
pour coder dans TigerJython, ajouter la ligne suivante en haut de votre code:
```
from __future__ import print_function
```

## Exercice 1
Dans cet exercice, vous allez écrire des fonctions permettant d'extraire des données d'une liste

1. Ecrire une fonction `premierDeLaListe(liste)` qui retourne la première valeur de la liste passée en paramètre
2. Ecrire une fonction `dernierDeLaListe(liste)` qui retourne la dernière valeur de la liste passée en parmaètre.
3. Ecrire une fonction `avantDernierDeLaListe(liste)` qui retourne l'avant-dernière valeur de la liste passée en paramètre.
4. Ecrire une fonction `niemeValeurDeLaListe(liste, n)` qui retourne la nième valeur de la liste (`n` et `liste` passés en paramètres).

Testez vos fonctions avec les listes suivantes:</br>
```
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] #liste de 10 nombres entiers

["alice", "bob"] #liste de deux chaînes de caractères 

[-3.5] #liste de un nombre réel 

[] #liste vide
```

**Questions:** *Que se passe-t-il si la liste passée en paramètre est vide? ou si pour une liste de 2 éléments, on demande le 3ème?*

## Exercice 2
Ecrire une fonction afficherListe(maListe) qui affiche chaque élément d'une liste sur une nouvelle ligne. Réutilisez les listes de l'Exercice 1.

## Exercice 3
Ecrire une fonction afficherPositifs(nombres) qui affiche les éléments strictement positif d'une liste de nombres. Réutilisez la liste des 10 nombres entiers de l'Exercice 1.

## Exercice 4
Afficher le nombre d'éléments d'une liste d'entiers `maListe` dont la valeur est compris entre 2 valeurs lues, `valMin` et `valMax` (inégalités strictes).

<!--Dans cet exercice, les valeurs fournies (`valMin` et `valMax`) sont toujours données dans l'ordre, à savoir: la plus petite d'abord, la plus grande ensuite. Il n'est donc pas nécessaire de tester que les deux valeurs données soient dans le bon ordre puisqu'elles le sont de fait.-->

#### Données de test:

maListe | valMin | valMax | nombre d'éléments
   ---|---|---|:---:
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | -1 | 1 |1
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | -10 | 9 | 10
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8]| 2 | 7 | 2
[-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] | 5 | 5 | 0


## Exercice 5
Python intègre une fonction nommée `len(list)` qui retourne la longueur de la liste passée en paramètre (le nombre d'éléments qu'elle contient).
Ecrire votre propre fonction `longueur(list)` qui compte le nombre d'éléments d'une liste sans utiliser la fonction `len(list)`. Comparez ensuite votre résultat avec celui de la fonction `len(list)`. Réutilisez les listes de l'Exercice 1 pour vos tests.

## Exercice 6
Python intègre la possibilité de connaître la position (l'index) d'un élément dans une liste. La syntaxe pour l'utiliser est la suivante: `liste.index(element)`
<br/>**Par exemple:**
```python
liste = ["un","quatre","six","8"]
indexDuSix = liste.index("six")
print(indexDuSix) #affiche 2
```
Ecrire une fonction nommée `monIndex(liste, e)` qui retourne l'index de l'élément `e` s'il est contenu dans la liste, sinon `-1`. Ceci, sans utiliser la fonction `index(element)`.

Comparez ensuite votre résultat avec celui de la fonction `index(element)`. Notamment, observez ce qu'il se passe si l'élément est contenu plusieurs fois dans la liste. Réutilisez les listes de l'Exercice 1 pour vos tests.



<!--
idees:

Exercices:
coder le count() sans utiliser count()
coder le index(el, start, end) pour retourner l'index d'un élément dans une partie de la liste 
Pour chaque éléments d'une liste: afficher son type en utilisant la fonction type
    compter le nombre d'éléments de type entier(p.ex) dans une liste (en utlisant  type(element)==int )
livret sous forme de liste => implique append()

-->
