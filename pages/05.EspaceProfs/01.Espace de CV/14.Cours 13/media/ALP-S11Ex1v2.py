# Série 11, Exercice 1
# Clément Vogt
# Version 2: Utilisation de la méthode prédéfinie count() pour compter le nombre d'apparations d'un caractère dans une chaîne de caractères

# Procédures et fonctions
def donnees_entree():
    return input("Saisissez une chaîne de caractères"), input("Saisissez un caractère à compter")

def afficher_resultats(chaine,caractere,nb_fois):
    print("la lettre '",caractere,"' se trouve ", nb_fois, " fois dans '",chaine,"'",sep="")

# Procédure main()
def main():
    chaine, caractere = donnees_entree()
    nb_fois = chaine.count(caractere)
    afficher_resultats(chaine,caractere,nb_fois)
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()