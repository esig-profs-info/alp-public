---
template: item
published: false
---
# TP Rattrapage - partie B

Vous allez désormais utiliser un dictionnaire supplémentaire, ```dicoNoms``` qui associe à chaque cours (épreuve), une liste de noms, ceux des étudiants concernés par l'épreuve de rattrapage.
Au besoin récupérer la nouvelle version de [RattrapageHelper.py](RattrapageHelper.py).


### Exo B.1 - Interaction (simple)
 Ecrire une procédure (fonction, commande) Python qui demande à l'utilisateur un nom, puis un cours et qui indique si oui ou non l'étudiant doit faire le rattrapage. Afficher un message si le cours est inconnu.


### Exo B.2 - Le plus d'étudiants
 Ecrire une fonction Python qui détermine le cours où le plus d'étudiants doit passer le rattrapage.
 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : la réponse est ```le cours TEPR``` (avec 13 étudiants)

 *Supplément* : Ecrire une variante qui indique le cours **et** le nombre d'étudiants. Pour cela, que devez-vous modifier : la fonction de calcul ou la fonction d'affichage ?


### Exo B.3 - Dénombrer les étudiants
 Ecrire une fonction Python qui crée un dictionnaire associant à chaque cours le nombre d'étudiants qui doivent passer le rattrapage.
 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : voici les résultats (ordre non significatif)

|  Cours  | Nombre d'étudiants   |
|---------|:--------------------:|
| ALP     |                   11 |
| MIRESE  |                    9 |
| MODE    |                   11 |
| DROIT   |                   12 |
| DWEB    |                   11 |
| FRTC    |                   11 |
| TQGE    |                   12 |
| GESTION |                   12 |
| TEPR    |                   13 |
|         |                      |


 *Supplément* : Ecrire une variante de la fonction de l'Exo B.2 qui utilise ce dictionnaire.


### Exo B.4 - Liste des étudiants
 Ecrire une fonction Python qui établit la liste des étudiants qui doivent passer un ou plusieurs rattrapages. Chaque nom ne doit apparaître qu'une fois.
 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : voici le résultat (ordre non significatif)
 ```
 ['HOEC', 'PYGOOPY', 'ZASFA', 'VLEZUR', 'ZETHODE', 'UDVO', 'BUQOH-AKHSO', 
  'AREGAZRRO', 'HUJICY', 'YVEWOW', 'MIJGYH', 'RYHXAH', 'LJUOSOP', 'VEMI',
  'ZEEJ', 'GUKO-PAYQYJ', 'SIO', 'GUFUJU']
 ```


### Exo B.5 - Inverser le dictionnaire des noms
 Ecrire une fonction Python qui construit un dictionnaire associant, à chaque étudiant concerné, la liste des cours où il doit passer un rattrapage.
 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : voici le résultat (ordre non significatif)
 ```
 HOEC doit repasser ALP, MIRESE, MODE, FRTC, TQGE
 PYGOOPY doit repasser ALP, MIRESE, MODE, DROIT, DWEB, FRTC, TQGE, GESTION, TEPR
 ZASFA doit repasser ALP, MIRESE, MODE, DROIT, DWEB, FRTC, TQGE, GESTION, TEPR
 VLEZUR doit repasser ALP, MIRESE, MODE, DROIT, DWEB, FRTC, TQGE, GESTION, TEPR
 ZETHODE doit repasser ALP, MIRESE, FRTC, TEPR
 UDVO doit repasser ALP, MIRESE, MODE, DROIT, FRTC, TQGE, GESTION, TEPR
 BUQOH-AKHSO doit repasser ALP, MIRESE, MODE, DROIT, DWEB, FRTC, TQGE, GESTION, TEPR
 AREGAZRRO doit repasser ALP, FRTC
 HUJICY doit repasser ALP, FRTC
 YVEWOW doit repasser ALP, FRTC
 MIJGYH doit repasser ALP, MIRESE, MODE, DROIT, FRTC, TQGE, GESTION, TEPR
 RYHXAH doit repasser MIRESE, MODE, DROIT, DWEB, TQGE, GESTION, TEPR
 LJUOSOP doit repasser MODE, DROIT, DWEB, TQGE, GESTION, TEPR
 VEMI doit repasser MODE, DROIT, DWEB, TQGE, GESTION, TEPR
 ZEEJ doit repasser MODE, DROIT, DWEB, TQGE, GESTION, TEPR
 GUKO-PAYQYJ doit repasser DROIT, DWEB, GESTION, TEPR
 SIO doit repasser DROIT, DWEB, TQGE, GESTION, TEPR
 GUFUJU doit repasser DWEB
 ```

### Exo B.6 - Lister les cours concernant les mêmes
 Ecrire une fonction Python qui affiche les épreuves ayant le *même nombre d'étudiants* concernés.
 Il n'est pas obligatoire d'utiliser le dictionnaire construit en B.3 pour répondre à la question mais c'est une voie possible (et assez pratique si on y réfléchit bien).

 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : voici le résultat (ordre non significatif)
 ```
 Il y a 11 étudiants qui doivent repasser ALP, MODE, DWEB, FRTC
 Il y a 9 étudiants qui doivent repasser MIRESE
 Il y a 12 étudiants qui doivent repasser DROIT, TQGE, GESTION
 Il y a 13 étudiants qui doivent repasser TEPR
 ```

 *Supplément* (***très difficile*** !) : Ecrire une fonction Python qui affiche les épreuves ayant la même *liste d'étudiants* concernés. Dans ce cas, l'utilisation d'un nouveau dictionnaire n'est pas possible sans une contorsion technique (en effet, une liste ne peut pas être une clé dans un dictionnaire). Une autre approche serait d'utiliser une liste dont les éléments sont des listes à deux éléments qui sont eux-mêmes des listes (de noms et de cours, respectivement).

 >*Info* : voici le résultat (ordre non significatif)

    Les cours ALP et FRTC partagent cette liste d'étudiants :
    HOEC, PYGOOPY, ZASFA, VLEZUR, ZETHODE, UDVO, BUQOH-AKHSO, AREGAZRRO, HUJICY, YVEWOW, MIJGYH
    
    Les cours DROIT et GESTION partagent cette liste d'étudiants :
    GUKO-PAYQYJ, LJUOSOP, PYGOOPY, ZASFA, VLEZUR, RYHXAH, VEMI, UDVO, BUQOH-AKHSO, MIJGYH, SIO, ZEEJ
 

### Exo B.7 - Dictionnaire des durées par étudiant
 Pour cette question, vous aurez besoin, en plus de ```dicoNoms```, du dictionnaire ```dicoDurées``` de la partie A. 
 Ecrire une fonction Python qui range (et renvoie), dans un nouveau dictionnaire, la durée *totale* de rattrapage que doit passer chaque étudiant concerné.
 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : voici le résultat (ordre non significatif)

|   étudiant  | durée (en minutes) |
|:------------|:---------------:|
| HOEC        |             660 |
| PYGOOPY     |            1020 |
| ZASFA       |            1020 |
| VLEZUR      |            1020 |
| ZETHODE     |             510 |
| UDVO        |             900 |
| BUQOH-AKHSO |            1020 |
| AREGAZRRO   |             330 |
| HUJICY      |             330 |
| YVEWOW      |             330 |
| MIJGYH      |             900 |
| RYHXAH      |             690 |
| LJUOSOP     |             600 |
| VEMI        |             600 |
| ZEEJ        |             600 |
| GUKO-PAYQYJ |             360 |
| SIO         |             420 |
| GUFUJU      |             120 |

### Exo B.8 - Temps de rattrapage le plus élevé
 Pour cette question, il sera beaucoup plus simple d'avoir recours au dictionnaire créé dans l'exercice précédent B.7. 
 Ecrire une fonction Python qui détermine la durée de rattrapage la plus élevée.
 Ecrire aussi une fonction de test (appel+affichage).

 >*Info* : la réponse est ```1020```

 *Supplément* : Modifier la fonction d'une façon ou d'une autre pour répondre à la question : qui sont les étudiants concernés par cette durée de rattrapage maximale ? (sous la forme d'une liste)

 >*Info* : la réponse est

 ```Les étudiants passant 1020 minutes de rattrapage sont PYGOOPY, ZASFA, VLEZUR, BUQOH-AKHSO.```

