# pas d'import

#constantes
LISTE_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
LISTE_2 = [0, 1, 7, 1, 5, 9, 2, 6, 5, 3, 34, 0, 23, 11, 75, 23, 55, 4, 12, 10, 4, 67, 99]
LISTE_3 = [1000, 32, -10, 0, 9012, 234, 642, 9, -9, -10, 98, 22, -33]

#liste: une liste de nombres,
#pivot: la valeur frontière,
def compteNbValeurs_pp_pg(liste, pivot):
    pass
    #todo: retourne une liste de 2 valeurs (nb plus petit que pivot, nb plus grand que pivot)

#listeBase: la liste complète
#listeDecompte: la liste des 2 valeurs (nb plus petit, nb plus grand)
def verifieResultat(listeBase, listeDecompte, pivot):
    pass
    #todo: #retourne vrai si le compte est bon (toutes les valeurs ont été comptées),
            #retourne faux sinon

#listeBase: la liste complète
#listeDecompte: la liste des 2 valeurs (nb plus petit, nb plus grand)
def afficherResultat(listeComplete, listeDecompte, pivot):
    pass
    #todo: affiche par exemple:
    #"la liste [1,2,3,4,5,6,7,8,9,10] contient 3 nombres plus petits que la valeur pivot 4 et 6 plus grands"
    
#--programme principal (main)--
def main():
    pass
#todo: à compléter pour obtenir les sorties produites
# c'est-à-dire
# - afficher le résultat pour les 3 listes avec le pivot 4 si le compte est bon,
# - sinon afficher "problème"