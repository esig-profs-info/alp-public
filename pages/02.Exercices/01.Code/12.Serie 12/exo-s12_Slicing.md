---
template: item
published: true
---

# Série 12
## Objectifs
Voir quelques cas d'utilisation du ***slicing*** (= découpage en tranche)


===

## Notions pour la série
[Démarche de résolution de problèmes](/ressources/démarche%20de%20resolution%20de%20problemes/Démarche.pdf)

[Squelette du programme](/ressources/squelette/squelette.py)

[Les chaînes de caractères](/ressources/chaines%20de%20caracteres)

## Contraintes impératives
Les différents programmes que vous devez réaliser seront nommés **ALP-S12Ex1** à **ALP-S12Ex3** et se trouveront dans un dossier nommé **ALP-S12**.

## Énoncé
## Exercice 1
Pour une prise en main de base, écrire un programme comportant deux fonctions :
* `premiers3(s)` permettant d'obtenir, en une ligne grâce au *slicing*, les 3 premiers caractères de la chaîne `s` passée en paramètre.
* `derniers3(s)` permettant d'obtenir, en une ligne grâce au *slicing*, les 3 derniers caractères de la chaîne `s` passée en paramètre.

Ce programme demandera à l'utilisateur une chaîne et affichera les valeurs renvoyées par les deux fonctions pour cette chaine.

Afficher le résultat comme illustré ci-dessous :

| Chaîne saisie | Résultat                                 |
|---------------|------------------------------------------|
| PYTHON        | Les 3 premiers caractères = PYT<br>Les 3 derniers caractères = HON |
| Esig          | Les 3 premiers caractères = Esi<br>Les 3 derniers caractères = sig |
| ...           |                                          |


## Exercice 1Plus
Reprendre le programme et les fonctions de l'exercice précédent. 
L'objectif est désormais d'avoir un deuxième paramètre aux fonctions qui indique combien de caractères on désire obtenir. 

| Chaîne saisie | Nombre de caractères | Résultat                                 |
|---------------|----------------------|------------------------------------------|
| PYTHON        | 3                    | Les 3 premiers caractères = PYT<br>Les 3 derniers caractères = HON |
| PYTHON        | 4                    | Les 4 premiers caractères = PYTH<br>Les 4 derniers caractères = THON |
| Esig          | 3                    | Les 3 premiers caractères = Esi<br>Les 3 derniers caractères = sig |
| Esig          | 2                    | Les 2 premiers caractères = Es<br>Les 2 derniers caractères = ig |
| ...           |                      |                                          |
         |

## Exercice 2
Reprendre la fonction `ajoute_au_milieu(s1,s2)` de la série 11 et écrire une version qui n'utilise que du *slicing*.<br/>

Rappel des exemples:
```
ajoute_au_milieu("python", "COUCOU") → "pytCOUCOUhon"
ajoute_au_milieu("sandbox", "COUCOU") → "sandCOUCOUbox"
```

## Exercice 3
Reprendre le programme qui indique si un mot saisi en entrée est un palindrome (un mot que se lit dans les deux sens). Votre programme doit contenir au minimum une fonction à résultat booléeen. 

Comme dans la version sans *slicing* de la série précédente, vous avez deux approches possibles :
1. Vous pouvez inverser la chaîne et vérifier si la chaîne inversée est égale au mot de départ.
2. Vous pouvez aussi comparer la première moitié du mot avec *l'inverse* de la deuxième moitié : ces deux morceaux de chaînes seront égaux si le mot de départ est un palindrome. (Vous pouvez tout aussi bien comparer l'inverse de la première moitié avec la deuxième moitié ; cela revient au même.) Le coeur du problème est de bien découper en deux moitiés comparables.

Rappel des exemples:
```
Quel mot voulez-vous tester? sugus
--> sugus est un palindrome

Quel mot voulez-vous tester? python
--> python n'est pas un palindrome
```
