# Exercice de révision sur les boucles
# Nom et prénom: ?

# Procédures et fonctions
''' Afficher les 15 premiers multiples de 6 :  6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90 '''
def afficher_mult_6a():
    # A COMPLETER
    
''' Afficher les multiples de 6, compris entre 6 et 50 :  6, 12, 18, 24, 30, 36, 42, 48 '''
def afficher_mult_6b():
    # A COMPLETER
    
''' Afficher les multiples de 6, tant que ce n'est pas un multiple de 14 :  6, 12, 18, 24, 30, 36 '''
def afficher_mult_6c():
    # A COMPLETER

# Procédure main()
def main():
    afficher_mult_6a()
    afficher_mult_6b()
    afficher_mult_6c()
    
# Appel de la procédure main()
if __name__ == "__main__":
    main()