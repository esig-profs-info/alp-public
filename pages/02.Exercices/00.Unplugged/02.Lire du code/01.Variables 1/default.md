---
template: item
published: true
---

# Variables 1

Répondez aux questions suivantes et *notez vos réponses*. Les programmes peuvent contenir des erreurs: dans ce cas, essayez de prédire le type d'erreur produit.

Après avoir répondu à toutes les questions, testez les programmes. Ajustez vos réponses si nécessaire. Si les programmes produisent des erreurs, trouver une correction.

Chaque question est complètement indépendante des autres (c'est un nouveau programme à chaque fois).

## Question 0

Soit le programme suivant:

```python
x = "super"
```
Qu'affichera ce programme? Pourquoi?

## Question 1

Soit le programme suivant:

```python
print(x)
```
Qu'affichera ce programme? Pourquoi?

## Question 2
Soit le programme suivant:
```python
x = 3
x
```
Qu'affichera ce programme? Pourquoi?

## Question 3
Soit le programme suivant:
```python
y = 3
print(x)
```
Qu'affichera ce programme? Pourquoi?

## Question 4
Soit le programme suivant:
```python
x = y
print(x)
```
Qu'affichera ce programme? Pourquoi?

## Question 4
Soit le programme suivant:
```python
schtroumpf = "hello"
schtroumpf = schtroumpf + " schtroumpf" + "\n"
print(schtroumpf*3)
```
Qu'affichera ce programme? Pourquoi?

## Question 5
Soit le programme suivant:
```python
schtroumpf = "hello"
bla = schtroumpf*2
print(schtroumpf)
print(bla)
```
Qu'affichera ce programme? Pourquoi?

## Question 6
Soit le programme suivant:
```python
schtroumpf = "hello"
bla = schtroumpf - 2
print(schtroumpf)
print(bla)
```
Qu'affichera ce programme? Pourquoi?

## Question 7
Soit le programme suivant:
```python
x = 3
y = x
x += 2
print(x)
print(y)
```
Qu'affichera ce programme? Pourquoi?

## Question 8
Soit le programme suivant:
```python
x = 3
y = 1
# échanger les valeurs
x = y
y = x
print(x)
print(y)
```
Qu'affichera ce programme? Pourquoi?


